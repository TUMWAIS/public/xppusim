from xppusim.modullib.Error import Error
# Overview of high level functions provided by this module

#FUNCTION:                              DESCRIPTION:
#stack_separateWP: 			            Separate single workpiece from stack
#crane_moveToStack: 			        Move crane from any position to stack
#crane_moveToConveyor: 			        Move crane from any position to conveyor
#crane_moveToStamp: 			        Move crane from any position to stamp
#crane_pickupWP: 			            Pick up a workpiece at current position with crane
#crane_putdownWP: 			            Put down a workpiece at current position with crane
#stamp_stampWP: 				        Stamp workpiece that was provided to the stamp
#lsc_moveWPForwardToPos1: 		        Move workpiece forward to positioning sensor 1 on LargeSortingConveyor
#lsc_moveWPForwardToPos2: 		        Move workpiece forward to positioning sensor 2 on LargeSortingConveyor
#lsc_moveWPBackToPos0: 			        Move workpiece back to positioning sensor 0 on LargeSortingConveyor
#lsc_moveWPBackToPos1: 			        Move workpiece back to positioning sensor 1 on LargeSortingConveyor
#lsc_moveWPfromPos1ToCC1: 		        Move workpiece from positioning sensor 1 to Color Check 1 on LargeSortingConveyor
#lsc_moveWPfromPos1ToCC2: 		        Move workpiece from positioning sensor 1 to Color Check 2 on LargeSortingConveyor
#lsc_moveWPfromPos1ToRamp1: 	        Move workpiece from positioning sensor 1 to ramp 1 on LargeSortingConveyor
#lsc_moveWPfromPos1ToRamp2: 	        Move workpiece from positioning sensor 1 to ramp 2 on LargeSortingConveyor
#lsc_moveAndSortWPToRamp3: 		        Move workpiece from any position to ramp 3 of the LargeSortingConveyor and sort it into the ramp
#lsc_sortWPToRamp1: 			        Sort workpiece at ramp 1 of the LargeSortingConveyor into the ramp
#lsc_sortWPToRamp2: 			        Sort workpiece at ramp 2 of the LargeSortingConveyor into the ramp
#lsc_moveWPToPickingPosOnPAC: 	        Move workpiece from LargeSortingConveyor to the picking position on PickalphaConveyor
#pac_moveWPForwardToPos2: 		        Move workpiece forward to positioning sensor 2 on PicALphaConveyor
#pac_moveWPBackToPos1: 			        Move workpiece back to positioning sensor 1 on PicALphaConveyor
#pac_moveWPToPos1OnSSC: 		        Move workpiece from the PicalphaConveyor to the positioning sensor 1 on SmallSortingConveyor
#ssc_moveWPfromPos1ToCC: 		        Move workpiece from positioning sensor 1 to Color Check on SmallSortingConveyor
#ssc_moveWPForwardToPos2: 		        Move workpiece forward to positioning sensor 2 on SmallSortingConveyor
#ssc_moveWPForwardToPos3: 		        Move workpiece forward to positioning sensor 3 on SmallSortingConveyor
#ssc_moveWPBackToPos2: 			        Move workpiece back to positioning sensor 2 on SmallSortingConveyor
#ssc_moveWPBackToPos1: 			        Move workpiece back to positioning sensor 1 on SmallSortingConveyor
#ssc_moveAndSortWPToRamp4: 		        Move workpiece from any position to ramp 4 of the SmallSortingConveyor and sort it into the ramp
#ssc_moveWPToPos1OnRFC: 		        Move workpiece from the SmallSortingCOnveyor to the positioning sensor 1 on RefillConveyor
#rfc_moveWPForwardToPos2: 		        Move workpiece forward to positioning sensor 2 on RefillConveyor
#rfc_moveWPBackToPos1: 			        Move workpiece back to positioning sensor 1 on RefillConveyor
#rfc_moveWPToPos0OnLSC: 			    Move workpiece from the RefillCOnveyor to the positioning sensor 0 on LargeSortingConveyor
#picalpha_moveBackToPickupPos1: 		Move PicAlpha back to second workpiece behind switch
#picalpha_moveBackToPickupPos2: 		Move PicAlpha back to third workpiece behind switch
#picalpha_moveBackToPutdownPos1: 	    Move PicAlpha back the putdown position in the middle of the conveyor
#picalpha_moveForwardToPickupPos1: 	    Move PicAlpha forward to second workpiece behind switch
#picalpha_moveFOrwardToPutdownPos1: 	Move PicAlpha forward to the putdown position in the middle of the conveyor
#picalpha_moveForwardToPutdownPos2: 	Move PicAlpha forward to the put down position at the end of the conveyor
#picalpha_pickupWP: 			        Pick up workpiece with PickAlpha at current position
#picalpha_putdownWP: 			        Put down workpiece with PickAlpha at current position


#--- Global variables ---

# Thresholds for faults due to not met post conditions
threshold_small = 50
threshold_large = 200


#--- High level functions for scenario building ---

# Separate single workpiece from stack
def stack_separateWP(xppu, sensor=False):
    xppu.stack.separator.DO_Extend = True
    counter = 0
    while xppu.stack.separator.DI_Extended is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("AI_Position:", xppu.stack.separator.AI_Position,
                  "DI_Extended:", xppu.stack.separator.DI_Extended,
                  "DI_Retracted:", xppu.stack.separator.DI_Retracted)
    xppu.stack.separator.DO_Extend = False
    counter = 0
    while xppu.stack.separator.DI_Retracted is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("AI_Position:", xppu.stack.separator.AI_Position,
                  "DI_Extended:", xppu.stack.separator.DI_Extended,
                  "DI_Retracted:", xppu.stack.separator.DI_Retracted)
    if sensor is True:
        print("DI_WPProvided", xppu.stack.DI_WPProvided,
              "DI_WPMetallic", xppu.stack.DI_WPMetallic,
              "DI_WPLight", xppu.stack.DI_WPLight,
              "AI_WPWeight", xppu.stack.AI_WPWeight)

# Move crane from any position to stack
def crane_moveToStack(xppu, sensor=False):
    if xppu.crane.table.AI_Position < 0:
        xppu.crane.table.DO_TurnCounterClockwise = True
        counter = 0
        while xppu.crane.table.DI_AtStack is False and counter < threshold_small:
            counter += 1
            xppu.make_step(1)
            if sensor is True:
                print("DI_AtStack:", xppu.crane.table.DI_AtStack,
                      "DI_AtConveyor:", xppu.crane.table.DI_AtConveyor,
                      "DI_AtStamp:", xppu.crane.table.DI_AtStamp,
                      "AI_Position", xppu.crane.table.AI_Position)
        xppu.crane.table.DO_TurnCounterClockwise = False
    elif xppu.crane.table.AI_Position > 0:
        xppu.crane.table.DO_TurnClockwise = True
        counter = 0
        while xppu.crane.table.DI_AtStack is False and counter < threshold_small:
            counter += 1
            xppu.make_step(1)
            if sensor is True:
                print("DI_AtStack:", xppu.crane.table.DI_AtStack,
                      "DI_AtConveyor:", xppu.crane.table.DI_AtConveyor,
                      "DI_AtStamp:", xppu.crane.table.DI_AtStamp,
                      "AI_Position", xppu.crane.table.AI_Position)
        xppu.crane.table.DO_TurnClockwise = False

# Move crane from any position to conveyor
def crane_moveToConveyor(xppu, sensor=False):
    if xppu.crane.table.AI_Position < 90:
        xppu.crane.table.DO_TurnCounterClockwise = True
        counter = 0
        while xppu.crane.table.DI_AtConveyor is False and counter < threshold_small:
            counter += 1
            xppu.make_step(1)
            if sensor is True:
                print("DI_AtStack:", xppu.crane.table.DI_AtStack,
                      "DI_AtConveyor:", xppu.crane.table.DI_AtConveyor,
                      "DI_AtStamp:", xppu.crane.table.DI_AtStamp,
                      "AI_Position", xppu.crane.table.AI_Position)
        xppu.crane.table.DO_TurnCounterClockwise = False
    elif xppu.crane.table.AI_Position > 90:
        xppu.crane.table.DO_TurnClockwise = True
        counter = 0
        while xppu.crane.table.DI_AtConveyor is False and counter < threshold_small:
            counter += 1
            xppu.make_step(1)
            if sensor is True:
                print("DI_AtStack:", xppu.crane.table.DI_AtStack,
                      "DI_AtConveyor:", xppu.crane.table.DI_AtConveyor,
                      "DI_AtStamp:", xppu.crane.table.DI_AtStamp,
                      "AI_Position", xppu.crane.table.AI_Position)
        xppu.crane.table.DO_TurnClockwise = False

# Move crane from any position to stamp
def crane_moveToStamp(xppu, sensor=False):
    if xppu.crane.table.AI_Position < 180:
        xppu.crane.table.DO_TurnCounterClockwise = True
        counter = 0
        while xppu.crane.table.DI_AtStamp is False and counter < threshold_small:
            counter += 1
            xppu.make_step(1)
            if sensor is True:
                print("DI_AtStack:", xppu.crane.table.DI_AtStack,
                      "DI_AtConveyor:", xppu.crane.table.DI_AtConveyor,
                      "DI_AtStamp:", xppu.crane.table.DI_AtStamp,
                      "AI_Position", xppu.crane.table.AI_Position)
        xppu.crane.table.DO_TurnCounterClockwise = False
    elif xppu.crane.table.AI_Position > 180:
        xppu.crane.table.DO_TurnClockwise = True
        counter = 0
        while xppu.crane.table.DI_AtStamp is False and counter < threshold_small:
            counter += 1
            xppu.make_step(1)
            if sensor is True:
                print("DI_AtStack:", xppu.crane.table.DI_AtStack,
                      "DI_AtConveyor:", xppu.crane.table.DI_AtConveyor,
                      "DI_AtStamp:", xppu.crane.table.DI_AtStamp,
                      "AI_Position", xppu.crane.table.AI_Position)
        xppu.crane.table.DO_TurnClockwise = False

# Pick up a workpiece at current position with crane
def crane_pickupWP(xppu, sensor=False):
    xppu.crane.liftingCylinder.DO_Extend = False
    counter = 0
    while xppu.crane.liftingCylinder.DI_Retracted is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("AI_Position:", xppu.crane.liftingCylinder.AI_Position,
                  "DI_Extended:", xppu.crane.liftingCylinder.DI_Extended,
                  "DI_Retracted:", xppu.crane.liftingCylinder.DI_Retracted)
    xppu.crane.DO_Intake = True
    counter = 0
    while xppu.crane.DI_TakenIn is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_TakenIn:", xppu.crane.DI_TakenIn)
    xppu.crane.liftingCylinder.DO_Extend = True
    counter = 0
    while xppu.crane.liftingCylinder.DI_Extended is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("AI_Position:", xppu.crane.liftingCylinder.AI_Position,
                  "DI_Extended:", xppu.crane.liftingCylinder.DI_Extended,
                  "DI_Retracted:", xppu.crane.liftingCylinder.DI_Retracted)

# Put down a workpiece at current position with crane
def crane_putdownWP(xppu, sensor=False):
    xppu.crane.liftingCylinder.DO_Extend = False
    counter = 0
    while xppu.crane.liftingCylinder.DI_Retracted is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("AI_Position:", xppu.crane.liftingCylinder.AI_Position,
                  "DI_Extended:", xppu.crane.liftingCylinder.DI_Extended,
                  "DI_Retracted:", xppu.crane.liftingCylinder.DI_Retracted)
    xppu.crane.DO_Intake = False
    counter = 0
    while xppu.crane.DI_TakenIn is True and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_TakenIn:", xppu.crane.DI_TakenIn)
    xppu.crane.liftingCylinder.DO_Extend = True
    counter = 0
    while xppu.crane.liftingCylinder.DI_Extended is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("AI_Position:", xppu.crane.liftingCylinder.AI_Position,
                  "DI_Extended:", xppu.crane.liftingCylinder.DI_Extended,
                  "DI_Retracted:", xppu.crane.liftingCylinder.DI_Retracted)

# Stamp workpiece that was provided to the stamp and change stamp tool
def stamp_stampWP_setuptimes(xppu, sensor=False):
    stamp_setuptimes(xppu)
    stamp_stampWP(xppu, sensor)

# Stamp workpiece that was provided to the stamp
def stamp_stampWP(xppu, sensor=False):
    if sensor is True:
        print("DI_WPAvailable", xppu.stamp.DI_WPAvailable)
    xppu.stamp.slidingCylinder.DO_Extend = False
    xppu.stamp.slidingCylinder.DO_Retract = True
    counter = 0
    while xppu.stamp.slidingCylinder.DI_Retracted is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("AI_Position:", xppu.stamp.slidingCylinder.AI_Position,
                  "DI_Extended:", xppu.stamp.slidingCylinder.DI_Extended,
                  "DI_Retracted:", xppu.stamp.slidingCylinder.DI_Retracted)
    xppu.stamp.stampingCylinder.DO_Extend = True
    counter = 0
    while xppu.stamp.stampingCylinder.DI_Extended is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("AI_Position:", xppu.stamp.stampingCylinder.AI_Position,
                  "DI_Extended:", xppu.stamp.stampingCylinder.DI_Extended,
                  "DI_Retracted:", xppu.stamp.stampingCylinder.DI_Retracted)
    xppu.stamp.stampingCylinder.DO_Extend = False
    counter = 0
    while xppu.stamp.stampingCylinder.DI_Retracted is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("AI_Position:", xppu.stamp.stampingCylinder.AI_Position,
                  "DI_Extended:", xppu.stamp.stampingCylinder.DI_Extended,
                  "DI_Retracted:", xppu.stamp.stampingCylinder.DI_Retracted)
    xppu.stamp.slidingCylinder.DO_Extend = True
    xppu.stamp.slidingCylinder.DO_Retract = False
    counter = 0
    while xppu.stamp.slidingCylinder.DI_Extended is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("AI_Position:", xppu.stamp.slidingCylinder.AI_Position,
                  "DI_Extended:", xppu.stamp.slidingCylinder.DI_Extended,
                  "DI_Retracted:", xppu.stamp.slidingCylinder.DI_Retracted)

# If stamp setup is not equal to color of current WP, add setup time as (+100 timesteps)
def stamp_setuptimes(xppu):
    for wp in xppu.wp_register:
        if wp.pos.to_tuple(4) == xppu.stamp.slidingCylinder.pos.to_tuple(4):
            if xppu.stamp.wp_setup != wp.color:
                xppu.timestep += xppu.stamp_setup_time
                print('Setup time!')

# Move workpiece forward to positioning sensor 1 on LargeSortingConveyor
def lsc_moveWPForwardToPos1(xppu, sensor=False):
    xppu.largesortingconveyor.DO_TurnClockwise = True
    counter = 0
    while xppu.largesortingconveyor.DI_WPPosition1 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_WPPosition0:", xppu.largesortingconveyor.DI_WPPosition0,
                  "DI_WPPosition1:", xppu.largesortingconveyor.DI_WPPosition1,
                  "VI_WPPosition", xppu.largesortingconveyor.VI_WPPosition)
    xppu.make_step(2) # Center workpiece at sensor
    xppu.largesortingconveyor.DO_TurnClockwise = False

# Move workpiece forward to positioning sensor 2 on LargeSortingConveyor
def lsc_moveWPForwardToPos2(xppu, sensor=False):
    xppu.largesortingconveyor.DO_TurnClockwise = True
    counter = 0
    while xppu.largesortingconveyor.DI_WPPosition2 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_WPPosition0:", xppu.largesortingconveyor.DI_WPPosition0,
                  "DI_WPPosition1:", xppu.largesortingconveyor.DI_WPPosition1,
                  "DI_WPPosition2:", xppu.largesortingconveyor.DI_WPPosition2,
                  "DI_WPMetallic1:", xppu.largesortingconveyor.DI_WPMetallic1,
                  "DI_WPLight1:", xppu.largesortingconveyor.DI_WPLight1,
                  "DI_WPMetallic2:", xppu.largesortingconveyor.DI_WPMetallic2,
                  "DI_WPLight2:", xppu.largesortingconveyor.DI_WPLight2,
                  "VI_WPPosition", xppu.largesortingconveyor.VI_WPPosition)
    xppu.make_step(2)  # Center workpiece at sensor
    xppu.largesortingconveyor.DO_TurnClockwise = False

# Move workpiece back to positioning sensor 0 on LargeSortingConveyor
def lsc_moveWPBackToPos0(xppu, sensor=False):
    xppu.largesortingconveyor.DO_TurnCounterClockwise = True
    counter = 0
    while xppu.largesortingconveyor.DI_WPPosition0 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_WPPosition0:", xppu.largesortingconveyor.DI_WPPosition0,
                  "DI_WPPosition1:", xppu.largesortingconveyor.DI_WPPosition1,
                  "DI_WPPosition2:", xppu.largesortingconveyor.DI_WPPosition2,
                  "DI_WPMetallic1:", xppu.largesortingconveyor.DI_WPMetallic1,
                  "DI_WPLight1:", xppu.largesortingconveyor.DI_WPLight1,
                  "DI_WPMetallic2:", xppu.largesortingconveyor.DI_WPMetallic2,
                  "DI_WPLight2:", xppu.largesortingconveyor.DI_WPLight2,
                  "VI_WPPosition", xppu.largesortingconveyor.VI_WPPosition)
    xppu.make_step(2)  # Center workpiece at sensor
    xppu.largesortingconveyor.DO_TurnCounterClockwise = False

# Move workpiece back to positioning sensor 1 on LargeSortingConveyor
def lsc_moveWPBackToPos1(xppu, sensor=False):
    xppu.largesortingconveyor.DO_TurnCounterClockwise = True
    counter = 0
    while xppu.largesortingconveyor.DI_WPPosition1 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_WPPosition1:", xppu.largesortingconveyor.DI_WPPosition1,
                  "DI_WPPosition2:", xppu.largesortingconveyor.DI_WPPosition2,
                  "DI_WPMetallic1:", xppu.largesortingconveyor.DI_WPMetallic1,
                  "DI_WPLight1:", xppu.largesortingconveyor.DI_WPLight1,
                  "DI_WPMetallic2:", xppu.largesortingconveyor.DI_WPMetallic2,
                  "DI_WPLight2:", xppu.largesortingconveyor.DI_WPLight2,
                  "VI_WPPosition", xppu.largesortingconveyor.VI_WPPosition)
    xppu.make_step(2)  # Center workpiece at sensor
    xppu.largesortingconveyor.DO_TurnCounterClockwise = False

# Move workpiece from positioning sensor 1 to Color Check 1 on LargeSortingConveyor
def lsc_moveWPfromPos1ToCC1(xppu, sensor=False):
    xppu.largesortingconveyor.DO_TurnClockwise = True
    for i in range(6): # Time based movement, due to missing sensor for black workpieces
        xppu.make_step(1)
        if sensor is True:
            print("DI_WPPosition1:", xppu.largesortingconveyor.DI_WPPosition1,
                  "DI_WPMetallic1:", xppu.largesortingconveyor.DI_WPMetallic1,
                  "DI_WPLight1:", xppu.largesortingconveyor.DI_WPLight1,
                  "VI_WPPosition", xppu.largesortingconveyor.VI_WPPosition)
    xppu.largesortingconveyor.DO_TurnClockwise = False

# Move workpiece from positioning sensor 1 to Color Check 2 on LargeSortingConveyor
def lsc_moveWPfromPos1ToCC2(xppu, sensor=False):
    xppu.largesortingconveyor.DO_TurnClockwise = True
    for i in range(24): # Time based movement, due to missing sensor for black workpieces
        xppu.make_step(1)
        if sensor is True:
            print("DI_WPPosition1:", xppu.largesortingconveyor.DI_WPPosition1,
                  "DI_WPMetallic1:", xppu.largesortingconveyor.DI_WPMetallic1,
                  "DI_WPLight1:", xppu.largesortingconveyor.DI_WPLight1,
                  "DI_WPMetallic2:", xppu.largesortingconveyor.DI_WPMetallic2,
                  "DI_WPLight2:", xppu.largesortingconveyor.DI_WPLight2,
                  "VI_WPPosition", xppu.largesortingconveyor.VI_WPPosition)
    xppu.largesortingconveyor.DO_TurnClockwise = False

# Move workpiece from positioning sensor 1 to ramp 1 on LargeSortingConveyor
def lsc_moveWPfromPos1ToRamp1(xppu, sensor=False):
    xppu.largesortingconveyor.DO_TurnClockwise = True
    for i in range(12):  # Time based movement, due to missing sensor
        xppu.make_step(1)
        if sensor is True:
            print("DI_WPPosition1:", xppu.largesortingconveyor.DI_WPPosition1,
                  "DI_WPMetallic1:", xppu.largesortingconveyor.DI_WPMetallic1,
                  "DI_WPLight1:", xppu.largesortingconveyor.DI_WPLight1,
                  "VI_WPPosition", xppu.largesortingconveyor.VI_WPPosition)
    xppu.largesortingconveyor.DO_TurnClockwise = False

# Move workpiece from positioning sensor 1 to ramp 2 on LargeSortingConveyor
def lsc_moveWPfromPos1ToRamp2(xppu, sensor=False):
    xppu.largesortingconveyor.DO_TurnClockwise = True
    for i in range(40):  # Time based movement, due to missing sensor
        xppu.make_step(1)
        if sensor is True:
            print("DI_WPPosition1:", xppu.largesortingconveyor.DI_WPPosition1,
                  "DI_WPMetallic1:", xppu.largesortingconveyor.DI_WPMetallic1,
                  "DI_WPLight1:", xppu.largesortingconveyor.DI_WPLight1,
                  "DI_WPMetallic2:", xppu.largesortingconveyor.DI_WPMetallic2,
                  "DI_WPLight2:", xppu.largesortingconveyor.DI_WPLight2,
                  "VI_WPPosition", xppu.largesortingconveyor.VI_WPPosition)
    xppu.largesortingconveyor.DO_TurnClockwise = False

# Move workpiece from any position to ramp 3 of the LargeSortingConveyor and sort it into the ramp
def lsc_moveAndSortWPToRamp3(xppu, sensor=False):
    if xppu.largesortingconveyor.ramp3.DI_RampFull is True:
        raise Error("Ramp3 is full, cannot put wp on ramp")
    xppu.largesortingconveyor.DO_TurnClockwise = True
    counter = 0
    while xppu.largesortingconveyor.DI_WPPosition2 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_WPPosition0:", xppu.largesortingconveyor.DI_WPPosition0,
                  "DI_WPPosition1:", xppu.largesortingconveyor.DI_WPPosition1,
                  "DI_WPPosition2:", xppu.largesortingconveyor.DI_WPPosition2,
                  "DI_WPMetallic1:", xppu.largesortingconveyor.DI_WPMetallic1,
                  "DI_WPLight1:", xppu.largesortingconveyor.DI_WPLight1,
                  "DI_WPMetallic2:", xppu.largesortingconveyor.DI_WPMetallic2,
                  "DI_WPLight2:", xppu.largesortingconveyor.DI_WPLight2,
                  "VI_WPPosition", xppu.largesortingconveyor.VI_WPPosition)
    xppu.make_step(5) # Time-based movement into ramp in order to be able to attempt sorting into full ramp
    xppu.largesortingconveyor.DO_TurnClockwise = False
    for i in range(15): # Some waiting to capture sliding in video
        xppu.make_step(1)
        if sensor is True:
            print("DI_RampFull:", xppu.largesortingconveyor.ramp3.DI_RampFull)

# Sort workpiece at ramp 1 of the LargeSortingConveyor into the ramp
def lsc_sortWPToRamp1(xppu, sensor=False):
    if xppu.largesortingconveyor.ramp1.DI_RampFull is True:
        raise Error("Ramp1 is full, cannot put wp on ramp")
    xppu.largesortingconveyor.separator1.DO_Extend = True
    counter = 0
    while xppu.largesortingconveyor.separator1.DI_Extended is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("AI_Position:", xppu.largesortingconveyor.separator1.AI_Position,
                  "DI_Extended:", xppu.largesortingconveyor.separator1.DI_Extended,
                  "DI_Retracted:", xppu.largesortingconveyor.separator1.DI_Retracted)
    xppu.largesortingconveyor.separator1.DO_Extend = False
    counter = 0
    while xppu.largesortingconveyor.separator1.DI_Retracted is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("AI_Position:", xppu.largesortingconveyor.separator1.AI_Position,
                  "DI_Extended:", xppu.largesortingconveyor.separator1.DI_Extended,
                  "DI_Retracted:", xppu.largesortingconveyor.separator1.DI_Retracted)
    for i in range(15):  # Some waiting to capture sliding in video
        xppu.make_step(1)
        if sensor is True:
            print("DI_RampFull:", xppu.largesortingconveyor.ramp1.DI_RampFull)

# Sort workpiece at ramp 2 of the LargeSortingConveyor into the ramp
def lsc_sortWPToRamp2(xppu, sensor=False):
    if xppu.largesortingconveyor.ramp2.DI_RampFull is True:
        raise Error("Ramp2 is full, cannot put wp on ramp")
    xppu.largesortingconveyor.separator2.DO_Extend = True
    counter = 0
    while xppu.largesortingconveyor.separator2.DI_Extended is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("AI_Position:", xppu.largesortingconveyor.separator2.AI_Position,
                  "DI_Extended:", xppu.largesortingconveyor.separator2.DI_Extended,
                  "DI_Retracted:", xppu.largesortingconveyor.separator2.DI_Retracted)
    xppu.largesortingconveyor.separator2.DO_Extend = False
    counter = 0
    while xppu.largesortingconveyor.separator2.DI_Retracted is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("AI_Position:", xppu.largesortingconveyor.separator2.AI_Position,
                  "DI_Extended:", xppu.largesortingconveyor.separator2.DI_Extended,
                  "DI_Retracted:", xppu.largesortingconveyor.separator2.DI_Retracted)
    for i in range(15):  # Some waiting to capture sliding in video
        xppu.make_step(1)
        if sensor is True:
            print("DI_RampFull:", xppu.largesortingconveyor.ramp2.DI_RampFull)

# Move workpiece from LargeSortingConveyor to the picking position on PickalphaConveyor
def lsc_moveWPToPickingPosOnPAC(xppu, sensor=False):
    xppu.largesortingconveyor.switch.DO_Extend = True
    counter = 0
    while xppu.largesortingconveyor.switch.DI_Extended is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_Extended:", xppu.largesortingconveyor.switch.DI_Extended,
                  "DI_Retracted:", xppu.largesortingconveyor.switch.DI_Retracted)
    CurrentSenVal = xppu.picalphaconveyor.AI_WPPosition # Get value without workpiece in range
    xppu.largesortingconveyor.DO_TurnClockwise = True
    xppu.picalphaconveyor.DO_TurnClockwise = True
    counter = 0
    while xppu.picalphaconveyor.AI_WPPosition >= CurrentSenVal and counter < threshold_large: # Move workpiece into range of ultrasonic sensor
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_WPPosition0:", xppu.largesortingconveyor.DI_WPPosition0,
                  "DI_WPPosition1:", xppu.largesortingconveyor.DI_WPPosition1,
                  "DI_WPMetallic1:", xppu.largesortingconveyor.DI_WPMetallic1,
                  "DI_WPLight1:", xppu.largesortingconveyor.DI_WPLight1,
                  "DI_WPMetallic2:", xppu.largesortingconveyor.DI_WPMetallic2,
                  "DI_WPLight2:", xppu.largesortingconveyor.DI_WPLight2,
                  "VI_WPPosition", xppu.largesortingconveyor.VI_WPPosition,
                  "AI_WPPosition:", xppu.picalphaconveyor.AI_WPPosition,
                  "VI_WPPosition", xppu.picalphaconveyor.VI_WPPosition)
    # Move until detected by positioning sensor, if first workpiece on PicAlphaConveyor, or until the measured distance
    # of the ultrasonic sensor is reduced by the workpiece diameter (diameter = 5)
    counter = 0
    if xppu.picalphaconveyor.DI_WPPosition1 is False:
        while xppu.picalphaconveyor.DI_WPPosition1 is False and counter < threshold_small:
            counter += 1
            xppu.make_step(1)
            if sensor is True:
                print("DI_WPPosition1:", xppu.picalphaconveyor.DI_WPPosition1,
                      "AI_WPPosition:", xppu.picalphaconveyor.AI_WPPosition,
                      "VI_WPPosition", xppu.picalphaconveyor.VI_WPPosition)
        xppu.make_step(2)  # Center workpiece at sensor
    else:
        while xppu.picalphaconveyor.AI_WPPosition < CurrentSenVal-5 and counter < threshold_small:
            counter += 1
            xppu.make_step(1)
            if sensor is True:
                print("DI_WPPosition1:", xppu.picalphaconveyor.DI_WPPosition1,
                      "AI_WPPosition:", xppu.picalphaconveyor.AI_WPPosition,
                      "VI_WPPosition", xppu.picalphaconveyor.VI_WPPosition)
    xppu.largesortingconveyor.DO_TurnClockwise = False
    xppu.picalphaconveyor.DO_TurnClockwise = False
    counter = 0
    xppu.largesortingconveyor.switch.DO_Extend = False
    while xppu.largesortingconveyor.switch.DI_Retracted is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_Extended:", xppu.largesortingconveyor.switch.DI_Extended,
                  "DI_Retracted:", xppu.largesortingconveyor.switch.DI_Retracted)

# Move workpiece forward to positioning sensor 2 on PicALphaConveyor
def pac_moveWPForwardToPos2(xppu, sensor=False):
    xppu.picalphaconveyor.DO_TurnClockwise = True
    counter = 0
    while xppu.picalphaconveyor.DI_WPPosition2 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_WPPosition1:", xppu.picalphaconveyor.DI_WPPosition1,
                  "DI_WPPosition2:", xppu.picalphaconveyor.DI_WPPosition2,
                  "AI_WPPosition:", xppu.picalphaconveyor.AI_WPPosition,
                  "VI_WPPosition", xppu.picalphaconveyor.VI_WPPosition)
    xppu.make_step(2)  # Center workpiece at sensor
    xppu.picalphaconveyor.DO_TurnClockwise = False

# Move workpiece back to positioning sensor 1 on PicALphaConveyor
def pac_moveWPBackToPos1(xppu, sensor=False):
    xppu.picalphaconveyor.DO_TurnCounterClockwise = True
    counter = 0
    while xppu.picalphaconveyor.DI_WPPosition1 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_WPPosition1:", xppu.picalphaconveyor.DI_WPPosition1,
                  "DI_WPPosition2:", xppu.picalphaconveyor.DI_WPPosition2,
                  "AI_WPPosition:", xppu.picalphaconveyor.AI_WPPosition,
                  "VI_WPPosition", xppu.picalphaconveyor.VI_WPPosition)
    xppu.make_step(2)  # Center workpiece at sensor
    xppu.picalphaconveyor.DO_TurnCounterClockwise = False

# Move workpiece from the PickAlphaConveyor to the positioning sensor 1 on LargeSortingConveyor
def pac_moveWPToPos1OnLSC(xppu, sensor=False):
    xppu.picalphaconveyor.DO_TurnCounterClockwise = True
    xppu.largesortingconveyor.DO_TurnCounterClockwise = True
    counter = 0
    while xppu.largesortingconveyor.DI_WPPosition1 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("PAC.DI_WPPosition2:", xppu.picalphaconveyor.DI_WPPosition2,
                  "PAC.DI_WPPosition1:", xppu.picalphaconveyor.DI_WPPosition1,
                  "PAC.VI_WPPosition", xppu.picalphaconveyor.VI_WPPosition,
                  "LSC.DI_WPLight2:", xppu.largesortingconveyor.DI_WPLight2,
                  "LSC.DI_WPMetallic2:", xppu.largesortingconveyor.DI_WPMetallic2,
                  "LSC.DI_WPLight1:", xppu.largesortingconveyor.DI_WPLight1,
                  "LSC.DI_WPMetallic1:", xppu.largesortingconveyor.DI_WPMetallic1,
                  "LSC.DI_WPPosition1:", xppu.largesortingconveyor.DI_WPPosition1,
                  "LSC.WPPosition", xppu.largesortingconveyor.VI_WPPosition)
    xppu.make_step(2) # Center workpiece at sensor
    xppu.picalphaconveyor.DO_TurnCounterClockwise = False
    xppu.largesortingconveyor.DO_TurnCounterClockwise = False

# Move workpiece from the PicalphaConveyor to the positioning sensor 1 on SmallSortingConveyor
def pac_moveWPToPos1OnSSC(xppu, sensor=False):
    xppu.picalphaconveyor.DO_TurnClockwise = True
    xppu.smallsortingconveyor.DO_TurnClockwise = True
    counter =  0
    while xppu.smallsortingconveyor.DI_WPPosition1 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("PAC.AI_WPPosition:", xppu.picalphaconveyor.AI_WPPosition,
                  "PAC.DI_WPPosition1:", xppu.picalphaconveyor.DI_WPPosition1,
                  "PAC.DI_WPPosition2:", xppu.picalphaconveyor.DI_WPPosition2,
                  "PAC.VI_WPPosition", xppu.picalphaconveyor.VI_WPPosition,
                  "SSC.DI_WPPosition1:", xppu.smallsortingconveyor.DI_WPPosition1,
                  "SSC.VI_WPPosition", xppu.smallsortingconveyor.VI_WPPosition)
    xppu.make_step(2)  # Center workpiece at sensor
    xppu.picalphaconveyor.DO_TurnClockwise = False
    xppu.smallsortingconveyor.DO_TurnClockwise = False

# Move workpiece from positioning sensor 1 to Color Check on SmallSortingConveyor
def ssc_moveWPfromPos1ToCC(xppu, sensor=False):
    xppu.smallsortingconveyor.DO_TurnClockwise = True
    for i in range(20): # Time based movement, due to missing sensor for black workpieces
        xppu.make_step(1)
        if sensor is True:
            print("DI_WPPosition1:", xppu.smallsortingconveyor.DI_WPPosition1,
                  "DI_WPLight1:", xppu.smallsortingconveyor.DI_WPLight1,
                  "DI_WPMetallic1:", xppu.smallsortingconveyor.DI_WPMetallic1,
                  "VI_WPPosition", xppu.smallsortingconveyor.VI_WPPosition)
    xppu.smallsortingconveyor.DO_TurnClockwise = False

# Move workpiece forward to positioning sensor 2 on SmallSortingConveyor
def ssc_moveWPForwardToPos2(xppu, sensor=False):
    xppu.smallsortingconveyor.DO_TurnClockwise = True
    counter = 0
    while xppu.smallsortingconveyor.DI_WPPosition2 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_WPPosition1:", xppu.smallsortingconveyor.DI_WPPosition1,
                  "DI_WPLight1:", xppu.smallsortingconveyor.DI_WPLight1,
                  "DI_WPMetallic1:", xppu.smallsortingconveyor.DI_WPMetallic1,
                  "DI_WPPosition2:", xppu.smallsortingconveyor.DI_WPPosition2,
                  "VI_WPPosition", xppu.smallsortingconveyor.VI_WPPosition)
    xppu.make_step(2)  # Center workpiece at sensor
    xppu.smallsortingconveyor.DO_TurnClockwise = False

# Move workpiece forward to positioning sensor 3 on SmallSortingConveyor
def ssc_moveWPForwardToPos3(xppu, sensor=False):
    xppu.smallsortingconveyor.DO_TurnClockwise = True
    counter = 0
    while xppu.smallsortingconveyor.DI_WPPosition3 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_WPPosition1:", xppu.smallsortingconveyor.DI_WPPosition1,
                  "DI_WPLight1:", xppu.smallsortingconveyor.DI_WPLight1,
                  "DI_WPMetallic1:", xppu.smallsortingconveyor.DI_WPMetallic1,
                  "DI_WPPosition2:", xppu.smallsortingconveyor.DI_WPPosition2,
                  "DI_WPPosition3:", xppu.smallsortingconveyor.DI_WPPosition3,
                  "VI_WPPosition", xppu.smallsortingconveyor.VI_WPPosition)
    xppu.make_step(2)  # Center workpiece at sensor
    xppu.smallsortingconveyor.DO_TurnClockwise = False

# Move workpiece back to positioning sensor 2 on SmallSortingConveyor
def ssc_moveWPBackToPos2(xppu, sensor=False):
    xppu.smallsortingconveyor.DO_TurnCounterClockwise = True
    counter = 0
    while xppu.smallsortingconveyor.DI_WPPosition2 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_WPPosition2:", xppu.smallsortingconveyor.DI_WPPosition2,
                  "DI_WPPosition3:", xppu.smallsortingconveyor.DI_WPPosition3,
                  "VI_WPPosition", xppu.smallsortingconveyor.VI_WPPosition)
    xppu.make_step(2)  # Center workpiece at sensor
    xppu.smallsortingconveyor.DO_TurnCounterClockwise = False

# Move workpiece back to positioning sensor 1 on SmallSortingConveyor
def ssc_moveWPBackToPos1(xppu, sensor=False):
    xppu.smallsortingconveyor.DO_TurnCounterClockwise = True
    counter = 0
    while xppu.smallsortingconveyor.DI_WPPosition1 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_WPPosition1:", xppu.smallsortingconveyor.DI_WPPosition1,
                  "DI_WPLight1:", xppu.smallsortingconveyor.DI_WPLight1,
                  "DI_WPMetallic1:", xppu.smallsortingconveyor.DI_WPMetallic1,
                  "DI_WPPosition2:", xppu.smallsortingconveyor.DI_WPPosition2,
                  "DI_WPPosition3:", xppu.smallsortingconveyor.DI_WPPosition3,
                  "VI_WPPosition", xppu.smallsortingconveyor.VI_WPPosition)
    xppu.make_step(2)  # Center workpiece at sensor
    xppu.smallsortingconveyor.DO_TurnCounterClockwise = False

# Move workpiece from any position to ramp 4 of the SmallSortingConveyor and sort it into the ramp
def ssc_moveAndSortWPToRamp4(xppu, sensor=False):
    if xppu.smallsortingconveyor.ramp1.DI_RampFull is True:
        raise Error("Ramp4 is full, cannot put wp on ramp")
    xppu.smallsortingconveyor.DO_TurnClockwise = True
    counter = 0
    while xppu.smallsortingconveyor.DI_WPPosition3 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_WPPosition1:", xppu.smallsortingconveyor.DI_WPPosition1,
                  "DI_WPPosition2:", xppu.smallsortingconveyor.DI_WPPosition2,
                  "DI_WPPosition3:", xppu.smallsortingconveyor.DI_WPPosition3,
                  "VI_WPPosition", xppu.smallsortingconveyor.VI_WPPosition,
                  "DI_WPLight1:", xppu.smallsortingconveyor.DI_WPLight1,
                  "DI_WPMetallic1:", xppu.smallsortingconveyor.DI_WPMetallic1)
    xppu.make_step(5) # Time-based movement into ramp in order to be able to attempt sorting into full ramp
    xppu.smallsortingconveyor.DO_TurnClockwise = False
    for i in range(5):  # Some waiting to capture sliding in video
        xppu.make_step(1)
        if sensor is True:
            print("DI_RampFull:", xppu.smallsortingconveyor.ramp1.DI_RampFull)

# Move workpiece from the SmallSortingConveyor to the positioning sensor 2 on PicAlphaConveyor
def ssc_moveWPToPos2OnPAC(xppu, sensor=False):
    xppu.smallsortingconveyor.DO_TurnCounterClockwise = True
    xppu.picalphaconveyor.DO_TurnCounterClockwise = True
    counter = 0
    while xppu.picalphaconveyor.DI_WPPosition2 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("SSC.DI_WPPosition3:", xppu.smallsortingconveyor.DI_WPPosition3,
                  "SSC.DI_WPPosition2:", xppu.smallsortingconveyor.DI_WPPosition2,
                  "SSC.DI_WPPosition1:", xppu.smallsortingconveyor.DI_WPPosition1,
                  "SSC.DI_WPLight1:", xppu.smallsortingconveyor.DI_WPLight1,
                  "SSC.DI_WPMetallic1:", xppu.smallsortingconveyor.DI_WPMetallic1,
                  "SSC.VI_WPPosition", xppu.smallsortingconveyor.VI_WPPosition,
                  "PAC.DI_WPPosition2:", xppu.picalphaconveyor.DI_WPPosition2,
                  "PAC.VI_WPPosition", xppu.picalphaconveyor.VI_WPPosition)
    xppu.make_step(2) # Center workpiece at sensor
    xppu.smallsortingconveyor.DO_TurnCounterClockwise = False
    xppu.picalphaconveyor.DO_TurnCounterClockwise = False

# Move workpiece from the SmallSortingConveyor to the positioning sensor 1 on RefillConveyor
def ssc_moveWPToPos1OnRFC(xppu, sensor=False):
    counter = 0
    xppu.smallsortingconveyor.switch.DO_Extend = True
    while xppu.smallsortingconveyor.switch.DI_Extended is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_Extended:", xppu.smallsortingconveyor.switch.DI_Extended,
                  "DI_Retracted:", xppu.smallsortingconveyor.switch.DI_Retracted)
    xppu.smallsortingconveyor.DO_TurnClockwise = True
    xppu.refillconveyor.DO_TurnClockwise = True
    counter =  0
    while xppu.refillconveyor.DI_WPPosition1 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("SSC.DI_WPPosition1:", xppu.smallsortingconveyor.DI_WPPosition1,
                  "SSC.DI_WPLight1:", xppu.smallsortingconveyor.DI_WPLight1,
                  "SSC.DI_WPMetallic1:", xppu.smallsortingconveyor.DI_WPMetallic1,
                  "SSC.DI_WPPosition2:", xppu.smallsortingconveyor.DI_WPPosition2,
                  "SSC.VI_WPPosition", xppu.smallsortingconveyor.VI_WPPosition,
                  "RFC.WPPosition1:", xppu.refillconveyor.DI_WPPosition1,
                  "RFC.VI_WPPosition", xppu.refillconveyor.VI_WPPosition)
    xppu.make_step(2)  # Center workpiece at sensor
    xppu.refillconveyor.DO_TurnClockwise = False
    xppu.smallsortingconveyor.DO_TurnClockwise = False
    counter = 0
    xppu.smallsortingconveyor.switch.DO_Extend = False
    while xppu.smallsortingconveyor.switch.DI_Retracted is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_Extended:", xppu.smallsortingconveyor.switch.DI_Extended,
                  "DI_Retracted:", xppu.smallsortingconveyor.switch.DI_Retracted)

# Move workpiece forward to positioning sensor 2 on RefillConveyor
def rfc_moveWPForwardToPos2(xppu, sensor=False):
    xppu.refillconveyor.DO_TurnClockwise = True
    counter = 0
    while xppu.refillconveyor.DI_WPPosition2 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("RFC.DI_WPPosition1:", xppu.refillconveyor.DI_WPPosition1,
                  "RFC.DI_WPPosition2:", xppu.refillconveyor.DI_WPPosition2,
                  "RFC.VI_WPPosition", xppu.refillconveyor.VI_WPPosition)
    xppu.make_step(2)  # Center workpiece at sensor
    xppu.refillconveyor.DO_TurnClockwise = False

# Move workpiece back to positioning sensor 1 on RefillConveyor
def rfc_moveWPBackToPos1(xppu, sensor=False):
    xppu.refillconveyor.DO_TurnCounterClockwise = True
    counter = 0
    while xppu.refillconveyor.DI_WPPosition1 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("RFC.DI_WPPosition1:", xppu.refillconveyor.DI_WPPosition1,
                  "RFC.DI_WPPosition2:", xppu.refillconveyor.DI_WPPosition2,
                  "RFC.VI_WPPosition", xppu.refillconveyor.VI_WPPosition)
    xppu.make_step(2)  # Center workpiece at sensor
    xppu.refillconveyor.DO_TurnCounterClockwise = False

# Move workpiece from the RefillConveyor to the positioning sensor 2 on SmallSortingConveyor
def rfc_moveWPToPos2OnSSC(xppu, sensor=False):
    xppu.refillconveyor.DO_TurnCounterClockwise = True
    xppu.smallsortingconveyor.DO_TurnCounterClockwise = True
    counter = 0
    while xppu.smallsortingconveyor.DI_WPPosition2 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("RFC.DI_WPPosition2:", xppu.refillconveyor.DI_WPPosition2,
                  "RFC.DI_WPPosition1:", xppu.refillconveyor.DI_WPPosition1,
                  "RFC.VI_WPPosition", xppu.refillconveyor.VI_WPPosition,
                  "SSC.DI_WPPosition2:", xppu.smallsortingconveyor.DI_WPPosition2,
                  "SSC.VI_WPPosition", xppu.smallsortingconveyor.VI_WPPosition)
    xppu.make_step(2)  # Center workpiece at sensor
    xppu.refillconveyor.DO_TurnCounterClockwise = False
    xppu.smallsortingconveyor.DO_TurnCounterClockwise = False

# Move workpiece from the RefillCOnveyor to the positioning sensor 0 on LargeSortingConveyor
def rfc_moveWPToPos0OnLSC(xppu, sensor=False):
    xppu.refillconveyor.DO_TurnClockwise = True
    counter = 0
    while xppu.largesortingconveyor.DI_WPPosition0 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("RFC.DI_WPPosition1:", xppu.refillconveyor.DI_WPPosition1,
                  "RFC.DI_WPPosition2:", xppu.refillconveyor.DI_WPPosition2,
                  "RFC.VI_WPPosition", xppu.refillconveyor.VI_WPPosition,
                  "LSC.DI_WPPosition0:", xppu.largesortingconveyor.DI_WPPosition0,
                  "LSC.VI_WPPosition", xppu.largesortingconveyor.VI_WPPosition)
    xppu.refillconveyor.DO_TurnClockwise = False

# Move PicAlpha back to second workpiece behind switch
def picalpha_moveBackToPickupPos1(xppu, sensor=False):
    xppu.picalpha.carrier.DO_TurnCounterClockwise = True
    counter = 0
    while xppu.picalpha.carrier.DI_AtPickup1 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_AtEndRight:", xppu.picalpha.carrier.DI_AtEndRight,
                  "DI_AtEndLeft:", xppu.picalpha.carrier.DI_AtEndLeft,
                  "DI_AtPickup1:", xppu.picalpha.carrier.DI_AtPickup1,
                  "DI_AtPickup2:", xppu.picalpha.carrier.DI_AtPickup2,
                  "DI_AtPutDown1:", xppu.picalpha.carrier.DI_AtPutDown1,
                  "DI_AtPutDown2:", xppu.picalpha.carrier.DI_AtPutDown2)
    xppu.picalpha.carrier.DO_TurnCounterClockwise = False

# Move PicAlpha back to third workpiece behind switch
def picalpha_moveBackToPickupPos2(xppu, sensor=False):
    xppu.picalpha.carrier.DO_TurnCounterClockwise = True
    counter = 0
    while xppu.picalpha.carrier.DI_AtPickup2 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_AtEndRight:", xppu.picalpha.carrier.DI_AtEndRight,
                  "DI_AtEndLeft:", xppu.picalpha.carrier.DI_AtEndLeft,
                  "DI_AtPickup1:", xppu.picalpha.carrier.DI_AtPickup1,
                  "DI_AtPickup2:", xppu.picalpha.carrier.DI_AtPickup2,
                  "DI_AtPutDown1:", xppu.picalpha.carrier.DI_AtPutDown1,
                  "DI_AtPutDown2:", xppu.picalpha.carrier.DI_AtPutDown2)
    xppu.picalpha.carrier.DO_TurnCounterClockwise = False

# Move PicAlpha back the putdown position in the middle of the conveyor
def picalpha_moveBackToPutdownPos1(xppu, sensor=False):
    xppu.picalpha.carrier.DO_TurnCounterClockwise = True
    counter = 0
    while xppu.picalpha.carrier.DI_AtPutDown1 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_AtEndRight:", xppu.picalpha.carrier.DI_AtEndRight,
                  "DI_AtPutDown1:", xppu.picalpha.carrier.DI_AtPutDown1,
                  "DI_AtPutDown2:", xppu.picalpha.carrier.DI_AtPutDown2)
    xppu.picalpha.carrier.DO_TurnCounterClockwise = False

# Move PicAlpha forward to second workpiece behind switch
def picalpha_moveForwardToPickupPos1(xppu, sensor=False):
    xppu.picalpha.carrier.DO_TurnClockwise = True
    counter = 0
    while xppu.picalpha.carrier.DI_AtPickup1 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_AtEndLeft:", xppu.picalpha.carrier.DI_AtEndLeft,
                  "DI_AtPickup1:", xppu.picalpha.carrier.DI_AtPickup1,
                  "DI_AtPickup2:", xppu.picalpha.carrier.DI_AtPickup2)
    xppu.picalpha.carrier.DO_TurnClockwise = False

# Move PicAlpha forward to the putdown position in the middle of the conveyor
def picalpha_moveForwardToPutdownPos1(xppu, sensor=False):
    xppu.picalpha.carrier.DO_TurnClockwise = True
    counter = 0
    while xppu.picalpha.carrier.DI_AtPutDown1 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_AtEndLeft:", xppu.picalpha.carrier.DI_AtEndLeft,
                  "DI_AtPickup1:", xppu.picalpha.carrier.DI_AtPickup1,
                  "DI_AtPickup2:", xppu.picalpha.carrier.DI_AtPickup2,
                  "DI_AtPutDown1:", xppu.picalpha.carrier.DI_AtPutDown1)
    xppu.picalpha.carrier.DO_TurnClockwise = False

# Move PicAlpha forward to the put down position at the end of the conveyor
def picalpha_moveForwardToPutdownPos2(xppu, sensor=False):
    xppu.picalpha.carrier.DO_TurnClockwise = True
    counter = 0
    while xppu.picalpha.carrier.DI_AtPutDown2 is False and counter < threshold_large:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_AtEndLeft:", xppu.picalpha.carrier.DI_AtEndLeft,
                  "DI_AtPickup1:", xppu.picalpha.carrier.DI_AtPickup1,
                  "DI_AtPickup2:", xppu.picalpha.carrier.DI_AtPickup2,
                  "DI_AtPutDown1:", xppu.picalpha.carrier.DI_AtPutDown1,
                  "DI_AtPutDown2:", xppu.picalpha.carrier.DI_AtPutDown2)
    xppu.picalpha.carrier.DO_TurnClockwise = False

# Pick up workpiece with PickAlpha at current position
def picalpha_pickupWP(xppu, sensor=False):
    xppu.picalpha.liftingCylinder.DO_Extend = True
    counter = 0
    while xppu.picalpha.liftingCylinder.DI_Extended is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_Extended:", xppu.picalpha.liftingCylinder.DI_Extended,
                  "DI_Retracted:", xppu.picalpha.liftingCylinder.DI_Retracted,
                  "AI_Position:", xppu.picalpha.liftingCylinder.AI_Position,
                  "VI_WPPosition", xppu.picalphaconveyor.VI_WPPosition)
    xppu.picalpha.DO_Intake = True
    counter = 0
    while xppu.picalpha.DI_TakenIn is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_TakenIn:", xppu.picalpha.DI_TakenIn)
    xppu.picalpha.liftingCylinder.DO_Extend = False
    counter = 0
    while xppu.picalpha.liftingCylinder.DI_Retracted is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_Extended:", xppu.picalpha.liftingCylinder.DI_Extended,
                  "DI_Retracted:", xppu.picalpha.liftingCylinder.DI_Retracted,
                  "AI_Position:", xppu.picalpha.liftingCylinder.AI_Position,
                  "VI_WPPosition", xppu.picalphaconveyor.VI_WPPosition)

# Put down workpiece with PickAlpha at current position
def picalpha_putdownWP(xppu, sensor=False):
    xppu.picalpha.liftingCylinder.DO_Extend = True
    counter = 0
    while xppu.picalpha.liftingCylinder.DI_Extended is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_Extended:", xppu.picalpha.liftingCylinder.DI_Extended,
                  "DI_Retracted:", xppu.picalpha.liftingCylinder.DI_Retracted,
                  "AI_Position:", xppu.picalpha.liftingCylinder.AI_Position,
                  "VI_WPPosition", xppu.picalphaconveyor.VI_WPPosition)
    xppu.picalpha.DO_Intake = False
    while xppu.picalpha.DI_TakenIn is True and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_TakenIn:", xppu.picalpha.DI_TakenIn)
    xppu.picalpha.liftingCylinder.DO_Extend = False
    counter = 0
    while xppu.picalpha.liftingCylinder.DI_Retracted is False and counter < threshold_small:
        counter += 1
        xppu.make_step(1)
        if sensor is True:
            print("DI_Extended:", xppu.picalpha.liftingCylinder.DI_Extended,
                  "DI_Retracted:", xppu.picalpha.liftingCylinder.DI_Retracted,
                  "AI_Position:", xppu.picalpha.liftingCylinder.AI_Position,
                  "VI_WPPosition", xppu.picalphaconveyor.VI_WPPosition)

