# Overview of test scenarios included in this module

#TEST SCENARIO:     DESCRIPTION:
#standard_1:		Sort single workpiece into ramp1 without stamping
#standard_2:		Sort single workpiece into ramp2 without stamping
#standard_3:		Sort single workpiece into ramp3 without stamping
#standard_4:		Stamp single workpiece and sort into ramp 1
#standard_4a:		Stamp single workpiece and sort into ramp 2
#standard_4b:		Stamp single workpiece and sort into ramp 3
#standard_5: 		Sort workpieces into ramps 1-3 depending on type using ColorCheck 1 on LargeSortingConveyor, metallic workpieces also gets stamped
#standard_6:		Sort workpieces int ramps 1-3 depending on type using ColorCheck 2 on LargeSortingConveyor, metallic workpieces also gets stamped
#standard_7:		Circle single workpiece around and sort into ramp 1
#standard_8: 		Sort single workpiece into ramp4 without stamping
#standard_9:		Change order of 3 workpieces with PicAlpha and sort into ramp 4
#standard_10: 		Circle workpieces around and sort them into ramps 1-3 depending on type using color check 1 on SmallSortingConveyor, metallic workpieces also gets stamped

#nonstandard_1:		Block workpiece movement on LargeSortingConveyor with separation cylinders and switch
#nonstandard_2: 	Move workpiece to RefillConveyor, return it to LargeSortingConveyor, and sort it into ramp 1
#nonstandard_3: 	Block workpiece movement on RefillConveyor, SmallSortingConveyor and PicAlphaConveyor with switch
#nonstandard_4: 	Attempt to sort workpiece into full ramp 1
#nonstandard_5:		Attempt to sort workpiece into full ramp 2
#nonstandard_6:		Attempt to sort workpiece into full ramp 3
#nonstandard_7: 	Attempt to sort workpiece into full ramp 4
#nonstandard_8:		Store 3 workpieces on RefillConveyor first, on PicAlphaConveyor second, and sort them into ramp 3 third
#nonstandard_9:		Clamp and unclamp workpiece at the stack
#nonstandard_10:    Cycle workpiece on conveyer system before stamping it

#error_1:		Crane turns without extending first
#error_2:		Crane drops workpiece in mid-air
#error_3:		Crane attempts to stack workpieces on LargeSortingConveyor
#error_4:		Workpiece is put down on the stamp when the slidingCylinder is retracted
#error_5:		SlidingCylinder of stamp is retracting while the workpiece is still attached to crane
#error_6:		LargeSortingConveyor moves while the workpiece is still attached to crane
#error_7:		Workpiece is moved of the LargeSortingConveyor
#error_8:		PicAlpha moves without retracting first and collides with workpiece
#error_9:		PicAlpha stacks workpieces on PicAlphaConveyor
#error_10:		PicAlpha drops workpiece in mid-air


# Import high-level functions to build test cases
from xppusim.high_level_func import *

# -- Standard test cases --

# Sort single workpiece into ramp1 without stamping
def standard_1(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_moveToStack(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToConveyor(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    lsc_moveWPForwardToPos1(xppu, sensor)
    lsc_moveWPfromPos1ToRamp1(xppu, sensor)
    lsc_sortWPToRamp1(xppu, sensor)

# Sort single workpiece into ramp2 without stamping
def standard_2(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_moveToStack(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToConveyor(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    lsc_moveWPForwardToPos1(xppu, sensor)
    lsc_moveWPfromPos1ToRamp2(xppu, sensor)
    lsc_sortWPToRamp2(xppu, sensor)

# Sort single workpiece into ramp3 without stamping
def standard_3(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_moveToStack(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToConveyor(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    lsc_moveAndSortWPToRamp3(xppu, sensor)

# Stamp single workpiece and sort into ramp 1
def standard_4(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_moveToStack(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToStamp(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    stamp_stampWP(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToConveyor(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    lsc_moveWPForwardToPos1(xppu, sensor)
    lsc_moveWPfromPos1ToRamp1(xppu, sensor)
    lsc_sortWPToRamp1(xppu, sensor)


# Stamp single workpiece and sort into ramp 1
def standard_4a(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_moveToStack(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToStamp(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    stamp_stampWP(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToConveyor(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    lsc_moveWPForwardToPos1(xppu, sensor)
    lsc_moveWPfromPos1ToRamp2(xppu, sensor)
    lsc_sortWPToRamp2(xppu, sensor)


# Stamp single workpiece and sort into ramp 1
def standard_4b(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_moveToStack(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToStamp(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    stamp_stampWP(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToConveyor(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    lsc_moveAndSortWPToRamp3(xppu, sensor)


# Sort workpieces into ramps 1-3 depending on type using ColorCheck 1 on LargeSortingConveyor, metallic workpieces also gets stamped
def standard_5(xppu, sensor=False):
    for i in range(5):
        stack_separateWP(xppu, sensor)
        crane_moveToStack(xppu, sensor)
        if xppu.stack.DI_WPProvided is True:
            if xppu.stack.DI_WPMetallic is True:
                crane_moveToStack(xppu, sensor)
                crane_pickupWP(xppu, sensor)
                crane_moveToStamp(xppu, sensor)
                crane_putdownWP(xppu, sensor)
                stamp_stampWP(xppu, sensor)
                crane_pickupWP(xppu, sensor)
                crane_moveToConveyor(xppu, sensor)
                crane_putdownWP(xppu, sensor)
            elif xppu.stack.DI_WPProvided is True:
                crane_moveToStack(xppu, sensor)
                crane_pickupWP(xppu, sensor)
                crane_moveToConveyor(xppu, sensor)
                crane_putdownWP(xppu, sensor)
            lsc_moveWPForwardToPos1(xppu, sensor)
            lsc_moveWPfromPos1ToCC1(xppu, sensor)
            if xppu.largesortingconveyor.DI_WPMetallic1 is True:
                lsc_moveWPBackToPos1(xppu, sensor)
                lsc_moveWPfromPos1ToRamp1(xppu, sensor)
                lsc_sortWPToRamp1(xppu, sensor)
            elif xppu.largesortingconveyor.DI_WPLight1 is True:
                lsc_moveWPBackToPos1(xppu, sensor)
                lsc_moveWPfromPos1ToRamp2(xppu, sensor)
                lsc_sortWPToRamp2(xppu, sensor)
            else:
                lsc_moveAndSortWPToRamp3(xppu, sensor)

# Sort workpieces into ramps 1-3 depending on type using ColorCheck 2 on LargeSortingConveyor, metallic workpieces also gets stamped
def standard_6(xppu, sensor=False):
    for i in range(5):
        stack_separateWP(xppu, sensor)
        crane_moveToStack(xppu, sensor)
        if xppu.stack.DI_WPProvided is True:
            if xppu.stack.DI_WPMetallic is True:
                crane_moveToStack(xppu, sensor)
                crane_pickupWP(xppu, sensor)
                crane_moveToStamp(xppu, sensor)
                crane_putdownWP(xppu, sensor)
                stamp_stampWP(xppu, sensor)
                crane_pickupWP(xppu, sensor)
                crane_moveToConveyor(xppu, sensor)
                crane_putdownWP(xppu, sensor)
            elif xppu.stack.DI_WPProvided is True:
                crane_moveToStack(xppu, sensor)
                crane_pickupWP(xppu, sensor)
                crane_moveToConveyor(xppu, sensor)
                crane_putdownWP(xppu, sensor)
            lsc_moveWPForwardToPos1(xppu, sensor)
            lsc_moveWPfromPos1ToCC2(xppu, sensor)
            if xppu.largesortingconveyor.DI_WPMetallic2 is True:
                lsc_moveWPBackToPos1(xppu, sensor)
                lsc_moveWPfromPos1ToRamp1(xppu, sensor)
                lsc_sortWPToRamp1(xppu, sensor)
            elif xppu.largesortingconveyor.DI_WPLight2 is True:
                lsc_moveWPBackToPos1(xppu, sensor)
                lsc_moveWPfromPos1ToRamp2(xppu, sensor)
                lsc_sortWPToRamp2(xppu, sensor)
            else:
                lsc_moveAndSortWPToRamp3(xppu, sensor)

# Circle single workpiece around and sort into ramp 1
def standard_7(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_moveToStack(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToConveyor(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    lsc_moveWPToPickingPosOnPAC(xppu, sensor)
    pac_moveWPForwardToPos2(xppu, sensor)
    pac_moveWPToPos1OnSSC(xppu, sensor)
    ssc_moveWPToPos1OnRFC(xppu, sensor)
    rfc_moveWPForwardToPos2(xppu, sensor)
    rfc_moveWPToPos0OnLSC(xppu, sensor)
    lsc_moveWPForwardToPos1(xppu, sensor)
    lsc_moveWPfromPos1ToRamp1(xppu, sensor)
    lsc_sortWPToRamp1(xppu, sensor)

# Sort single workpiece into ramp4 without stamping
def standard_8(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_moveToStack(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToConveyor(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    lsc_moveWPToPickingPosOnPAC(xppu, sensor)
    pac_moveWPForwardToPos2(xppu, sensor)
    pac_moveWPToPos1OnSSC(xppu, sensor)
    ssc_moveAndSortWPToRamp4(xppu, sensor)

# Change order of 3 workpieces with PicAlpha and sort into ramp 4
def standard_9(xppu, sensor=False):
    for i in range(3):
        stack_separateWP(xppu, sensor)
        crane_moveToStack(xppu, sensor)
        crane_pickupWP(xppu, sensor)
        crane_moveToConveyor(xppu, sensor)
        crane_putdownWP(xppu, sensor)
        lsc_moveWPToPickingPosOnPAC(xppu, sensor)
    picalpha_moveBackToPickupPos2(xppu, sensor)
    picalpha_pickupWP(xppu, sensor)
    picalpha_moveForwardToPutdownPos1(xppu, sensor)
    picalpha_putdownWP(xppu, sensor)
    pac_moveWPToPos1OnSSC(xppu, sensor)
    ssc_moveAndSortWPToRamp4(xppu, sensor)
    picalpha_moveBackToPickupPos1(xppu, sensor)
    picalpha_pickupWP(xppu, sensor)
    picalpha_moveForwardToPutdownPos2(xppu, sensor)
    picalpha_putdownWP(xppu, sensor)
    pac_moveWPToPos1OnSSC(xppu, sensor)
    ssc_moveAndSortWPToRamp4(xppu, sensor)
    pac_moveWPForwardToPos2(xppu, sensor)
    pac_moveWPToPos1OnSSC(xppu, sensor)
    ssc_moveAndSortWPToRamp4(xppu, sensor)

# Circle workpieces around and sort them into ramps 1-3 depending on type using color check 1 on SmallSortingConveyor, metallic workpieces also gets stamped
def standard_10(xppu, sensor=False):
    for i in range(5):
        stack_separateWP(xppu, sensor)
        crane_moveToStack(xppu, sensor)
        if xppu.stack.DI_WPProvided is True:
            if xppu.stack.DI_WPMetallic is True:
                crane_moveToStack(xppu, sensor)
                crane_pickupWP(xppu, sensor)
                crane_moveToStamp(xppu, sensor)
                crane_putdownWP(xppu, sensor)
                stamp_stampWP(xppu, sensor)
                crane_pickupWP(xppu, sensor)
                crane_moveToConveyor(xppu, sensor)
                crane_putdownWP(xppu, sensor)
            elif xppu.stack.DI_WPProvided is True:
                crane_moveToStack(xppu, sensor)
                crane_pickupWP(xppu, sensor)
                crane_moveToConveyor(xppu, sensor)
                crane_putdownWP(xppu, sensor)
            lsc_moveWPToPickingPosOnPAC(xppu, sensor)
            pac_moveWPForwardToPos2(xppu, sensor)
            pac_moveWPToPos1OnSSC(xppu, sensor)
            ssc_moveWPfromPos1ToCC(xppu, sensor)
            if xppu.smallsortingconveyor.DI_WPMetallic1 is True:
                ssc_moveWPToPos1OnRFC(xppu, sensor)
                rfc_moveWPForwardToPos2(xppu, sensor)
                rfc_moveWPToPos0OnLSC(xppu, sensor)
                lsc_moveWPForwardToPos1(xppu, sensor)
                lsc_moveWPfromPos1ToRamp1(xppu, sensor)
                lsc_sortWPToRamp1(xppu, sensor)
            elif xppu.smallsortingconveyor.DI_WPLight1 is True:
                ssc_moveWPToPos1OnRFC(xppu, sensor)
                rfc_moveWPForwardToPos2(xppu, sensor)
                rfc_moveWPToPos0OnLSC(xppu, sensor)
                lsc_moveWPForwardToPos1(xppu, sensor)
                lsc_moveWPfromPos1ToRamp2(xppu, sensor)
                lsc_sortWPToRamp2(xppu, sensor)
            else:
                ssc_moveWPToPos1OnRFC(xppu, sensor)
                rfc_moveWPForwardToPos2(xppu, sensor)
                rfc_moveWPToPos0OnLSC(xppu, sensor)
                lsc_moveAndSortWPToRamp3(xppu, sensor)


# -- Non-standard but supported test cases --

# Block workpiece movement on LargeSortingConveyor with separation cylinders and switch
def nonstandard_1(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_moveToStack(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToConveyor(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    xppu.largesortingconveyor.separator1.DO_Extend = True
    lsc_moveWPForwardToPos2(xppu, sensor)
    xppu.largesortingconveyor.separator1.DO_Extend = False
    xppu.largesortingconveyor.separator2.DO_Extend = True
    lsc_moveWPForwardToPos2(xppu, sensor)
    xppu.largesortingconveyor.separator2.DO_Extend = False
    lsc_moveWPForwardToPos2(xppu, sensor)
    xppu.largesortingconveyor.separator2.DO_Extend = True
    #xppu.make_step(10)
    lsc_moveWPBackToPos1(xppu, sensor)
    xppu.largesortingconveyor.separator2.DO_Extend = False
    xppu.largesortingconveyor.switch.DO_Extend = True
    lsc_moveWPBackToPos1(xppu, sensor)
    xppu.largesortingconveyor.switch.DO_Extend = False
    xppu.largesortingconveyor.separator1.DO_Extend = True
    lsc_moveWPBackToPos1(xppu, sensor)
    xppu.largesortingconveyor.separator1.DO_Extend = False
    lsc_moveWPBackToPos1(xppu, sensor)
    lsc_moveWPfromPos1ToRamp1(xppu, sensor)
    lsc_sortWPToRamp1(xppu, sensor)

# Move workpiece to RefillConveyor, return it to LargeSortingConveyor, and sort it into ramp 1
def nonstandard_2(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_moveToStack(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToConveyor(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    lsc_moveWPToPickingPosOnPAC(xppu, sensor)
    pac_moveWPForwardToPos2(xppu, sensor)
    pac_moveWPToPos1OnSSC(xppu, sensor)
    ssc_moveWPToPos1OnRFC(xppu, sensor)
    rfc_moveWPForwardToPos2(xppu, sensor)
    rfc_moveWPToPos2OnSSC(xppu, sensor)
    ssc_moveWPToPos2OnPAC(xppu, sensor)
    pac_moveWPBackToPos1(xppu, sensor)
    pac_moveWPToPos1OnLSC(xppu, sensor)
    lsc_moveWPfromPos1ToRamp1(xppu, sensor)
    lsc_sortWPToRamp1(xppu, sensor)

# Block workpiece movement on RefillConveyor, SmallSortingConveyor and PicAlphaConveyor with switch
def nonstandard_3(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_moveToStack(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToConveyor(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    xppu.picalphaconveyor.switch.DO_Extend = True
    xppu.make_step(20)
    lsc_moveWPToPickingPosOnPAC(xppu, sensor)
    xppu.picalphaconveyor.switch.DO_Extend = False
    xppu.make_step(20)
    xppu.picalphaconveyor.switch.DO_Extend = True
    xppu.make_step(20)
    xppu.picalphaconveyor.switch.DO_Extend = False
    xppu.make_step(20)
    pac_moveWPForwardToPos2(xppu, sensor)
    pac_moveWPToPos1OnSSC(xppu, sensor)
    ssc_moveWPToPos1OnRFC(xppu, sensor)
    xppu.refillconveyor.switch.DO_Extend = True
    xppu.make_step(20)
    rfc_moveWPForwardToPos2(xppu, sensor)
    xppu.refillconveyor.switch.DO_Extend = False
    xppu.make_step(20)
    xppu.refillconveyor.switch.DO_Extend = True
    xppu.make_step(20)
    xppu.refillconveyor.switch.DO_Extend = False
    xppu.make_step(20)
    xppu.refillconveyor.DO_TurnClockwise = True
    xppu.make_step(10)
    xppu.refillconveyor.switch.DO_Extend = True
    xppu.make_step(20)
    xppu.refillconveyor.DO_TurnClockwise = False
    xppu.refillconveyor.switch.DO_Extend = False
    xppu.make_step(20)
    xppu.refillconveyor.DO_TurnCounterClockwise = True
    xppu.make_step(20)
    xppu.refillconveyor.DO_TurnCounterClockwise = False
    xppu.refillconveyor.switch.DO_Extend = True
    xppu.make_step(20)
    xppu.refillconveyor.switch.DO_Extend = False
    xppu.make_step(20)
    rfc_moveWPBackToPos1(xppu, sensor)
    xppu.refillconveyor.switch.DO_Extend = False
    rfc_moveWPToPos2OnSSC(xppu, sensor)
    ssc_moveWPForwardToPos3(xppu, sensor)
    xppu.smallsortingconveyor.switch.DO_Extend = True
    xppu.make_step(20)
    ssc_moveWPToPos2OnPAC(xppu, sensor)
    xppu.smallsortingconveyor.switch.DO_Extend = False
    xppu.make_step(20)
    ssc_moveWPToPos2OnPAC(xppu, sensor)
    xppu.picalphaconveyor.DO_TurnCounterClockwise = True
    xppu.make_step(95)
    xppu.picalphaconveyor.DO_TurnCounterClockwise = False
    xppu.picalphaconveyor.switch.DO_Extend = True
    xppu.make_step(20)
    xppu.picalphaconveyor.switch.DO_Extend = False
    xppu.make_step(20)
    xppu.picalphaconveyor.switch.DO_Extend = True
    xppu.make_step(20)
    pac_moveWPToPos1OnLSC(xppu, sensor)
    xppu.picalphaconveyor.switch.DO_Extend = False
    pac_moveWPToPos1OnLSC(xppu, sensor)
    lsc_moveWPfromPos1ToRamp1(xppu, sensor)
    lsc_sortWPToRamp1(xppu, sensor)

# Attempt to sort workpiece into full ramp 1
def nonstandard_4(xppu, sensor=False):
    for i in range(5):
        stack_separateWP(xppu, sensor)
        crane_moveToStack(xppu, sensor)
        crane_pickupWP(xppu, sensor)
        crane_moveToConveyor(xppu, sensor)
        crane_putdownWP(xppu, sensor)
        lsc_moveWPForwardToPos1(xppu, sensor)
        lsc_moveWPfromPos1ToRamp1(xppu, sensor)
        lsc_sortWPToRamp1(xppu, sensor)

# Attempt to sort workpiece into full ramp 2
def nonstandard_5(xppu, sensor=False):
    for i in range(5):
        stack_separateWP(xppu, sensor)
        crane_moveToStack(xppu, sensor)
        crane_pickupWP(xppu, sensor)
        crane_moveToConveyor(xppu, sensor)
        crane_putdownWP(xppu, sensor)
        lsc_moveWPForwardToPos1(xppu, sensor)
        lsc_moveWPfromPos1ToRamp2(xppu, sensor)
        lsc_sortWPToRamp2(xppu, sensor)

# Attempt to sort workpiece into full ramp 3
def nonstandard_6(xppu, sensor=False):
    for i in range(5):
        stack_separateWP(xppu, sensor)
        crane_moveToStack(xppu, sensor)
        crane_pickupWP(xppu, sensor)
        crane_moveToConveyor(xppu, sensor)
        crane_putdownWP(xppu, sensor)
        lsc_moveAndSortWPToRamp3(xppu, sensor)

# Attempt to sort workpiece into full ramp 4
def nonstandard_7(xppu, sensor=False):
    for i in range(5):
        stack_separateWP(xppu, sensor)
        crane_moveToStack(xppu, sensor)
        crane_pickupWP(xppu, sensor)
        crane_moveToConveyor(xppu, sensor)
        crane_putdownWP(xppu, sensor)
        lsc_moveWPToPickingPosOnPAC(xppu, sensor)
        pac_moveWPForwardToPos2(xppu, sensor)
        pac_moveWPToPos1OnSSC(xppu, sensor)
        ssc_moveAndSortWPToRamp4(xppu, sensor)

# Store 3 workpieces on RefillConveyor first, on PicAlphaConveyor second, and sort them into ramp 3 third
def nonstandard_8(xppu, sensor=False):
    for i in range(3):
        stack_separateWP(xppu, sensor)
        crane_moveToStack(xppu, sensor)
        crane_pickupWP(xppu, sensor)
        crane_moveToConveyor(xppu, sensor)
        crane_putdownWP(xppu, sensor)
        lsc_moveWPToPickingPosOnPAC(xppu, sensor)
        pac_moveWPForwardToPos2(xppu, sensor)
        pac_moveWPToPos1OnSSC(xppu, sensor)
        ssc_moveWPToPos1OnRFC(xppu, sensor)
        xppu.refillconveyor.DO_TurnClockwise = True
        xppu.make_step(90)  # Time-based since high-level function only works for first workpiece
        xppu.refillconveyor.DO_TurnClockwise = False
    for i in range(3):
        rfc_moveWPToPos0OnLSC(xppu, sensor)
        lsc_moveWPToPickingPosOnPAC(xppu, sensor)
    xppu.largesortingconveyor.separator1.DO_Extend = True
    xppu.make_step(10)
    pac_moveWPToPos1OnLSC(xppu, sensor)
    xppu.picalphaconveyor.DO_TurnCounterClockwise = True
    xppu.largesortingconveyor.DO_TurnCounterClockwise = True
    xppu.make_step(100)
    xppu.picalphaconveyor.DO_TurnCounterClockwise = False
    xppu.largesortingconveyor.DO_TurnCounterClockwise = False
    xppu.largesortingconveyor.separator1.DO_Extend = False
    xppu.largesortingconveyor.DO_TurnClockwise = True
    xppu.make_step(100)

# Clamp and unclamp workpiece at the stack
def nonstandard_9(xppu, sensor=False):
    xppu.stack.separator.DO_Extend = True
    xppu.make_step(7) # Only partial separation
    xppu.stack.separator.DO_Extend = False
    xppu.make_step(200) # Wait a moment
    xppu.stack.separator.DO_Extend = True
    xppu.make_step(10)
    crane_moveToStack(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    xppu.stack.separator.DO_Extend = False
    xppu.make_step(10)
    crane_pickupWP(xppu, sensor)
    crane_moveToConveyor(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    lsc_moveWPForwardToPos1(xppu, sensor)
    lsc_moveWPfromPos1ToRamp1(xppu, sensor)
    lsc_sortWPToRamp1(xppu, sensor)

# Cycle workpiece on conveyer system before stamping it
def nonstandard_10(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_moveToStack(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToConveyor(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    lsc_moveWPToPickingPosOnPAC(xppu, sensor)
    pac_moveWPForwardToPos2(xppu, sensor)
    pac_moveWPToPos1OnSSC(xppu, sensor)
    ssc_moveWPToPos1OnRFC(xppu, sensor)
    rfc_moveWPForwardToPos2(xppu, sensor)
    rfc_moveWPToPos0OnLSC(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToStamp(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    stamp_stampWP(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToConveyor(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    lsc_moveWPForwardToPos1(xppu, sensor)
    lsc_moveWPfromPos1ToRamp1(xppu, sensor)
    lsc_sortWPToRamp1(xppu, sensor)

# -- Non-standard and not modeled use cases --

# Crane turns without extending first
def error_1(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_moveToStack(xppu, sensor)
    xppu.crane.liftingCylinder.DO_Extend = True
    xppu.make_step(10)
    xppu.make_step(30) # Wait a moment
    xppu.crane.DO_Intake = True
    crane_moveToConveyor(xppu, sensor)

# Crane drops workpiece in mid-air
def error_2(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_moveToStack(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    xppu.crane.table.DO_TurnCounterClockwise = True
    xppu.make_step(9) # Move 45 degree
    xppu.crane.DO_Intake = False
    xppu.make_step(9) # Attempt to move another 45 degree
    xppu.crane.table.DO_TurnCounterClockwise = False

# Crane attempts to stack workpieces on LargeSortingConveyor
def error_3(xppu, sensor=False):
    for i in range(2):
        stack_separateWP(xppu, sensor)
        crane_moveToStack(xppu, sensor)
        crane_pickupWP(xppu, sensor)
        crane_moveToConveyor(xppu, sensor)
        crane_putdownWP(xppu, sensor)
    lsc_moveWPForwardToPos2(xppu, sensor)

# Workpiece is put down on the stamp when the slidingCylinder is retracted
def error_4(xppu, sensor=False):
    xppu.stamp.slidingCylinder.DO_Retract = True
    xppu.stamp.slidingCylinder.DO_Extend = False
    stack_separateWP(xppu, sensor)
    crane_moveToStack(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToStamp(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    stamp_stampWP(xppu, sensor)

# SlidingCylinder of stamp is retracting while the workpiece is still attached to crane
def error_5(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_moveToStack(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToStamp(xppu, sensor)
    xppu.crane.liftingCylinder.DO_Extend = True
    xppu.make_step(10)
    stamp_stampWP(xppu, sensor)

# LargeSortingConveyor moves while the workpiece is still attached to crane
def error_6(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_moveToStack(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToConveyor(xppu, sensor)
    xppu.crane.liftingCylinder.DO_Extend = True
    xppu.make_step(10)
    lsc_moveWPForwardToPos1(xppu, sensor)

# Workpiece is moved of the LargeSortingConveyor
def error_7(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_moveToStack(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    crane_moveToConveyor(xppu, sensor)
    crane_putdownWP(xppu, sensor)
    lsc_moveWPForwardToPos1(xppu, sensor)
    xppu.largesortingconveyor.DO_TurnCounterClockwise = True
    xppu.make_step(20)
    xppu.largesortingconveyor.DO_TurnCounterClockwise = False

# PicAlpha moves without retracting first and collides with workpiece
def error_8(xppu, sensor=False):
    for i in range(3):
        stack_separateWP(xppu, sensor)
        crane_moveToStack(xppu, sensor)
        crane_pickupWP(xppu, sensor)
        crane_moveToConveyor(xppu, sensor)
        crane_putdownWP(xppu, sensor)
        lsc_moveWPToPickingPosOnPAC(xppu, sensor)
    picalpha_moveBackToPickupPos1(xppu, sensor)
    xppu.picalpha.liftingCylinder.DO_Extend = True
    xppu.make_step(10)
    xppu.picalpha.DO_Intake = True
    xppu.make_step(1)
    picalpha_moveForwardToPutdownPos1(xppu, sensor)
    picalpha_putdownWP(xppu, sensor)
    picalpha_moveBackToPickupPos2(xppu, sensor)
    xppu.picalpha.liftingCylinder.DO_Extend = True
    xppu.make_step(10)
    xppu.picalpha.DO_Intake = True
    xppu.make_step(1)
    picalpha_moveForwardToPutdownPos2(xppu, sensor)

# PicAlpha stacks workpieces on PicAlphaConveyor
def error_9(xppu, sensor=False):
    for i in range(3):
        stack_separateWP(xppu, sensor)
        crane_moveToStack(xppu, sensor)
        crane_pickupWP(xppu, sensor)
        crane_moveToConveyor(xppu, sensor)
        crane_putdownWP(xppu, sensor)
        lsc_moveWPToPickingPosOnPAC(xppu, sensor)
    picalpha_moveBackToPickupPos1(xppu, sensor)
    picalpha_pickupWP(xppu, sensor)
    picalpha_moveForwardToPutdownPos1(xppu, sensor)
    picalpha_putdownWP(xppu, sensor)
    picalpha_moveBackToPickupPos2(xppu, sensor)
    picalpha_pickupWP(xppu, sensor)
    picalpha_moveForwardToPutdownPos1(xppu, sensor)
    picalpha_putdownWP(xppu, sensor)

# PicAlpha drops workpiece in mid-air
def error_10(xppu, sensor=False):
    for i in range(3):
        stack_separateWP(xppu, sensor)
        crane_moveToStack(xppu, sensor)
        crane_pickupWP(xppu, sensor)
        crane_moveToConveyor(xppu, sensor)
        crane_putdownWP(xppu, sensor)
        xppu.picalphaconveyor.switch.DO_Extend = True
        lsc_moveWPToPickingPosOnPAC(xppu, sensor)
    picalpha_moveBackToPickupPos1(xppu, sensor)
    picalpha_pickupWP(xppu, sensor)
    picalpha_moveForwardToPutdownPos1(xppu, sensor)
    xppu.picalpha.DO_Intake = False
    picalpha_moveBackToPickupPos1(xppu, sensor)

# Crane retracts outside of picking locations
def error_11(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_pickupWP(xppu, sensor)
    xppu.crane.table.DO_TurnCounterClockwise = True
    xppu.make_step(5)
    xppu.crane.table.DO_TurnCounterClockwise = False
    crane_putdownWP(xppu, sensor)

# Gripper slides over workpiece
def error_12(xppu, sensor=False):
    stack_separateWP(xppu, sensor)
    crane_moveToConveyor(xppu, sensor)
    xppu.crane.liftingCylinder.DO_Extend = True
    xppu.make_step(10)
    xppu.crane.table.DO_TurnClockwise = True
    xppu.make_step(25)