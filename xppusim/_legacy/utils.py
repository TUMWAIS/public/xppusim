import os

def rel_to_abs_path(rel_path, file=__file__):
    dir_path = os.path.dirname(os.path.realpath(file))
    abs_path = dir_path + "\\" + rel_path
    return abs_path
