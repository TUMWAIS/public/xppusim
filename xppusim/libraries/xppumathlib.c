#include <Python.h>
#include <math.h>


int inbtw(float bound1, float bound2, float value, float precision) {

    if(bound1<bound2) {
        if(bound1-precision<=value && bound2+precision>=value) {
            return 1;
        }else {
            return 0;
        }
    } else {
        if(bound1+precision>=value && bound2-precision<=value) {
            return 1;
        }else {
            return 0;
        }
    }
};

double get_attr_dbl(PyObject *obj, char attr_name[]) {
    PyObject *obj_attr_val = PyObject_GetAttrString(obj, attr_name);
    double attr_val = PyFloat_AsDouble(obj_attr_val);
    Py_DECREF(obj_attr_val);
    return attr_val;
};


static PyObject *xppuMathLib_median(PyObject *self, PyObject *args) {
    float bound1, bound2, value, precision=0.0001;

    if (!PyArg_ParseTuple(args, "fff|f", &bound1, &bound2, &value, &precision)) {
        return NULL;
    }

    if(inbtw(bound1, bound2, value, precision) == 1) {
        Py_RETURN_TRUE;
    } else {
        Py_RETURN_FALSE;
    }

};


static PyObject *xppuMathLib_wpinbtw(PyObject *self, PyObject *args) {
    PyObject *pos_start, *pos_end, *wp;
    float precision = 0.0001;

    if (!PyArg_ParseTuple(args, "OOO|f", &pos_start, &pos_end, &wp, &precision)) {
        return NULL;
    }

    double pos_start_x = get_attr_dbl(pos_start, "x");
    double pos_start_y = get_attr_dbl(pos_start, "y");
    double pos_start_z = get_attr_dbl(pos_start, "z");

    double pos_end_x = get_attr_dbl(pos_end, "x");
    double pos_end_y = get_attr_dbl(pos_end, "y");
    double pos_end_z = get_attr_dbl(pos_end, "z");

    PyObject *wp_pos = PyObject_GetAttrString(wp, "pos");
    double wp_x = get_attr_dbl(wp_pos, "x");
    double wp_y = get_attr_dbl(wp_pos, "y");
    double wp_z = get_attr_dbl(wp_pos, "z");
    Py_DECREF(wp_pos);

    if(inbtw(pos_start_x, pos_end_x, wp_x, precision) == 1 && inbtw(pos_start_y, pos_end_y, wp_y, precision) == 1 && inbtw(pos_start_z, pos_end_z, wp_z, precision) == 1) {
        Py_RETURN_TRUE;
    } else {
        Py_RETURN_FALSE;
    }
};


static PyObject *xppuMathLib_wpatpos(PyObject *self, PyObject *args, PyObject *kwargs) {
    PyObject *pos, *wp, *step = NULL, *dim = NULL, *frac = NULL;
    double precision = 0.0001;
    static char* keywords[] = {"pos", "wp", "step", "dim", "frac", "precision", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OO|OOOf", keywords, &pos, &wp, &step, &dim, &frac, &precision)) {
        return NULL;
    }

    double pos_x = get_attr_dbl(pos, "x");
    double pos_y = get_attr_dbl(pos, "y");
    double pos_z = get_attr_dbl(pos, "z");

    PyObject *wp_pos = PyObject_GetAttrString(wp, "pos");
    double wp_pos_x = get_attr_dbl(wp_pos, "x");
    double wp_pos_y = get_attr_dbl(wp_pos, "y");
    double wp_pos_z = get_attr_dbl(wp_pos, "z");
    Py_DECREF(wp_pos);

    PyObject *wp_dim = PyObject_GetAttrString(wp, "dim");
    double wp_dim_x = get_attr_dbl(wp_dim, "x");
    double wp_dim_y = get_attr_dbl(wp_dim, "y");
    double wp_dim_z = get_attr_dbl(wp_dim, "z");
    Py_DECREF(wp_dim);

    double step_x = 0;
    double step_y = 0;
    double step_z = 0;
    if(step) {
        step_x = get_attr_dbl(step, "x");
        step_y = get_attr_dbl(step, "y");
        step_z = get_attr_dbl(step, "z");
    }

    double dim_x = 0;
    double dim_y = 0;
    double dim_z = 0;
    if(dim) {
        dim_x = get_attr_dbl(dim, "x");
        dim_y = get_attr_dbl(dim, "y");
        dim_z = get_attr_dbl(dim, "z");
    }

    double frac_x = 1;
    double frac_y = 1;
    double frac_z = 1;
    if(frac) {
        frac_x = get_attr_dbl(frac, "x");
        frac_y = get_attr_dbl(frac, "y");
        frac_z = get_attr_dbl(frac, "z");
    }

    double x_min = wp_pos_x + step_x - wp_dim_x/2/frac_x - dim_x/2;
    double x_max = wp_pos_x + step_x + wp_dim_x/2/frac_x + dim_x/2;
    double y_min = wp_pos_y + step_y - wp_dim_y/2/frac_y - dim_y/2;
    double y_max = wp_pos_y + step_y + wp_dim_y/2/frac_y + dim_y/2;
    double z_min = wp_pos_z + step_z - dim_z;
    double z_max = wp_pos_z + step_z + wp_dim_z;

    if(inbtw(x_min, x_max, pos_x, precision) == 1 && inbtw(y_min, y_max, pos_y, precision) == 1 && inbtw(z_min, z_max, pos_z, precision) == 1) {
        Py_RETURN_TRUE;
    } else {
        Py_RETURN_FALSE;
    }

};


static PyObject *xppuMathLib_vecnorm(PyObject *self, PyObject *args) {
    PyObject *vec;

    if (!PyArg_ParseTuple(args, "O", &vec)) {
        return NULL;
    }

    double vec_x = get_attr_dbl(vec, "x");
    double vec_y = get_attr_dbl(vec, "y");
    double vec_z = get_attr_dbl(vec, "z");

    double norm = sqrt(pow(vec_x,2) + pow(vec_y,2) + pow(vec_z,2));
    return PyFloat_FromDouble(norm);

};

static PyMethodDef xppuMathLib_FunctionsTable[] = {
    {
        "median",                       // name exposed to Python
        xppuMathLib_median,             // C wrapper function
        METH_VARARGS,                   // received variable args (but really just 1)
        "Evaluates if a value lies between two given bounds" // documentation
    }, {
        "wp_in_btw",                    // name exposed to Python
        xppuMathLib_wpinbtw,            // C wrapper function
        METH_VARARGS,                   // received variable args (but really just 1)
        "Checks whether a workpiece is located between two coordinates"     // documentation
    }, {
        "wp_at_pos",                    // name exposed to Python
        xppuMathLib_wpatpos,            // C wrapper function
        METH_VARARGS | METH_KEYWORDS,   // received variable args (but really just 1)
        "Checks whether a location (or a volume) overlapps with the current (or future) position of a workpiece. The center of the volume is assumed to be at the center of the bottom plane (similar to the center of workpieces) (Basis for workpiece overlapping and sensor detection)"          // documentation
    }, {
        "vec_norm",                       // name exposed to Python
        xppuMathLib_vecnorm,              // C wrapper function
        METH_VARARGS,                     // received variable args (but really just 1)
        "Calculates the 2-norm of a mathutils vector" // documentation
    }, {
        NULL, NULL, 0, NULL
    }
};

static struct PyModuleDef xppuMathLib_Module = {
    PyModuleDef_HEAD_INIT,
    "xppumathlib",     // name of module exposed to Python
    "Library containing often executed functions within xppusim", // module documentation
    -1,
    xppuMathLib_FunctionsTable
};

PyMODINIT_FUNC PyInit_xppumathlib(void) {
    return PyModule_Create(&xppuMathLib_Module);
};