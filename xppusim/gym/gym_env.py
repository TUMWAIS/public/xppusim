import json
import numpy as np
from copy import deepcopy

from gym.core import GoalEnv
from gym.spaces import MultiDiscrete
from gym.spaces import Discrete
from gym.spaces import Dict
from gym.spaces import Box

from xppusim.modullib import XPPU, Error
from xppusim import high_level_func
from xppusim.gym.actionspaces import aggregated, low_level_onoff, low_level_switch, low_level_branching, multiagent


class XPPUSIMEnv(GoalEnv):
    """
    The XPPUSIMEnv is the gym-based reinforcement learning environment.
    The agent directly interacts with this class during training.
    This class wraps the XPPUSim objects, which handles the discrete event simulation.

    """

    def __init__(self,
                 state_space_conf_path,
                 action_space_conf_path,
                 goal,
                 goal_args = [],
                 wp_in_stack=None,
                 wp_combinations_in_pool=None,
                 wp_combination_pool_size=81,
                 wp_num_in_stack=4,
                 wp_pool_seed=None,
                 render=False,
                 normalize_state_space=True,
                 action_space_type=None,
                 position_list=None,
                 render_dir=None,
                 crane_turn_only_extended=False,
                 crane_move_at_pickpoint=True,
                 reset_fn=None):


        # Create a workpiece pool depending on the provided configuration parameters
        if wp_in_stack is not None:
            self.wp_pool = [wp_in_stack.copy()]                 # Copy by value
        elif wp_combinations_in_pool is not None:
            self.wp_pool = wp_combinations_in_pool.copy()       # Copy by value
        else:
            self.wp_pool = self._create_wp_pool(wp_num_in_stack, wp_combination_pool_size, wp_pool_seed)

        # Initialize xppusim
        self._workpieces = self._sample_wp_config()
        self._position_list = position_list
        self._wp_num_in_stack = len(self._workpieces)  # Recalculate since data may not be available if a pool or specific combination is provided
        self._render = render
        self._crane_turn_only_extended = crane_turn_only_extended
        self._crane_move_at_pickpoint = crane_move_at_pickpoint
        self._normalize_state_space = normalize_state_space

        self.xppusim = self.get_sim_env()

        # Read action space structure from configuration file and calculate dimensions
        self._action_space_type = action_space_type
        self.action_space_names = self._get_action_space(action_space_conf_path)
        self.action_dict = self.xppusim.get_actions()
        self._action_space_dim = [len(self.action_space_names)]
        self.action_space = Discrete(len(self.action_space_names))

        # Read state space structure from configuration file
        self._state_space_mapping, self._state_space_names = self._get_state_space(state_space_conf_path, action_space_conf_path)

        # Calculate state space dimensions
        self._observation_space_dim = []
        for var_name in self._state_space_names:
            if "_DI_" in var_name:
                self._observation_space_dim.append(2)
            elif "_AI" in self._state_space_names:
                self._observation_space_dim.append(4096)


        self.render_dir = render_dir
        self.reset_fn = reset_fn

        # Get initial observation vector
        self._initial_observation = self._get_state()

        # Initialize info dict
        self._info = {}
        self._info["system_positions"] = self.xppusim.get_position_dict()
        self._info["workpiece_info"] = self._get_wp_info()
        self._info["done_msg"] = None
        self._info["error_msg"] = None
        self._info["reward_msg"] = ""
        self._info["max_reward"] = None
        self._info["initial_pos"] = ""
        self._info["prev_action_name"] = ""
        self._info["action_name"] = ""

        # Initialize goal
        self.goal = goal(self._state_space_mapping, self._info, *goal_args)
        achieved_goal, desired_goal = self.goal.reset(self._initial_observation, self._info)

        # Extend state_space_mapping and observation_space_dim to include reward states
        if self.goal.get_reward_state() is not None:
            self._observation_space_dim.append(4096)
            self._state_space_mapping["reward"] = [len(self._initial_observation)]

        # Store observation space and observations
        self.observation_space = Dict(dict(
            desired_goal=Box(low=-1, high=1, shape=self.goal.shape),
            achieved_goal=Box(low=-1, high=1, shape=self.goal.shape),
            observation=MultiDiscrete(self._observation_space_dim)
            ))

        self.observation_space_mapping = {"observation":    self._state_space_mapping,
                                          "achieved_goal":  self.goal.goal_space_mapping,
                                          "desired_goal":   self.goal.goal_space_mapping}

        self._observation = {"desired_goal": desired_goal,
                             "achieved_goal": achieved_goal,
                             "observation": self._initial_observation
                            }

        self._observation_prev = self._observation.copy() # Copy by value!


    def get_sim_env(self):
        """
        Returns the simulation object to be wrapped by this class.

        :return:    Object
        """
        xppusim = XPPU.XPPU(wp_orders=self._workpieces,
                            render=self._render,
                            crane_turn_only_extended=self._crane_turn_only_extended,
                            crane_move_at_pickpoint=self._crane_move_at_pickpoint,
                            normalize_states=self._normalize_state_space)

        return xppusim


    def set_coord_predictor(self, coord_predictor):
        """
        Sets the coordinate predictor of the environment

        :param coord_predictor: Coordinate Predictor to be set
        """
        if coord_predictor is not None:
            state = self.xppusim.get_states_from_sim()
            action = self.xppusim.get_actions()
            coord_predictor.reset(state, action)
            self.xppusim.CoordPredictor = coord_predictor
        else:
            self.xppusim.CoordPredictor = None


    def get_observation(self):
        achieved_goal, desired_goal = self.goal.update(self._observation["observation"], self._info)
        observation = deepcopy(self._observation)
        observation["achieved_goal"] = achieved_goal
        return observation, deepcopy(self._info)


    def reset(self, wp_in_stack=None, position = None, render=None, reset_fn_args=None):
        """
        Resets the environment

        :param new_wps: If provided the simulation is reseted to this workpiece configuration, otherwise a random one is drawn from the pool
        :return self.current_state: A list with the state of the simulation after the reset
        :return self.goal: The goal selected after the reset
        """

        # Set new workpiece configuration
        if wp_in_stack is None:
            self._workpieces = self._sample_wp_config()
        else:
            self._workpieces = wp_in_stack.copy()   # Copy by value

        # Update initial workpiece position, if provided
        if position is None and self._position_list is not None:
            position = np.random.choice(self._position_list)

        # Reset the simulation environment
        self.xppusim.reset(self._workpieces, position, render)
        if self.reset_fn is not None:
            self.reset_fn(self.xppusim, wp_in_stack, position, reset_fn_args)

        # Get new simulation state
        self.action_dict = self.xppusim.get_actions()
        obs = self._get_state()

        # First update of info dict (Everything that is provided by the goal class)
        self._info["workpiece_info"] = self._get_wp_info()
        self._info["initial_pos"] = position
        self._info["done_msg"] = None
        self._info["error_msg"] = None
        self._info["reward_msg"] = ""
        self._info["max_reward"] = 1
        self._info["prev_action_name"] = ""
        self._info["action_name"] = ""

        # Get the achieved goal, desired goal, and maximum possible reward
        achieved_goal, desired_goal = self.goal.reset(obs, self._info)

        # Add reward state
        reward_state = self.goal.get_reward_state()
        if reward_state is not None:
            obs = np.append(obs, reward_state)

        # Store the new observation
        self._observation["observation"] = obs
        self._observation["achieved_goal"] = achieved_goal
        self._observation["desired_goal"] = desired_goal
        self._observation_prev = self._observation.copy()           # Copy by value

        return self._observation.copy(), self. _info.copy()         # Copy by value


    def step(self, action):
        """
        Takes an action in the environment

        :param action:          Action to execute for the step
        :return _observation:   The new observation vector after taking the action
        :return reward:         Reward received for the taken action
        :return done:           Flag indicating whether the trajectory is finished
        :return _info:          Info dictionary containing further information regarding the transition
        """
        error_msg = None

        # Execute action in simulation environment
        try:
            if self._action_space_type == "aggregated":
                exec('aggregated.' + self.action_space_names[action[0]] + '(self.xppusim)')
            elif self._action_space_type == "multiagent":
                exec('multiagent.' + self.action_space_names[action[0]] + '(self.xppusim)')
            elif self._action_space_type == "high_level":
                exec('high_level_func.' + self.action_space_names[action[0]] + '(self.xppusim)')
            elif self._action_space_type == "low_level_switch":
                self.action_dict = eval('low_level_switch.' + self.action_space_names[action[0]] + '(self.action_dict)')
                self.xppusim.set_actions(self.action_dict)
                self.xppusim.make_step()
            elif self._action_space_type == "low_level_onoff":
                self.action_dict = eval('low_level_onoff.' + self.action_space_names[action[0]] + '(self.action_dict)')
                self.xppusim.set_actions(self.action_dict)
                self.xppusim.make_step()
            elif self._action_space_type == "low_level_branching":
                str_ = ""
                for action_name, action_val in zip(self.action_space_names, action):
                    str_ += action_name + ": " + str(action_val) + " | "
                    if action_name not in self.action_dict:
                        print(action_name)
                        print(self.action_dict)
                        print("ACTION NOT FOUND!!!!")
                    self.action_dict[action_name] = bool(action_val)
                self.xppusim.set_actions(self.action_dict)
                self.xppusim.make_step()
            else:
                raise Error.ConfigurationError(self._action_space_type, "Action space type not implemented.")
        except Error.Error as err:
            error_msg = str(err)

        # Get next simulation state
        obs = self._get_state()

        # First update of info dict (Everything that is not provided by the goal class)
        self._info["workpiece_info"] = self._get_wp_info()
        self._info["error_msg"] = error_msg
        self._info["prev_action_name"] = self._info["action_name"]
        if self._action_space_type != "low_level_branching":
            self._info["action_name"] = self.action_space_names[action[0]]
        else:
            self._info["action_name"] = action
            if self._info["prev_action_name"] == "":
                self._info["prev_action_name"] = action

        # Update goal state and get reward
        achieved_goal, desired_goal= self.goal.update(obs, self._info)

        # Move waypoint for rendering
        position = []
        if self._normalize_state_space is True:
            position.append(desired_goal[0] * 130)
            position.append(desired_goal[1] * 130)
            position.append(desired_goal[2] * 30)
            self.xppusim.set_subgoal(position)

        # Store the new observation
        self._observation_prev = self._observation.copy()       # Copy by value
        self._observation["observation"] = obs
        self._observation["achieved_goal"] = achieved_goal
        self._observation["desired_goal"] = desired_goal

        # Calculate the reward for the transition (Important: Reward is calculated based on the previous desired goal)
        reward, done, self._info = self.goal.compute_reward(self._observation_prev["achieved_goal"],
                                                                  self._observation["achieved_goal"],
                                                                  self._observation["desired_goal"],       #TODO: Check
                                                                  self._info)

        # Add reward state
        reward_state = self.goal.get_reward_state()
        if reward_state is not None:
            self._observation["observation"] = np.append(self._observation["observation"], reward_state)

        return self._observation.copy(), reward, done, self._info.copy()       # Copy by value



    def render(self):
        """
        Renders the trajectory simulated since last reset

        """
        self.xppusim.save(self.render_dir, "tmp.blend")
        self.xppusim.render_file(self.render_dir, "tmp.blend")


    def _create_wp_pool(self, wp_num_in_stack, wp_combination_pool_size, wp_pool_seed):
        """
        Creates a pool of random workpiece configurations

        :return: List with workpiece indices in the created pool
        """

        rand_num_gen = np.random.RandomState(wp_pool_seed)
        wp_config_space = self.get_wp_config_space(wp_num_in_stack)
        wp_config_space_dim = len(wp_config_space)
        indices = rand_num_gen.choice(wp_config_space_dim, wp_combination_pool_size, replace=False)
        wp_pool = [wp_config_space[i] for i in indices]

        return wp_pool



    def get_wp_config_space(self, wp_num_in_stack):
        """
        Creates a workpiece pool containing all possible combinations

        :param params: Parameter object containing the number of workpieces in the stack
        :return:       List of lists containing workpiece combinations, e.g., [[black, white], [white, black]]
        """

        def choose_wp_recursive(wp_type_list, max_wps, wp_conf=[], wp_pool=[], wp_num=0):
            for wp in wp_type_list:

                if wp_num < max_wps:
                    tmp = wp_conf.copy()
                    tmp.append(wp)
                    choose_wp_recursive(wp_type_list, max_wps, tmp, wp_pool, wp_num + 1)
                else:
                    wp_pool.append(wp_conf)
                    return wp_pool

            return wp_pool

        WP_TYPE_LIST = ["black", "white", "metallic"]
        wp_pool = choose_wp_recursive(WP_TYPE_LIST, wp_num_in_stack)

        return wp_pool


    # Complete random sampling of workpieces
    def _sample_wp_config(self):
        """
        Samples a workpiece configuration from the workpiece pool

        :return: List of workpiece indices in the sampled configuration
        """
        idx = np.random.randint(len(self.wp_pool))

        return self.wp_pool[idx]


    #ToDo: Remove
    def _get_wp_indices_for_pool(self, wp_combinations_in_pool):
        """
        Converts a list of lists containing workpice types to be placed in the stack into a list of lists with unique
        workpiece indices

        :param wp_combinations_in_pool: List of lists with workpiece types, e.g., [[black, white], [metallic, black]]
        :return: List of lists with unique workpiece indices (each index is only included once)
        """

        wp_pool_idx_list = []
        for wp_config in wp_combinations_in_pool:
            wp_pool_idx_list.append(self._get_wp_indexes_for_config(wp_config))

        return wp_pool_idx_list



    # Create structured dictionary mapping states to indices of the state vector
    def _get_state_space(self, state_space_conf_path, action_space_conf_path):
        """
        Reads the JSON with the state space configuration and returns a dictionary that maps the indices of the state
        vector to hierarchical state input groups for the neural network.

        On the first level it is differentiated between "system" inputs (e.g., sensors of the xppu) and "workpiece" inputs
        (e.g., workpiece information like color). The workpiece group contains sub-groups for each workpiece in the stack.
        On the lowest level, the state variables are mapted to their index in the state vector.

        system
            [State variable]: [Index in the state vector]
        workpiece
            [Workpiece index]
                [State variable]: [Index in the state vector]

        :return state_space_mapping: Nested dictionary containing the mapping
        :return state_space_names: List of variable names in the state_space
        """

        with open(state_space_conf_path) as json_file:
            state_space_conf = json.load(json_file)

        with open(action_space_conf_path) as json_file:
            action_space_conf = json.load(json_file)

        state_space_mapping = {"system": {}, "workpiece": [], "action": {}}
        state_space_names = []
        idx = 0
        for state, value in state_space_conf["system"].items():
            if value is True:
                state_space_mapping["system"][state] = idx
                state_space_names.append(state)
                idx += 1

        for wp_idx in range(len(self._workpieces)):
            state_space_mapping["workpiece"].append({})
            for state, value in state_space_conf["workpiece"].items():
                if value is True:
                    state_space_mapping["workpiece"][wp_idx][state] = idx
                    state_space_names.append(state)
                    idx += 1

        if self._action_space_type == "low_level_switch" or self._action_space_type == "low_level_onoff":
            for action, value in action_space_conf.items():
                if value is True and action != "DO_Nothing":
                    state_space_mapping["action"][action] = idx
                    state_space_names.append(action)
                    idx += 1

        return state_space_mapping, state_space_names


    # Load action space configuration from file (to avoid file access during training)
    def _get_action_space(self, action_space_conf_path):
        """
            Reads JSON with action space configuration and returns a list with all available actions

        :return: List with the (function) names of all available actiones
        """
        with open(action_space_conf_path) as json_file:
            action_space_conf = json.load(json_file)

        action_space_names = []
        for action, value in action_space_conf.items():
            if value is True:
                if self._action_space_type == "low_level_onoff" and action != "DO_Nothing":    # Add action twice if low level on/off
                    action_space_names.append(action + "_True")
                    action_space_names.append(action + "_False")
                else:
                    action_space_names.append(action)

        return action_space_names


    # Gets the current simulation state
    def _get_state(self):
        """
        Returns the current state (state vector) of the simulation as a numpy array.
        The function therfore accesses the get_states() function of the xppusim module

        :return: Numpy array containing the current state of the simulation environment
        """
        xppu_states = self.xppusim.get_states()
        xppu_actions = self.xppusim.get_actions()
        states = []

        for state in self._state_space_mapping["system"].keys():
            states.append(xppu_states[state])

        for wp_idx, wp_states in enumerate(self._state_space_mapping["workpiece"]):
            for state in wp_states.keys():
                states.append(xppu_states["workpiece"][wp_idx][state])

        for action in self._state_space_mapping["action"]:
            if action != "DO_Nothing":
                states.append(xppu_actions[action])

        return np.array(states)


    def _get_wp_info(self):
        """
        Returns the current workpiece information to be included in the info dictionary

        :return: List of disctionaries containing the workpiece information
        """
        xppu_states = self.xppusim.get_states()
        wp_info = []

        for wp_idx, wp_state in enumerate(xppu_states["workpiece"]):
            wp_info.append({})

            wp_info[wp_idx]["position"] = [wp_state["position.x"], wp_state["position.y"], wp_state["position.z"]]
            wp_info[wp_idx]["position_real"] = [wp_state["position_real.x"], wp_state["position_real.y"], wp_state["position_real.z"]]
            wp_info[wp_idx]["stamped"] = wp_state["stamped"]

            if wp_state["color_black"] == 1:
                wp_info[wp_idx]["type"] = "black"
            elif wp_state["color_white"] == 1:
                wp_info[wp_idx]["type"] = "white"
            elif wp_state["color_metallic"] == 1:
                wp_info[wp_idx]["type"] = "metallic"

            if wp_state["stored_ramp1"] == 1:
                wp_info[wp_idx]["stored"] = "ramp1"
            elif wp_state["stored_ramp2"] == 1:
                wp_info[wp_idx]["stored"] = "ramp2"
            elif wp_state["stored_ramp3"] == 1:
                wp_info[wp_idx]["stored"] = "ramp3"
            elif wp_state["stored_ramp4"] == 1:
                wp_info[wp_idx]["stored"] = "ramp4"
            else:
                wp_info[wp_idx]["stored"] = None

        return wp_info

