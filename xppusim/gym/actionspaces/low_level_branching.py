
def stack_separator_DO_Extend(action, action_dict):
    action_dict["stack_separator_DO_Extend"] = action[0]
    return action_dict

def crane_liftingCylinder_DO_Extend(action, action_dict):
    action_dict["crane_liftingCylinder_DO_Extend"] = action[0]
    return action_dict

def crane_table_DO_TurnClockwise(action, action_dict):
    action_dict["crane_table_DO_TurnClockwise"] = action[0]
    return action_dict

def crane_table_DO_TurnCounterClockwise(action, action_dict):
    action_dict["crane_table_DO_TurnCounterClockwise"] = action[0]
    return action_dict

def crane_DO_Intake(action, action_dict):
    action_dict["crane_DO_Intake"] = action
    return action_dict