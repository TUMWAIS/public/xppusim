from xppusim.gym.actionspaces import aggregated


def stack_separateWP(xppu):
    aggregated.stack_separateWP(xppu)


def crane_moveWPtoConveyor(xppu):
    if xppu.stack.DI_WPProvided is True:
        aggregated.crane_pickWPfromStackToConveyor(xppu)

    elif xppu.stamp.DI_WPAvailable is True:
        aggregated.crane_pickWPfromStampToConveyor(xppu)


def crane_moveWPtoStamp(xppu):
    if xppu.stack.DI_WPProvided is True:
        aggregated.crane_pickWPfromStackToStamp(xppu)

    elif xppu.largesortingconveyor.DI_WPPosition0 is True:
        aggregated.crane_pickWPfromConveyorToStamp(xppu)


def crane_moveWPtoStack(xppu):
    if xppu.largesortingconveyor.DI_WPPosition0 is True:
        aggregated.crane_pickWPfromConveyorToStack(xppu)

    elif xppu.stamp.DI_WPAvailable is True:
        aggregated.crane_pickWPfromStampToStack(xppu)


def stamp_stampWP(xppu):
    aggregated.stamp_stampWP(xppu)


def lsc_sortWPtoRamp1(xppu):
    aggregated.lsc_moveWPForwardToPos1(xppu)
    aggregated.lsc_sortWPToRamp1fromPos1(xppu)


def lsc_sortWPtoRamp2(xppu):
    aggregated.lsc_moveWPForwardToPos1(xppu)
    aggregated.lsc_sortWPToRamp2fromPos1(xppu)


def lsc_sortWPtoRamp3(xppu):
    aggregated.lsc_sortWPToRamp3(xppu)


def lsc_moveWPBackToPos0(xppu):
    aggregated.lsc_moveWPBackToPos0(xppu)