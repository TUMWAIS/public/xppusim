from xppusim import high_level_func

# -------------------------------------------------
# Overview of defined actions
# -------------------------------------------------

#crane_pickWPfromStackToConveyor            Move Workpiece from Stack to Conveyor
#crane_pickWPfromStackToStamp               Move Workpiece from Stack to Stamp
#crane_pickWPfromConveyorToStack            Move Workpiece from Conveyor to Stack
#crane_pickWPfromStampToConveyor            Move Workpiece from Stamp to Conveyor
#crane_pickWPfromConveyorToStamp            Move Workpiece from Conveyor to Stamp
#crane_pickWPfromStampToStack               Move Workpiece from Stamp to Stack
#lsc_moveWPForwardToPos1 		            Move workpiece forward to positioning sensor 1 on LargeSortingConveyor
#lsc_moveWPForwardToPos2 		            Move workpiece forward to positioning sensor 2 on LargeSortingConveyor
#lsc_moveWPBackToPos0 			            Move workpiece back to positioning sensor 0 on LargeSortingConveyor
#lsc_moveWPBackToPos1 			            Move workpiece back to positioning sensor 1 on LargeSortingConveyor
#lsc_sortWPToRamp1fromPos1 			        Sort workpiece at ramp 1 of the LargeSortingConveyor into the ramp
#lsc_sortWPToRamp2fromPos1 			        Sort workpiece at ramp 2 of the LargeSortingConveyor into the ramp
#lsc_moveWPToPickingPosOnPAC 		        Move workpiece from any position to ramp 3 of the LargeSortingConveyor and sort it into the ramp
#pac_moveWPToPos1OnSSC 	                    Move workpiece from LargeSortingConveyor to the picking position on PickalphaConveyor
#rfc_moveWPToPos0OnLSC: 			        Move workpiece from the RefillCOnveyor to the positioning sensor 0 on LargeSortingConveyor
#rfc_moveWPForwardToPos2 		            Move workpiece forward to positioning sensor 2 on RefillConveyor
#rfc_moveWPBackToPos1 			            Move workpiece back to positioning sensor 1 on RefillConveyor
#pac_moveWPToPos1OnLSC                      Moves workpiece tom PickAlphaConveyor back to position 1 on LargeSortingConveyor
#ssc_moveWPForwardToPos3                    Moves workpiece forward to position 3 on SmallSortingConveyor
#ssc_moveWPBackToPos1: 			            Move workpiece back to positioning sensor 1 on SmallSortingConveyor
#ssc_moveAndSortWPToRamp4: 		            Move workpiece from any position to ramp 4 of the SmallSortingConveyor and sort it into the ramp
#ssc_moveWPToPos1OnRFC: 		            Move workpiece from the SmallSortingCOnveyor to the positioning sensor 1 on RefillConveyor
#lsc_moveWPfromPos1ToCC2                    Move workpiece from position 1 on LargeSortingConveyor to first color check position
#lsc_moveWPfromPos1ToCC1                    Move workpiece from position 1 on LargeSortingConveyor to second color check position


# -------------------------------------------------
# Build actions from high-level functions
# -------------------------------------------------

def stack_separateWP(xppu, sensor=False):
    high_level_func.stack_separateWP(xppu, sensor)

def crane_pickWPfromStackToConveyor(xppu, sensor=False):
    high_level_func.crane_moveToStack(xppu, sensor)
    high_level_func.crane_pickupWP(xppu, sensor)
    high_level_func.crane_moveToConveyor(xppu, sensor)
    high_level_func.crane_putdownWP(xppu, sensor)

def crane_pickWPfromStackToStamp(xppu, sensor=False):
    high_level_func.crane_moveToStack(xppu, sensor)
    high_level_func.crane_pickupWP(xppu, sensor)
    high_level_func.crane_moveToStamp(xppu, sensor)
    high_level_func.crane_putdownWP(xppu, sensor)

def crane_pickWPfromConveyorToStack(xppu, sensor=False):
    high_level_func.crane_moveToConveyor(xppu, sensor)
    high_level_func.crane_pickupWP(xppu, sensor)
    high_level_func.crane_moveToStack(xppu, sensor)
    high_level_func.crane_putdownWP(xppu, sensor)

def crane_pickWPfromStampToConveyor(xppu, sensor=False):
    high_level_func.crane_moveToStamp(xppu, sensor)
    high_level_func.crane_pickupWP(xppu, sensor)
    high_level_func.crane_moveToConveyor(xppu, sensor)
    high_level_func.crane_putdownWP(xppu, sensor)

def crane_pickWPfromStampToStack(xppu, sensor=False):
    high_level_func.crane_moveToStamp(xppu, sensor)
    high_level_func.crane_pickupWP(xppu, sensor)
    high_level_func.crane_moveToStack(xppu, sensor)
    high_level_func.crane_putdownWP(xppu, sensor)

def crane_pickWPfromConveyorToStamp(xppu, sensor=False):
    high_level_func.crane_moveToConveyor(xppu, sensor)
    high_level_func.crane_pickupWP(xppu, sensor)
    high_level_func.crane_moveToStamp(xppu, sensor)
    high_level_func.crane_putdownWP(xppu, sensor)

def stamp_stampWP(xppu, sensor=False):
    high_level_func.stamp_stampWP(xppu, sensor)

def lsc_moveWPForwardToPos1(xppu, sensor=False):
    high_level_func.lsc_moveWPForwardToPos1(xppu, sensor)

def lsc_moveWPForwardToPos2(xppu, sensor=False):
    high_level_func.lsc_moveWPForwardToPos2(xppu, sensor)

def lsc_moveWPBackToPos0(xppu, sensor=False):
    high_level_func.lsc_moveWPBackToPos0(xppu, sensor)

def lsc_moveWPBackToPos1(xppu, sensor=False):
    high_level_func.lsc_moveWPBackToPos1(xppu, sensor)

def lsc_sortWPToRamp1fromPos1(xppu, sensor=False):
    #high_level_func.lsc_moveWPfromPos1ToCC1(xppu, sensor)       #additional step
    #high_level_func.lsc_moveWPBackToPos1(xppu, sensor)          # additional step
    high_level_func.lsc_moveWPfromPos1ToRamp1(xppu, sensor)
    high_level_func.lsc_sortWPToRamp1(xppu, sensor)

def lsc_sortWPToRamp2fromPos1(xppu, sensor=False):
    high_level_func.lsc_moveWPfromPos1ToRamp2(xppu, sensor)
    high_level_func.lsc_sortWPToRamp2(xppu, sensor)

def lsc_sortWPToRamp3(xppu, sensor=False):
    high_level_func.lsc_moveAndSortWPToRamp3(xppu, sensor)

def lsc_moveWPToPickingPosOnPAC(xppu, sensor=False):
    high_level_func.lsc_moveWPToPickingPosOnPAC(xppu, sensor)

def pac_moveWPToPos1OnSSC(xppu, sensor=False):
    high_level_func.pac_moveWPToPos1OnSSC(xppu, sensor)

def rfc_moveWPToPos0OnLSC(xppu, sensor=False):
    high_level_func.rfc_moveWPToPos0OnLSC(xppu, sensor)

def rfc_moveWPForwardToPos2(xppu, sensor=False):
    high_level_func.rfc_moveWPForwardToPos2(xppu, sensor)

def rfc_moveWPBackToPos1(xppu, sensor=False):
    high_level_func.rfc_moveWPBackToPos1(xppu, sensor)

def pac_moveWPToPos1OnLSC(xppu, sensor=False):
    high_level_func.pac_moveWPToPos1OnLSC(xppu,sensor)

def ssc_moveWPForwardToPos3(xppu, sensor=False):
    high_level_func.ssc_moveWPForwardToPos3(xppu, sensor)

def ssc_moveWPBackToPos1(xppu, sensor=False):
    high_level_func.ssc_moveWPBackToPos1(xppu, sensor)

def ssc_moveWPToPos1OnRFC(xppu, sensor=False):
    high_level_func.ssc_moveWPToPos1OnRFC(xppu, sensor)

def ssc_moveAndSortWPToRamp4(xppu, sensor=False):
    high_level_func.ssc_moveAndSortWPToRamp4(xppu, sensor)

def lsc_moveWPfromPos1ToCC2(xppu, sensor=False):
    high_level_func.lsc_moveWPfromPos1ToCC2(xppu, sensor)

def lsc_moveWPfromPos1ToCC1(xppu, sensor=False):
    high_level_func.lsc_moveWPfromPos1ToCC1(xppu, sensor)

