
def stack_separator_DO_Extend_True(action_dict):
    action_dict["stack_separator_DO_Extend"] = True
    return action_dict

def stack_separator_DO_Extend_False(action_dict):
    action_dict["stack_separator_DO_Extend"] = False
    return action_dict

def crane_liftingCylinder_DO_Extend_True(action_dict):
    action_dict["crane_liftingCylinder_DO_Extend"] = True
    return action_dict

def crane_liftingCylinder_DO_Extend_False(action_dict):
    action_dict["crane_liftingCylinder_DO_Extend"] = False
    return action_dict

def crane_table_DO_TurnClockwise_True(action_dict):
    action_dict["crane_table_DO_TurnClockwise"] = True
    return action_dict

def crane_table_DO_TurnClockwise_False(action_dict):
    action_dict["crane_table_DO_TurnClockwise"] = False
    return action_dict

def crane_table_DO_TurnCounterClockwise_True(action_dict):
    action_dict["crane_table_DO_TurnCounterClockwise"] = True
    return action_dict

def crane_table_DO_TurnCounterClockwise_False(action_dict):
    action_dict["crane_table_DO_TurnCounterClockwise"] = False
    return action_dict

def crane_DO_Intake_True(action_dict):
    action_dict["crane_DO_Intake"] = True
    return action_dict

def crane_DO_Intake_False(action_dict):
    action_dict["crane_DO_Intake"] = False
    return action_dict

def stamp_slidingCylinder_DO_Extend_True(action_dict):
    action_dict["stamp_slidingCylinder_DO_Extend"] = True
    return action_dict

def stamp_slidingCylinder_DO_Extend_False(action_dict):
    action_dict["stamp_slidingCylinder_DO_Extend"] = False
    return action_dict

def stamp_slidingCylinder_DO_Retract_True(action_dict):
    action_dict["stamp_slidingCylinder_DO_Retract"] = True
    return action_dict

def stamp_slidingCylinder_DO_Retract_False(action_dict):
    action_dict["stamp_slidingCylinder_DO_Retract"] = False
    return action_dict

def stamp_stampingCylinder_DO_Extend_True(action_dict):
    action_dict["stamp_stampingCylinder_DO_Extend"] = True
    return action_dict

def stamp_stampingCylinder_DO_Extend_False(action_dict):
    action_dict["stamp_stampingCylinder_DO_Extend"] = False
    return action_dict

def largesortingconveyor_separator1_DO_Extend_True(action_dict):
    action_dict["largesortingconveyor_separator1_DO_Extend"] = True
    return action_dict

def largesortingconveyor_separator1_DO_Extend_False(action_dict):
    action_dict["largesortingconveyor_separator1_DO_Extend"] = False
    return action_dict

def largesortingconveyor_separator2_DO_Extend_True(action_dict):
    action_dict["largesortingconveyor_separator2_DO_Extend"] = True
    return action_dict

def largesortingconveyor_separator2_DO_Extend_False(action_dict):
    action_dict["largesortingconveyor_separator2_DO_Extend"] = False
    return action_dict

def largesortingconveyor_TurnClockwise_True(action_dict):
    action_dict["largesortingconveyor_TurnClockwise"] = True
    return action_dict

def largesortingconveyor_TurnClockwise_False(action_dict):
    action_dict["largesortingconveyor_TurnClockwise"] = False
    return action_dict

def largesortingconveyor_TurnCounterClockwise_True(action_dict):
    action_dict["largesortingconveyor_TurnCounterClockwise"] = True
    return action_dict

def largesortingconveyor_TurnCounterClockwise_False(action_dict):
    action_dict["largesortingconveyor_TurnCounterClockwise"] = False
    return action_dict

def largesortingconveyor_switch_DO_Extend_True(action_dict):
    action_dict["largesortingconveyor_switch_DO_Extend"] = True
    return action_dict

def largesortingconveyor_switch_DO_Extend_False(action_dict):
    action_dict["largesortingconveyor_switch_DO_Extend"] = False
    return action_dict

def picalphaconveyor_DO_TurnClockwise_True(action_dict):
    action_dict["picalphaconveyor_DO_TurnClockwised"] = True
    return action_dict

def picalphaconveyor_DO_TurnClockwise_False(action_dict):
    action_dict["picalphaconveyor_DO_TurnClockwised"] = False
    return action_dict

def picalphaconveyor_DO_TurnCounterClockwise_True(action_dict):
    action_dict["picalphaconveyor_DO_TurnCounterClockwise"] = True
    return action_dict

def picalphaconveyor_DO_TurnCounterClockwise_False(action_dict):
    action_dict["picalphaconveyor_DO_TurnCounterClockwise"] = False
    return action_dict

def picalphaconveyor_switch_DO_Extend_True(action_dict):
    action_dict["picalphaconveyor_switch_DO_Extend"] = True
    return action_dict

def picalphaconveyor_switch_DO_Extend_False(action_dict):
    action_dict["picalphaconveyor_switch_DO_Extend"] = False
    return action_dict

def smallsortingconveyor_DO_TurnClockwise_True(action_dict):
    action_dict["smallsortingconveyor_DO_TurnClockwise"] = True
    return action_dict

def smallsortingconveyor_DO_TurnClockwise_False(action_dict):
    action_dict["smallsortingconveyor_DO_TurnClockwise"] = False
    return action_dict

def smallsortingconveyor_DO_TurnCounterClockwise_True(action_dict):
    action_dict["smallsortingconveyor_DO_TurnCounterClockwise"] = True
    return action_dict

def smallsortingconveyor_DO_TurnCounterClockwise_False(action_dict):
    action_dict["smallsortingconveyor_DO_TurnCounterClockwise"] = False
    return action_dict

def smallsortingconveyor_switch_DO_Extend_True(action_dict):
    action_dict["smallsortingconveyor_switch_DO_Extend"] = True
    return action_dict

def smallsortingconveyor_switch_DO_Extend_False(action_dict):
    action_dict["smallsortingconveyor_switch_DO_Extend"] = False
    return action_dict

def refillconveyor_DO_TurnClockwise_True(action_dict):
    action_dict["refillconveyor_DO_TurnClockwise"] = True
    return action_dict

def refillconveyor_DO_TurnClockwise_False(action_dict):
    action_dict["refillconveyor_DO_TurnClockwise"] = False
    return action_dict

def refillconveyor_DO_TurnCounterClockwise_True(action_dict):
    action_dict["refillconveyor_DO_TurnCounterClockwise"] = True
    return action_dict

def refillconveyor_DO_TurnCounterClockwise_False(action_dict):
    action_dict["refillconveyor_DO_TurnCounterClockwise"] = False
    return action_dict

def refillconveyor_switch_DO_Extend_True(action_dict):
    action_dict["refillconveyor_switch.DO_Extend"] = True
    return action_dict

def refillconveyor_switch_DO_Extend_False(action_dict):
    action_dict["refillconveyor_switch.DO_Extend"] = False
    return action_dict

def picalpha_DO_Intake_True(action_dict):
    action_dict["picalpha_DO_Intake"] = True
    return action_dict

def picalpha_DO_Intake_False(action_dict):
    action_dict["picalpha_DO_Intake"] = False
    return action_dict

def picalpha_carrier_DO_TurnClockwise_True(action_dict):
    action_dict["picalpha_carrier_DO_TurnClockwise"] = True
    return action_dict

def picalpha_carrier_DO_TurnClockwise_False(action_dict):
    action_dict["picalpha_carrier_DO_TurnClockwise"] = False
    return action_dict

def picalpha_carrier_DO_TurnCounterClockwise_True(action_dict):
    action_dict["picalpha_carrier_DO_TurnCounterClockwise"] = True
    return action_dict

def picalpha_carrier_DO_TurnCounterClockwise_False(action_dict):
    action_dict["picalpha_carrier_DO_TurnCounterClockwise"] = False
    return action_dict

def DO_Nothing(action_dict):
    return action_dict