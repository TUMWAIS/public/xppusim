from abc import ABC, abstractmethod


class AbstractGoal(ABC):
    """
    The abstract base class used for all goals.

    This object is handed over to the XPPUEnv during initialization,
    as the parameters achieved_goal and desired_goal.

    The desired_goal is the goal which the agent tries to reach.
    The achieved_goal is the goal the agent actually achieved.

    The XPPUEnv class calls the update() and
    compute_reward() method of the achieved_goal
    in each step and sets its done flag
    if the achieved_goal.done flag is set.

    Goals must be able to be reset and possible exchanged
    during the training. Keep this in mind when implementing
    custom goal objects.
    """

    def __init__(self,
                 state_space_mapping,
                 info):

        self.info = info
        self.state_space_mapping = state_space_mapping
        self.goal_space_mapping, self.shape = self._get_goal_space_mapping()

        self.desired_goal = None
        self.achieved_goal = None

        self.done = False
        self.max_reward = 0


    @abstractmethod
    def reset(self, observation, info):
        self.observation = observation.copy()
        self.info = info
        self.done = False


    @abstractmethod
    def update(self, observation, info):
        self.previous_observation = self.observation.copy()
        self.observation = observation.copy()
        self.info = info


    @abstractmethod
    def compute_reward(self):
        """
        Computes the reward.

        All sensor values and the workpiece data in the info dict
        can be used to calculate the reward.

        :param observation: The observations returned by XPPUEnv's step() method.
        :param info: The info dict returned by XPPUEnv's step() method.
        :param other_goal: Another goal objects which this must be compared against.
        """
        pass


    @abstractmethod
    def _get_goal_space_mapping(self):
        """
        Returns a list that maps the indices of the goal vector into workpiece-specific goal groups as well as a
        tuple describing the shape of the goal.

        [Workpiece index]
            [Goal variable]: [Index in the goal vector]

        :param info: Info dictionary of the last transition
        :return goal_space_mapping: List of dictionaries containing the mapping
        :return shape: Tuple containing the shape of the goal
        """
        goal_space_mapping = {}
        goal_space_mapping["workpiece"] = []
        goal_space_mapping["system"] = {}

        shape = None

        return goal_space_mapping, shape