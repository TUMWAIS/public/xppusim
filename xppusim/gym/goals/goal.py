from abc import ABC, abstractmethod


class Goal(ABC):
    """
    The abstract base class used for all goals.

    This object is handed over to the XPPUEnv during initialization,
    as the parameters achieved_goal and desired_goal.

    The desired_goal is the goal which the agent tries to reach.
    The achieved_goal is the goal the agent actually achieved.

    The XPPUEnv class calls the update() and
    compute_reward() method of the achieved_goal
    in each step and sets its done flag
    if the achieved_goal.done flag is set.

    Goals must be able to be reset and possible exchanged
    during the training. Keep this in mind when implementing
    custom goal objects.


    """
    def __init__(self, is_desired_goal=True, is_achieved_goal=False):
        """
        :param is_desired_goal: The goal you want to achieve.
        :param is_achieved_goal: The goal your agent has achieved at the moment.
        """
        self.is_desired_goal = is_desired_goal
        self.is_achieved_goal = is_achieved_goal
        self.shape = (3, )
        self.goal = (0, 0, 0)
        self.done = False

    @abstractmethod
    def reset(self):
        pass

    @abstractmethod
    def update(self, observation, info):
        pass

    @abstractmethod
    def compute_reward(self, observation, info,  other_goal):
        """
        Computes the reward.

        All sensor values and the workpiece data in the info dict
        can be used to calculate the reward.

        :param observation: The observations returned by XPPUEnv's step() method.
        :param info: The info dict returned by XPPUEnv's step() method.
        :param other_goal: Another goal objects which this must be compared against.
        """
        pass

    @abstractmethod
    def make_achieved_goal(self):
        """
        Returns an initialized achieved goal object.

        :return: Object of type Goal
        """
        pass
