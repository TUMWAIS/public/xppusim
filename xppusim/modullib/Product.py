import bpy


class Product:
    def __init__(self, name, weight, wp_register):
        self.id = len(wp_register)
        self.name = name
        self.weight = weight
        self.stored = False
        self.attached = False
        self.pos = bpy.data.objects[name].location.copy()
        self.dim = bpy.data.objects[name].dimensions.copy()
        self.RFID_data = {"Stamped": False, "Storage position": ""}

        if name.find("black") != -1:
            self.color = "black"
            self.color_black = True
            self.color_white = False
            self.color_metallic = False
        if name.find("white") != -1:
            self.color = "white"
            self.color_black = False
            self.color_white = True
            self.color_metallic = False
        if name.find("metallic") != -1:
            self.color = "metallic"
            self.color_black = False
            self.color_white = False
            self.color_metallic = True

        wp_register.append(self)