import math
import xppumathlib
from mathutils import Vector


class Module():
    def __init__(self):
        self.e_precision = 0.0001   # Currently not in use, standard of xppumathlib utilized

    # Checks if a workpiece at the current (or future) position overlapps with any other workpiece
    def overlapp(self, wp, wp_register, step=Vector([0,0,0])):
        result = False
        for other_wp in wp_register:
            if wp != other_wp:
                if xppumathlib.wp_at_pos(other_wp.pos, wp, step=step, dim=other_wp.dim):
                    result =  True
        return result

    # Checks whether two points are within one step size
    # (More exact compared to wp_at_pos())
    def pos_in_range(self, pos1, pos2, step_size):                                     # A little bit less than 1 step
        if abs(pos1.x-pos2.x)-self.e_precision <= abs(0.99*step_size.x) and\
                abs(pos1.y-pos2.y)-self.e_precision <= abs(0.99*step_size.y) and\
                abs(pos1.z-pos2.z)-self.e_precision <= abs(0.99*step_size.z):
            return True
        else:
            return False

    # Exact comparison of two coordinates
    def pos_compare(self, pos1, pos2):
        if (abs(pos1.x - pos2.x) <= self.e_precision and
                abs(pos1.y - pos2.y) <= self.e_precision and
                abs(pos1.z - pos2.z) <= self.e_precision):
            return True
        else:
            return False