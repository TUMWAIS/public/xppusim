import os
import numpy as np
import subprocess
import bpy
from pathlib import Path
from mathutils import Vector

# import bpy
from xppusim.modullib.Product import Product
from xppusim.modullib.modules.Crane import Crane
from xppusim.modullib.modules.LargeSortingConveyor import LargeSortingConveyor
from xppusim.modullib.modules.PicAlpha import PicAlpha
from xppusim.modullib.modules.PicAlphaConveyor import PicAlphaConveyor
from xppusim.modullib.modules.ReFillConveyor import ReFillConveyor
from xppusim.modullib.modules.SmallSortingConveyor import SmallSortingConveyor
from xppusim.modullib.modules.Stack import Stack
from xppusim.modullib.modules.Stamp import Stamp


class XPPU():

    def __init__(self,
                 rel_model_path="/model/XPPU_8.blend",
                 rel_renderscript_path="/scripts/run_blender.py",
                 wp_orders=None,
                 render=False,
                 wp_weights=None,
                 position = None,
                 stamp_setup_time=0,
                 stamp_wp_setup='black',
                 frames_per_render=1,
                 steps_per_render=1,
                 logging=False,
                 crane_turn_only_extended=True,
                 crane_move_at_pickpoint=True,
                 normalize_states=False):

        # Paths variables
        root_path = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)),os.pardir))  # Get root path assuming that project structure is kept
        self.model_path = root_path + rel_model_path
        self.renderscript_path = root_path + rel_renderscript_path

        # Global model variables
        self.module_register = []
        self.wp_register = []
        self.normalize_states = normalize_states

        # Specify important position
        self.position_dict = {"initial": [103.8773, 27.0000, 6.8000],       # 0.799, 0.208, 0.227
                              "at_stack": [96.3584, 27.0000, 6.8000],       # 0.741, 0.208, 0.227
                              "at_stamp": [52.4416, 27.0000, 6.8000],       # 0.403, 0.208, 0.227
                              "in_stamp": [39.4916, 27.0000, 6.8000],       # 0.303, 0.208, 0.227
                              "at_lsc_pos0": [74.4000, 48.9584, 6.8000],    # 0.572, 0.376, 0.227
                              "at_lsc_pos1": [74.4000, 58.9584, 6.8000],    # 0.572, 0.454, 0.227
                              "at_lsc_ramp1": [74.4000, 70.9584, 6.8000],   # 0.572, 0.542, 0.227
                              "at_lsc_ramp2": [74.4000, 98.9584, 6.8000],   # 0.572, 0.761, 0.227
                              "at_stack_up": [96.3584, 27.0000, 12.8000],   # 0.741, 0.208, 0.427
                              "at_stamp_up": [52.4416, 27.0000, 12.8000],   # 0.403, 0.208, 0.427
                              "at_lsc_up": [74.4000, 48.9584, 12.8000],
                              "at_ramp1_pos1": [55.0000, 70.5000, 1.356],
                              "at_ramp1_pos2": [60.0000, 70.5000, 3.0134],
                              "at_ramp1_pos3": [65.0000, 70.5000, 4.6710],
                              "at_ramp1_pos4": [70.0000, 70.5000, 6.0227],
                              "at_ramp2_pos1": [55.0000, 98.5000, 1.3568],
                              "at_ramp2_pos2": [60.0000, 98.5000, 3.0134],
                              "at_ramp2_pos3": [65.0000, 98.5000, 4.6710],
                              "at_ramp2_pos4": [70.0000, 98.5000, 6.0227],
                              "at_ramp3_pos1": [74.4000, 120.8741, 0.6808],
                              "at_ramp3_pos2": [74.4000, 115.9923, 2.3142],
                              "at_ramp3_pos3": [74.4000, 111.1113, 3.9474],
                              "at_ramp3_pos4": [74.4000, 106.2225, 5.5831],
                              "at_ramp4_pos1": [9.3000, 26.1604, 3.5289],
                              "at_lsc_btw_ramps": [74.4000, 80.0000, 6.8000],
                              "at_lsc_ramp3": [74.4000, 102.0000, 6.8000],
                              "at_lsc_pos_error": [74.4, 49, 6.8]}

        # Internal workpiece names
        self.wp_names = ["workpiece.black1", "workpiece.black2", "workpiece.black3", "workpiece.black4",
                         "workpiece.black5",
                         "workpiece.white1", "workpiece.white2", "workpiece.white3", "workpiece.white4",
                         "workpiece.white5",
                         "workpiece.metallic1", "workpiece.metallic2", "workpiece.metallic3", "workpiece.metallic4",
                         "workpiece.metallic5"]

        # Workpiece data
        if wp_orders is not None:
            self.wp_orders = self._get_wp_indices_for_config(wp_orders)
            self.num_wps = len(wp_orders)
        else:
            self.num_wps = 5
            self.wp_orders = np.random.choice(range(14), self.num_wps, replace=False)

        if wp_weights is not None:
            self.wp_weights = wp_weights
        else:
            self.wp_weights = [10, 15, 12, 13, 11,
                               20, 22, 18, 14, 20,
                               50, 55, 60, 45, 62]

        # Stamp configuration
        self.stamp_wp_setup = stamp_wp_setup
        self.stamp_setup_time = stamp_setup_time

        # Blender variables
        self.timestep = 0  # Counter for the actual time step of the simulation
        self.render_frame = 0  # Counter for the current rendering frame
        self.render = render
        self.frames_per_render = frames_per_render  # Slows down video playback and increases render time
        self.steps_per_render = steps_per_render  # Increases render time, however, with loss of render quality

        # Logging
        self.logging = logging

        # Build model
        bpy.ops.wm.open_mainfile(filepath=self.model_path)  # Open blender model
        self.stack = Stack("stack", self.module_register, self.render_frame, self.render)
        self.crane = Crane("crane", self.module_register, self.render_frame, self.render, turn_only_extended=crane_turn_only_extended, move_at_pickpoint=crane_move_at_pickpoint)
        self.stamp = Stamp("stamp", self.module_register, wp_setup=self.stamp_wp_setup, frame=self.render_frame, render=self.render)
        self.largesortingconveyor = LargeSortingConveyor("largesortingconveyor", self.num_wps, self.module_register, self.render_frame, self.render)
        self.picalphaconveyor = PicAlphaConveyor("picalphaconveyor", self.num_wps, self.module_register, self.render_frame, self.render)
        self.smallsortingconveyor = SmallSortingConveyor("smallsortingconveyor", self.num_wps, self.module_register, self.render_frame, self.render)
        self.refillconveyor = ReFillConveyor("refillconveyor", self.num_wps, self.module_register, self.render_frame, self.render)
        self.picalpha = PicAlpha("picalpha", self.module_register, self.render_frame, self.render)

        # Setup workpieces
        if position is None:
            self.create_wps(self.wp_orders)
        else:
            self.create_single_workpiece_at(position)

        self.CoordPredictor = None

    def reset(self, wp_new_order=None, position=None, render=None):
        # Delete all keyframes
        for obj in bpy.context.scene.objects:
            obj.animation_data_clear()

        # Reset variables
        self.timestep = 0
        self.render_frame = 0

        if render is not None:
            self.render = render

        # Reset modules
        self.stack.reset(self.render_frame, self.render)
        self.crane.reset(self.render_frame, self.render)
        self.stamp.reset(self.stamp_wp_setup, self.render_frame, self.render)
        self.largesortingconveyor.reset(self.render_frame, self.render)
        self.picalphaconveyor.reset(self.render_frame, self.render)
        self.smallsortingconveyor.reset(self.render_frame, self.render)
        self.refillconveyor.reset(self.render_frame, self.render)
        self.picalpha.reset(self.render_frame, self.render)

        # Create new workpiece configuration
        if wp_new_order is not None:
            self.wp_orders = self._get_wp_indices_for_config(wp_new_order)
        else:
            self.wp_orders = np.random.choice(range(14), self.num_wps, replace=False)

        # Reset workpieces
        if position is None:
            self.create_wps(self.wp_orders)
        else:
            self.create_single_workpiece_at(position)

        # Make single step to update all sensors    #TODO: could be avoided by improving the reset function
        self.make_step()

        # Store initial sensor values if a coordinate predictor is used
        if self.CoordPredictor is not None:
            state = self.get_states_from_sim(normalized=True)
            action = self.get_actions()
            self.CoordPredictor.reset(state, action)


    def make_step(self, num=1):

        for step in range(num):

            # Store states before step
            if self.CoordPredictor is not None:
                state = self.get_states_from_sim(normalized=True)
                action = self.get_actions()

            self.timestep += 1
            if self.render is True and self.timestep % self.steps_per_render == 0:  # Skip time steps when rendering ToDO: Check
                self.render_frame += self.frames_per_render  # Number of frames to move forward with each time step
                for module in self.module_register:
                    module.update(self.wp_register, self.render_frame, self.render, self.logging)
            else:
                for module in self.module_register:
                    module.update(self.wp_register, 0, False, self.logging)

            if self.render is True:
                bpy.data.scenes["Scene"].frame_end = self.render_frame  # Adjust length of scene

            # Store sensor values if a coordinate predictor is used
            if self.CoordPredictor is not None:
                state_next = self.get_states_from_sim(normalized=True)
                self.CoordPredictor.update(state, action, state_next)


    def get_states_from_sim(self, normalized=False):

        def normalize_VI_WPPosition(VI_WPPosition, max_VI_WPPosition):
            result = []
            for value in VI_WPPosition:
                if value != -1:
                    result.append(value/max_VI_WPPosition)
                else:
                    result.append(-1)

            return result

        def encode_storage_pos(pos_name, ramp_num):

            if ramp_num == 1 and pos_name == "largesortingconveyor.ramp1":
                return 1
            elif ramp_num == 2 and pos_name == "largesortingconveyor.ramp2":
                return 1
            elif ramp_num == 3 and pos_name == "largesortingconveyor.ramp3":
                return 1
            elif ramp_num == 4 and pos_name == "smallsortingconveyor.ramp1":
                return 1
            else:
                return 0

        if normalized is True:
            max_AI_Position_Cylinder = 20
            max_AI_Position_Table = 360
            max_AI_WPPosition = 50
            max_AI_WPWeight = 100
            max_VI_Position_Stack = 10
            max_AI_Position_Ramp = 20
            max_VI_WPPosition_LSC = 60
            max_VI_WPPosition_PAC = 60
            max_VI_WPPosition_SSC = 60
            max_VI_WPPosition_RFC = 60
            max_pos_x = 130
            max_pos_y = 130
            max_pos_z = 30
        else:
            max_AI_Position_Cylinder = 1
            max_AI_Position_Table = 1
            max_AI_WPPosition = 1
            max_AI_WPWeight = 1
            max_AI_Position_Ramp = 1
            max_VI_Position_Stack = 1
            max_VI_WPPosition_LSC = 1
            max_VI_WPPosition_PAC = 1
            max_VI_WPPosition_SSC = 1
            max_VI_WPPosition_RFC = 1
            max_pos_x = 1
            max_pos_y = 1
            max_pos_z = 1

        states = {}
        # Component and Module states
        states["stack.separator.DI_Extended"] = int(self.stack.separator.DI_Extended)
        states["stack.separator.DI_Retracted"] = int(self.stack.separator.DI_Retracted)
        states["stack.separator.AI_Position"] = self.stack.separator.AI_Position/max_AI_Position_Cylinder
        states["stack.DI_WPProvided"] = int(self.stack.DI_WPProvided)
        states["stack.DI_WPLight"] = int(self.stack.DI_WPLight)
        states["stack.DI_WPMetallic"] = int(self.stack.DI_WPMetallic)
        states["stack.AI_WPWeight"] = self.stack.AI_WPWeight/max_AI_WPWeight
        states["stack.VI_Position"] = self.stack.VI_Position/max_VI_Position_Stack
        states["crane.liftingCylinder.DI_Extended"] = int(self.crane.liftingCylinder.DI_Extended)
        states["crane.liftingCylinder.DI_Retracted"] = int(self.crane.liftingCylinder.DI_Retracted)
        states["crane.liftingCylinder.AI_Position"] = self.crane.liftingCylinder.AI_Position/max_AI_Position_Cylinder
        states["crane.DI_TakenIn"] = int(self.crane.DI_TakenIn)
        states["crane.table.DI_AtConveyor"] = int(self.crane.table.DI_AtConveyor)
        states["crane.table.DI_AtStack"] = int(self.crane.table.DI_AtStack)
        states["crane.table.DI_AtStamp"] = int(self.crane.table.DI_AtStamp)
        states["crane.table.AI_Position"] = self.crane.table.AI_Position/max_AI_Position_Table
        states["stamp.slidingCylinder.DI_Extended"] = int(self.stamp.slidingCylinder.DI_Extended)
        states["stamp.slidingCylinder.DI_Retracted"] = int(self.stamp.slidingCylinder.DI_Retracted)
        states["stamp.slidingCylinder.AI_Position"] = self.stamp.slidingCylinder.AI_Position/max_AI_Position_Cylinder
        states["stamp.stampingCylinder.DI_Extended"] = int(self.stamp.stampingCylinder.DI_Extended)
        states["stamp.stampingCylinder.DI_Retracted"] = int(self.stamp.stampingCylinder.DI_Retracted)
        states["stamp.stampingCylinder.AI_Position"] = self.stamp.stampingCylinder.AI_Position/max_AI_Position_Cylinder
        states["stamp.DI_WPAvailable"] = int(self.stamp.DI_WPAvailable)
        states["largesortingconveyor.DI_WPPosition0"] = int(self.largesortingconveyor.DI_WPPosition0)
        states["largesortingconveyor.DI_WPPosition1"] = int(self.largesortingconveyor.DI_WPPosition1)
        states["largesortingconveyor.DI_WPPosition2"] = int(self.largesortingconveyor.DI_WPPosition2)
        states["largesortingconveyor.DI_WPLight1"] = int(self.largesortingconveyor.DI_WPLight1)
        states["largesortingconveyor.DI_WPLight2"] = int(self.largesortingconveyor.DI_WPLight2)
        states["largesortingconveyor.DI_WPMetallic1"] = int(self.largesortingconveyor.DI_WPMetallic1)
        states["largesortingconveyor.DI_WPMetallic2"] = int(self.largesortingconveyor.DI_WPMetallic2)
        for idx in range(len(self.wp_register)):
            states["largesortingconveyor.VI_WPPosition_" + str(idx)] = normalize_VI_WPPosition(self.largesortingconveyor.VI_WPPosition, max_VI_WPPosition_LSC)[idx]
        states["largesortingconveyor.separator1.DI_Extended"] = int(self.largesortingconveyor.separator1.DI_Extended)
        states["largesortingconveyor.separator1.DI_Retracted"] = int(self.largesortingconveyor.separator1.DI_Retracted)
        states["largesortingconveyor.separator1.AI_Position"] = self.largesortingconveyor.separator1.AI_Position/max_AI_Position_Cylinder
        states["largesortingconveyor.separator2.DI_Extended"] = int(self.largesortingconveyor.separator2.DI_Extended)
        states["largesortingconveyor.separator2.DI_Retracted"] = int(self.largesortingconveyor.separator2.DI_Retracted)
        states["largesortingconveyor.separator2.AI_Position"] = self.largesortingconveyor.separator2.AI_Position/max_AI_Position_Cylinder
        states["largesortingconveyor.ramp1.DI_RampFull"] = int(self.largesortingconveyor.ramp1.DI_RampFull)
        states["largesortingconveyor.ramp2.DI_RampFull"] = int(self.largesortingconveyor.ramp2.DI_RampFull)
        states["largesortingconveyor.ramp3.DI_RampFull"] = int(self.largesortingconveyor.ramp3.DI_RampFull)
        states["largesortingconveyor.switch.DI_Extended"] = int(self.largesortingconveyor.switch.DI_Extended)
        states["largesortingconveyor.switch.DI_Retracted"] = int(self.largesortingconveyor.switch.DI_Retracted)
        states["largesortingconveyor.ramp1.AI_Position"] = self.largesortingconveyor.ramp1.AI_Position/max_AI_Position_Ramp
        states["largesortingconveyor.ramp2.AI_Position"] = self.largesortingconveyor.ramp2.AI_Position/max_AI_Position_Ramp
        states["largesortingconveyor.ramp3.AI_Position"] = self.largesortingconveyor.ramp3.AI_Position/max_AI_Position_Ramp
        states["picalphaconveyor.DI_WPPosition1"] = int(self.picalphaconveyor.DI_WPPosition1)
        states["picalphaconveyor.DI_WPPosition2"] = int(self.picalphaconveyor.DI_WPPosition2)
        states["picalphaconveyor.AI_WPPosition"] = self.picalphaconveyor.AI_WPPosition/max_AI_WPPosition
        states["picalphaconveyor.VI_WPPosition"] = normalize_VI_WPPosition(self.picalphaconveyor.VI_WPPosition, max_VI_WPPosition_PAC)
        states["picalphaconveyor.switch.DI_Extended"] = int(self.picalphaconveyor.switch.DI_Extended)
        states["picalphaconveyor.switch.DI_Retracted"] = int(self.picalphaconveyor.switch.DI_Retracted)
        states["smallsortingconveyor.DI_WPPosition1"] = int(self.smallsortingconveyor.DI_WPPosition1)
        states["smallsortingconveyor.DI_WPPosition2"] = int(self.smallsortingconveyor.DI_WPPosition2)
        states["smallsortingconveyor.DI_WPPosition3"] = int(self.smallsortingconveyor.DI_WPPosition3)
        states["smallsortingconveyor.DI_WPMetallic1"] = int(self.smallsortingconveyor.DI_WPMetallic1)
        states["smallsortingconveyor.DI_WPLight1"] = int(self.smallsortingconveyor.DI_WPLight1)
        states["smallsortingconveyor.VI_WPPosition"] = normalize_VI_WPPosition(self.smallsortingconveyor.VI_WPPosition, max_VI_WPPosition_SSC)
        states["smallsortingconveyor.ramp1.DI_RampFull"] = int(self.smallsortingconveyor.ramp1.DI_RampFull)
        states["smallsortingconveyor.switch.DI_Extended"] = int(self.smallsortingconveyor.switch.DI_Extended)
        states["smallsortingconveyor.switch.DI_Retracted"] = int(self.smallsortingconveyor.switch.DI_Retracted)
        states["refillconveyor.DI_WPPosition1"] = int(self.refillconveyor.DI_WPPosition1)
        states["refillconveyor.DI_WPPosition2"] = int(self.refillconveyor.DI_WPPosition2)
        states["refillconveyor.switch.DI_Extended"] = int(self.refillconveyor.switch.DI_Extended)
        states["refillconveyor.switch.DI_Retracted"] = int(self.refillconveyor.switch.DI_Retracted)
        states["refillconveyor.VI_WPPosition"] = normalize_VI_WPPosition(self.refillconveyor.VI_WPPosition, max_VI_WPPosition_RFC)
        states["picalpha.DI_TakenIn"] = int(self.picalpha.DI_TakenIn)
        states["picalpha.DI_AtPutDown1"] = int(self.picalpha.carrier.DI_AtPutDown1)
        states["picalpha.DI_AtPutDown2"] = int(self.picalpha.carrier.DI_AtPutDown2)
        states["picalpha.DI_AtPickup1"] = int(self.picalpha.carrier.DI_AtPickup1)
        states["picalpha.DI_AtPickup2"] = int(self.picalpha.carrier.DI_AtPickup2)
        states["picalpha.DI_AtEndLeft"] = int(self.picalpha.carrier.DI_AtEndLeft)
        states["picalpha.DI_AtEndRight"] = int(self.picalpha.carrier.DI_AtEndRight)
        states["picalpha.liftingCylinder.DI_Extended"] = int(self.picalpha.liftingCylinder.DI_Extended)
        states["picalpha.liftingCylinder.DI_Retracted"] = int(self.picalpha.liftingCylinder.DI_Retracted)
        states["picalpha.liftingCylinder.AI_Position"] = int(self.picalpha.liftingCylinder.AI_Position)


        # Workpiece states
        states["workpiece"] = []
        for wp in self.wp_register:
            wp_state = {}
            wp_state["position.x"] = wp.pos.x / max_pos_x
            wp_state["position.y"] = wp.pos.y / max_pos_y
            wp_state["position.z"] = wp.pos.z / max_pos_z
            wp_state["stamped"] = int(wp.RFID_data["Stamped"])
            wp_state["color_black"] = int(wp.color_black)
            wp_state["color_white"] = int(wp.color_white)
            wp_state["color_metallic"] = int(wp.color_metallic)
            wp_state["stored"] = int(wp.stored)
            wp_state["stored_ramp1"] = encode_storage_pos(wp.RFID_data["Storage position"], 1)
            wp_state["stored_ramp2"] = encode_storage_pos(wp.RFID_data["Storage position"], 2)
            wp_state["stored_ramp3"] = encode_storage_pos(wp.RFID_data["Storage position"], 3)
            wp_state["stored_ramp4"] = encode_storage_pos(wp.RFID_data["Storage position"], 4)

            wp_state["position_real.x"] = wp.pos.x / max_pos_x
            wp_state["position_real.y"] = wp.pos.y / max_pos_y
            wp_state["position_real.z"] = wp.pos.z / max_pos_z

            states["workpiece"].append(wp_state)

        return states



    def get_states(self):

        states = self.get_states_from_sim(self.normalize_states)

        if self.normalize_states is False:
            x_norm = 130
            y_norm = 130
            z_norm = 30
        else:
            x_norm = 1
            y_norm = 1
            z_norm = 1

        if self.CoordPredictor is not None:
            pos_pred = self.CoordPredictor.predict()
            states["workpiece"][0]["position.x"] = pos_pred[0] * x_norm
            states["workpiece"][0]["position.y"] = pos_pred[1] * y_norm
            states["workpiece"][0]["position.z"] = pos_pred[2] * z_norm

        return states



    def get_actions(self):
        actions = {}
        actions["stack_separator_DO_Extend"] = self.stack.separator.DO_Extend
        actions["crane_liftingCylinder_DO_Extend"] = self.crane.liftingCylinder.DO_Extend
        actions["crane_table_DO_TurnClockwise"] = self.crane.table.DO_TurnClockwise
        actions["crane_table_DO_TurnCounterClockwise"] = self.crane.table.DO_TurnCounterClockwise
        actions["crane_DO_Intake"] = self.crane.DO_Intake
        actions["stamp_slidingCylinder_DO_Extend"] = self.stamp.slidingCylinder.DO_Extend
        actions["stamp_slidingCylinder_DO_Retract"] = self.stamp.slidingCylinder.DO_Retract
        actions["stamp_stampingCylinder_DO_Extend"] = self.stamp.stampingCylinder.DO_Extend
        actions["largesortingconveyor_separator1_DO_Extend"] = self.largesortingconveyor.separator1.DO_Extend
        actions["largesortingconveyor_separator2_DO_Extend"] = self.largesortingconveyor.separator2.DO_Extend
        actions["largesortingconveyor_DO_TurnClockwise"] = self.largesortingconveyor.DO_TurnClockwise
        actions["largesortingconveyor_DO_TurnCounterClockwise"] = self.largesortingconveyor.DO_TurnCounterClockwise
        actions["largesortingconveyor_switch_DO_Extend"] = self.largesortingconveyor.switch.DO_Extend
        actions["picalphaconveyor_DO_TurnClockwise"] = self.picalphaconveyor.DO_TurnClockwise
        actions["picalphaconveyor_DO_TurnCounterClockwise"] = self.picalphaconveyor.DO_TurnCounterClockwise
        actions["picalphaconveyor_switch_DO_Extend"] =  self.picalphaconveyor.switch.DO_Extend
        actions["smallsortingconveyor_DO_TurnClockwise"] = self.smallsortingconveyor.DO_TurnClockwise
        actions["smallsortingconveyor_DO_TurnCounterClockwise"] = self.smallsortingconveyor.DO_TurnCounterClockwise
        actions["smallsortingconveyor_switch_DO_Extend"] = self.smallsortingconveyor.switch.DO_Extend
        actions["refillconveyor_DO_TurnClockwise"] = self.refillconveyor.DO_TurnClockwise
        actions["refillconveyor_DO_TurnCounterClockwise"] = self.refillconveyor.DO_TurnCounterClockwise
        actions["refillconveyor_switch_DO_Extend"] = self.refillconveyor.switch.DO_Extend
        actions["picalpha_DO_Intake"] =  self.picalpha.DO_Intake
        actions["picalpha_carrier_DO_TurnClockwise"] = self.picalpha.carrier.DO_TurnClockwise
        actions["picalpha_carrier_DO_TurnCounterClockwise"] =  self.picalpha.carrier.DO_TurnCounterClockwise
        actions["picalpha_liftingCylinder_DO_Extend"] = self.picalpha.liftingCylinder.DO_Extend

        return actions

    def set_actions(self, actions):

        if "stack_separator_DO_Extend" in actions:
            self.stack.separator.DO_Extend = actions["stack_separator_DO_Extend"]
        if "crane_liftingCylinder_DO_Extend" in actions:
            self.crane.liftingCylinder.DO_Extend = actions["crane_liftingCylinder_DO_Extend"]
        if "crane_table_DO_TurnClockwise" in actions:
            self.crane.table.DO_TurnClockwise = actions["crane_table_DO_TurnClockwise"]
        if "crane_table_DO_TurnCounterClockwise" in actions:
            self.crane.table.DO_TurnCounterClockwise = actions["crane_table_DO_TurnCounterClockwise"]
        if "crane_DO_Intake" in actions:
            self.crane.DO_Intake = actions["crane_DO_Intake"]
        if "stamp_slidingCylinder_DO_Extend" in actions:
            self.stamp.slidingCylinder.DO_Extend = actions["stamp_slidingCylinder_DO_Extend"]
        if "stamp_slidingCylinder_DO_Retract" in actions:
            self.stamp.slidingCylinder.DO_Retract = actions["stamp_slidingCylinder_DO_Retract"]
        if "stamp_stampingCylinder_DO_Extend" in actions:
            self.stamp.stampingCylinder.DO_Extend = actions["stamp_stampingCylinder_DO_Extend"]
        if "largesortingconveyor_separator1_DO_Extend" in actions:
            self.largesortingconveyor.separator1.DO_Extend = actions["largesortingconveyor_separator1_DO_Extend"]
        if "largesortingconveyor_separator2_DO_Extend" in actions:
            self.largesortingconveyor.separator2.DO_Extend = actions["largesortingconveyor_separator2_DO_Extend"]
        if "largesortingconveyor_DO_TurnClockwise" in actions:
            self.largesortingconveyor.DO_TurnClockwise = actions["largesortingconveyor_DO_TurnClockwise"]
        if "largesortingconveyor_DO_TurnCounterClockwise" in actions:
            self.largesortingconveyor.DO_TurnCounterClockwise = actions["largesortingconveyor_DO_TurnCounterClockwise"]
        if "largesortingconveyor_switch_DO_Extend" in actions:
            self.largesortingconveyor.switch.DO_Extend = actions["largesortingconveyor_switch_DO_Extend"]
        if "picalphaconveyor_DO_TurnClockwise" in actions:
            self.picalphaconveyor.DO_TurnClockwise = actions["picalphaconveyor_DO_TurnClockwise"]
        if "picalphaconveyor_DO_TurnCounterClockwise" in actions:
            self.picalphaconveyor.DO_TurnCounterClockwise = actions["picalphaconveyor_DO_TurnCounterClockwise"]
        if "picalphaconveyor_switch_DO_Extend" in actions:
            self.picalphaconveyor.switch.DO_Extend = actions["picalphaconveyor_switch_DO_Extend"]
        if "smallsortingconveyor_DO_TurnClockwise" in actions:
            self.smallsortingconveyor.DO_TurnClockwise = actions["smallsortingconveyor_DO_TurnClockwise"]
        if "smallsortingconveyor_DO_TurnCounterClockwise" in actions:
            self.smallsortingconveyor.DO_TurnCounterClockwise = actions["smallsortingconveyor_DO_TurnCounterClockwise"]
        if "smallsortingconveyor_switch_DO_Extend" in actions:
            self.smallsortingconveyor.switch.DO_Extend = actions["smallsortingconveyor_switch_DO_Extend"]
        if "refillconveyor_DO_TurnClockwise" in actions:
            self.refillconveyor.DO_TurnClockwise = actions["refillconveyor_DO_TurnClockwise"]
        if "refillconveyor_DO_TurnCounterClockwise" in actions:
            self.refillconveyor.DO_TurnCounterClockwise = actions["refillconveyor_DO_TurnCounterClockwise"]
        if "refillconveyor_switch_DO_Extend" in actions:
            self.refillconveyor.switch.DO_Extend = actions["refillconveyor_switch_DO_Extend"]
        if "picalpha_DO_Intake" in actions:
            self.picalpha.DO_Intake = actions["picalpha_DO_Intake"]
        if "picalpha_carrier_DO_TurnClockwise" in actions:
            self.picalpha.carrier.DO_TurnClockwise = actions["picalpha_carrier_DO_TurnClockwise"]
        if "picalpha_carrier_DO_TurnCounterClockwise" in actions:
            self.picalpha.carrier.DO_TurnCounterClockwise = actions["picalpha_carrier_DO_TurnCounterClockwise"]
        if "picalpha_liftingCylinder_DO_Extend" in actions:
            self.picalpha.liftingCylinder.DO_Extend = actions["picalpha_liftingCylinder_DO_Extend"]


    def save(self, path, filename):
        if not os.path.exists(path + "/blend"): os.makedirs(path + "/blend")
        bpy.ops.wm.save_as_mainfile(filepath=path + "/blend/" + filename)


    def render_file(self, path, filename):
        subprocess.run(["blender", path + "/blend/" + filename, "--python", self.renderscript_path, "-- video_path " +
                        path + "/video/result.avi"], stdout=subprocess.DEVNULL)


    def set_subgoal(self, position):
        position = Vector(position)

        if self.render is True:
            obj = bpy.data.objects["subgoal"]
            obj.location = position.copy()
            obj.keyframe_insert(data_path="location", frame=self.render_frame)


    # Create workpieces in the stack
    def create_wps(self, wp_idx_list=None):
        self.reset_workpieces()

        # Create and register workpieces
        for idx in wp_idx_list:
            Product(self.wp_names[idx], self.wp_weights[idx], self.wp_register)

        # Initialize stack
        self.stack.initialize_magazine(self.wp_register, self.render_frame, self.render)


    # Creates a single workpiece at a provided position
    def create_single_workpiece_at(self, position):
        self.reset_workpieces()

        workpiece = Product(self.wp_names[self.wp_orders[0]], self.wp_weights[0], self.wp_register)
        workpiece.pos = Vector(self.position_dict[position])

        if self.render is True:  # Add initial keyframe
            obj = bpy.data.objects[workpiece.name]
            obj.location = workpiece.pos.copy()
            obj.keyframe_insert(data_path="location", frame=self.render_frame)
            obj.keyframe_insert(data_path="rotation_euler", frame=self.render_frame)


    def _get_wp_indices_for_config(self, wp_in_stack):
        """
        Converts a list with workpiece types to be placed in the stack into a list of unique workpiece indices

        :param wp_in_stack: List of workpiece types, e.g., ["black", "white", "metallic", "black"]
        :return:  List of unique workpiece indices (each index is only included once)
        """
        BLACK_WPS = [0, 1, 2, 3, 4]
        WHITE_WPS = [5, 6, 7, 8, 9]
        METALLIC_WPS = [10, 11, 12, 13, 14]

        wp_idx_list = []
        for wp_name in wp_in_stack:
            if wp_name == "black":
                wp_idx_list.append(BLACK_WPS.pop(0))
            elif wp_name == "white":
                wp_idx_list.append(WHITE_WPS.pop(0))
            elif wp_name == "metallic":
                wp_idx_list.append(METALLIC_WPS.pop(0))

        return wp_idx_list


    def reset_workpieces(self):
        if self.wp_register != []:
            wp_origin_loc = bpy.data.objects["workpiece"].matrix_world.to_translation()
            wp_origin_rot = bpy.data.objects["workpiece"].rotation_euler
            if self.render is True:
                for wp in self.wp_register:
                    obj = bpy.data.objects[wp.name]
                    obj.location = wp_origin_loc
                    obj.rotation_euler = wp_origin_rot
                    obj.keyframe_insert(data_path="location", frame=self.render_frame)
                    obj.keyframe_insert(data_path="rotation_euler", frame=self.render_frame)
            self.wp_register = []


    def get_position_dict(self):
        if self.normalize_states is True:
            normalized_position_dict = {}
            for key, value in self.position_dict.items():
                normalized_position_dict[key] = [value[0] / 130, value[1] / 130, value[2] / 30]
            return normalized_position_dict
        else:
            return self.position_dict