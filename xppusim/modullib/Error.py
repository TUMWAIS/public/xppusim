class Error(Exception):
    pass


class ConfigurationError(Exception):
    """
    Exception raised for errors in the configuration
    """

    def __init__(self, expression, message):
        self.expression = expression
        self.message = message