import math
import xppumathlib
import bpy

from xppusim.modullib.Module import Module


class TurningTable(Module):
    def __init__(self, name, frame=0, render=True):
        super().__init__()
        self.name = name
        self.angle_init = bpy.data.objects[name].rotation_euler.z
        self.angle = self.angle_init
        self.step_size = 5 # Degrees per step, Needs to be chosen so that 0, 90, 180 cam be exactly reached
        self.lower_stop = -50   # Exact value: 51
        self.upper_stop = 220   # Exact value: 218

        self.DO_TurnClockwise = False
        self.DO_TurnCounterClockwise = False

        self.AI_Position = self.angle
        self.DI_AtStack = True if self.angle == 0 else False
        self.DI_AtConveyor = True if self.angle == 90 else False
        self.DI_AtStamp = True if self.angle == 180 else False

        if render is True: # Add initial keyframe
            obj = bpy.data.objects[self.name]
            obj.keyframe_insert(data_path="rotation_euler", frame=frame)

    def reset(self, frame=0, render=True):
        self.angle = self.angle_init
        self.DO_TurnClockwise = False
        self.DO_TurnCounterClockwise = False

        self.AI_Position = self.angle
        self.DI_AtStack = True if self.angle == 0 else False
        self.DI_AtConveyor = True if self.angle == 90 else False
        self.DI_AtStamp = True if self.angle == 180 else False

        if render is True: # Add initial keyframe
            obj = bpy.data.objects[self.name]
            obj.rotation_euler.z = math.radians(self.angle)
            obj.keyframe_insert(data_path="rotation_euler", frame=frame)

    def update(self, WPregister, frame=0, render=True):
        if self.DO_TurnClockwise == True and self.DO_TurnCounterClockwise == False:
            if self.angle > self.lower_stop:
                self.angle -= self.step_size
        elif self.DO_TurnClockwise == False and self.DO_TurnCounterClockwise == True:
            if self.angle < self.upper_stop:
                self.angle += self.step_size

        if self.angle == 360 or self.angle == -360:
            self.angle = 0

        self.AI_Position = self.angle
        self.DI_AtStack = True if self.angle == 0 else False
        self.DI_AtConveyor = True if self.angle == 90 or self.angle == -270 else False
        self.DI_AtStamp = True if self.angle == 180 or self.angle == -180 else False

        if render is True: # Add initial keyframe
            obj = bpy.data.objects[self.name]
            obj.rotation_euler.z = math.radians(self.angle)
            obj.keyframe_insert(data_path="rotation_euler", frame=frame)