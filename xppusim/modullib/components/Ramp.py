import bpy
import xppumathlib
from xppusim.modullib.Module import Module


class Ramp(Module):
    def __init__(self, name, frame=0, render=True, logging=False):
        super().__init__()
        self.name = name
        self.storage_pos_list = [bpy.data.objects[name + ".WPpos1"].matrix_world.to_translation().copy(),
                                 bpy.data.objects[name + ".WPpos2"].matrix_world.to_translation().copy(),
                                 bpy.data.objects[name + ".WPpos3"].matrix_world.to_translation().copy(),
                                 bpy.data.objects[name + ".WPpos4"].matrix_world.to_translation().copy()]
        self.pos_provided = bpy.data.objects[name + ".WPprovided"].matrix_world.to_translation().copy()
        self.ramp_angle = bpy.data.objects[name + ".WPprovided"].rotation_euler.copy()
        self.pos_rampfull = bpy.data.objects[name + ".presencesensor.sensorpos"].matrix_world.to_translation().copy()
        self.stored_wp_list = []
        self.step = (self.storage_pos_list[0] - self.pos_provided) / 10 # Movement of slide per step

        self.DI_RampFull = False
        self.AI_Position = 0

    def reset(self, frame=0, render=True, logging=False):
        self.DI_RampFull = False
        self.stored_wp_list = []
        self.AI_Position = 0

    def update(self, WPregister, frame=0, render=True, logging=False):

        self.DI_RampFull = False
        for wp in WPregister:
            if xppumathlib.wp_in_btw(self.pos_provided, self.storage_pos_list[0], wp):
                if not wp in self.stored_wp_list:
                    free_spot_idx = len(self.stored_wp_list)
                    free_spot = self.storage_pos_list[free_spot_idx] if free_spot_idx <=3 else self.storage_pos_list[-1]
                    if round(wp.pos.z + self.step.z,4) > round(free_spot.z,4):
                        wp.pos += self.step
                    else:
                        wp.pos = free_spot
                        self.stored_wp_list.append(wp)
                        wp.stored=True
                        wp.RFID_data["Storage position"] = self.name
                        if logging is True: print("Stored", wp.name, "Ramp", self.name,"Ramp Position:", len(self.stored_wp_list), "Stamped:", wp.RFID_data["Stamped"])

                if render is True:
                    obj = bpy.data.objects[wp.name]
                    obj.location = wp.pos.copy()
                    obj.keyframe_insert(data_path="location", frame=frame)

                    if obj.rotation_euler != self.ramp_angle:
                        # To assure that rotation starts in the current keyframe
                        obj.keyframe_insert(data_path="rotation_euler", frame=frame-1)
                        obj.rotation_euler = self.ramp_angle.copy()
                        obj.keyframe_insert(data_path="rotation_euler", frame=frame)

                if xppumathlib.wp_at_pos(self.pos_rampfull, wp):
                    self.DI_RampFull = True

                self.AI_Position = xppumathlib.vec_norm(wp.pos - self.pos_provided)