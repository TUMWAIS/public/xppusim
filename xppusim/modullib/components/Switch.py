import math
import xppumathlib
import bpy

from xppusim.modullib.Module import Module


class Switch(Module):
    def __init__(self, name, frame=0, render=True):
        super().__init__()
        self.name = name
        self.angle_init = bpy.data.objects[name+".arm"].rotation_euler.z
        self.angle_retracted = bpy.data.objects[name + ".Retracted"].rotation_euler.z
        self.angle_extended = bpy.data.objects[name + ".Extended"].rotation_euler.z
        self.angle = self.angle_init
        self.step_size = (self.angle_extended - self.angle_retracted) / 5 # Number of steps for extraction

        self.DO_Extend = True if abs(self.angle-self.angle_extended) <= self.e_precision else False
        self.DI_Extended = True if abs(self.angle-self.angle_extended) <= self.e_precision else False
        self.DI_Retracted = True if abs(self.angle-self.angle_retracted) <= self.e_precision else False

    def reset(self, init_extended, frame=0, render=True):
        self.angle = self.angle_init

        self.DO_Extend = True if abs(self.angle-self.angle_extended) <= self.e_precision else False
        self.DI_Extended = True if abs(self.angle-self.angle_extended) <= self.e_precision else False
        self.DI_Retracted = True if abs(self.angle-self.angle_retracted) <= self.e_precision else False

        if render is True: # Add initial keyframe
            obj = bpy.data.objects[self.name]
            obj.rotation_euler.z = math.radians(self.angle)
            obj.keyframe_insert(data_path="rotation_euler", frame=frame)

    def update(self, WPregister, frame=0, render=True):
        if self.DO_Extend is True and abs(self.angle-self.angle_extended) > self.e_precision:
            self.angle += self.step_size
        elif self.DO_Extend is False and abs(self.angle-self.angle_retracted) > self.e_precision:
            self.angle -= self.step_size


        self.DI_Extended = True if abs(self.angle-self.angle_extended) <= self.e_precision else False
        self.DI_Retracted = True if abs(self.angle-self.angle_retracted) <= self.e_precision else False

        if render is True: # Add initial keyframe
            obj = bpy.data.objects[self.name + ".arm"]
            obj.rotation_euler.z = self.angle
            obj.keyframe_insert(data_path="rotation_euler", frame=frame)