from xppusim.modullib.components.Cylinder import Cylinder
import xppumathlib


class BistableCylinder(Cylinder):
    def __init__(self, name, frame=0, render=True):
        super().__init__(name, frame, render)
        self.DO_Extend = True if self.pos.to_tuple(4) == self.pos_extended.to_tuple(4) else False
        self.DO_Retract = True if self.pos.to_tuple(4) == self.pos_retracted.to_tuple(4) else False

    def reset(self, frame=0, render=True):
        Cylinder.reset(self, frame, render)
        self.DO_Extend = True if self.pos.to_tuple(4) == self.pos_retracted.to_tuple(4) else False
        self.DO_Retract = True if self.pos.to_tuple(4) == self.pos_retracted.to_tuple(4) else False

    def update(self, WPregister, frame=0, render=True, ext_blocked=False, ret_blocked=False):
        if self.DO_Extend is True and self.DO_Retract is False and self.pos.to_tuple(4) != self.pos_extended.to_tuple(4) and ext_blocked is False:
            self.pos += self.step_size
        elif self.DO_Extend is False and self.DO_Retract is True and self.pos.to_tuple(4) != self.pos_retracted.to_tuple(4) and ret_blocked is False:
            self.pos -= self.step_size

        Cylinder.update(self, WPregister, frame, render, ext_blocked, ret_blocked) # Needs to be called last