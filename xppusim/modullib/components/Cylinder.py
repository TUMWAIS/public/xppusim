import bpy
import xppumathlib

from xppusim.modullib.Module import Module


class Cylinder(Module):
    """
        Basic cylinder class with sensors, but without actuation.
    """
    def __init__(self, name, frame=0, render=True):
        super().__init__()
        self.name = name
        self.pos_init = bpy.data.objects[name].matrix_world.to_translation().copy()
        self.pos_retracted = bpy.data.objects[name + ".Retracted"].matrix_world.to_translation().copy()
        self.pos_extended = bpy.data.objects[name + ".Extended"].matrix_world.to_translation().copy()

        self.pos = self.pos_init.copy()
        self.step_size = (self.pos_extended-self.pos_retracted)/10 # Steps for extension/retraction

        self.DI_Retracted = True if self.pos.to_tuple(4) == self.pos_retracted.to_tuple(4) else False
        self.DI_Extended = True if self.pos.to_tuple(4) == self.pos_extended.to_tuple(4) else False
        self.AI_Position = 20/xppumathlib.vec_norm(self.pos_extended-self.pos_retracted)* \
                           xppumathlib.vec_norm(self.pos-self.pos_retracted) # Analog sensor value btw. 0-20V

        if render is True: # Add initial keyframe
            obj = bpy.data.objects[self.name]
            obj.keyframe_insert(data_path="location", frame=frame)


    def reset(self, frame=0, render=True):
        self.pos = self.pos_init.copy()
        self.DO_Extend = True if self.pos.to_tuple(4) == self.pos_extended.to_tuple(4) else False
        self.DI_Retracted = True if self.pos.to_tuple(4) == self.pos_retracted.to_tuple(4) else False
        self.DI_Extended = True if self.pos.to_tuple(4) == self.pos_extended.to_tuple(4) else False
        self.AI_Position = 20 / xppumathlib.vec_norm(self.pos_extended - self.pos_retracted) * xppumathlib.vec_norm(self.pos - self.pos_retracted)

        if render is True:
            obj = bpy.data.objects[self.name]
            obj.location = self.pos.copy()
            obj.keyframe_insert(data_path="location", frame=frame)


    def update(self, WPregister, frame=0, render=True, ext_blocked=False, ret_blocked=False):
        self.DI_Retracted = True if self.pos.to_tuple(4) == self.pos_retracted.to_tuple(4) else False
        self.DI_Extended = True if self.pos.to_tuple(4) == self.pos_extended.to_tuple(4) else False
        self.AI_Position = 20/xppumathlib.vec_norm(self.pos_extended-self.pos_retracted)*xppumathlib.vec_norm(self.pos-self.pos_retracted)

        if render is True:
            obj = bpy.data.objects[self.name]
            obj.location = self.pos.copy()
            obj.keyframe_insert(data_path="location", frame=frame)