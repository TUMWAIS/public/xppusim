import bpy
import xppumathlib

from xppusim.modullib.Module import Module


class Carrier(Module):
    def __init__(self, name, frame=0, render=True):
        super().__init__()
        self.name = name
        self.pos_endleft = bpy.data.objects[name + ".posendleft"].matrix_world.to_translation().copy()
        self.pos_endright = bpy.data.objects[name + ".posendright"].matrix_world.to_translation().copy()
        self.pos_pickup1 = bpy.data.objects[name + ".pospickup1"].matrix_world.to_translation().copy()
        self.pos_pickup2 = bpy.data.objects[name + ".pospickup2"].matrix_world.to_translation().copy()
        self.pos_putdown1 = bpy.data.objects[name + ".posputdown1"].matrix_world.to_translation().copy()
        self.pos_putdown2 = bpy.data.objects[name + ".posputdown2"].matrix_world.to_translation().copy()
        self.pos_init = bpy.data.objects[name].matrix_world.to_translation().copy()
        self.pos = self.pos_init.copy()
        self.step_size = (self.pos_endright-self.pos_endleft) / xppumathlib.vec_norm(self.pos_endright-self.pos_endleft) * 2.5 # Movement per step

        self.DO_TurnClockwise = False
        self.DO_TurnCounterClockwise = False
        self.DI_AtPickup1 = True if self.pos_in_range(self.pos, self.pos_pickup1, self.step_size) is True else False
        self.DI_AtPickup2 = True if self.pos_in_range(self.pos, self.pos_pickup2, self.step_size) is True else False
        self.DI_AtPutDown1 = True if self.pos_in_range(self.pos, self.pos_putdown1, self.step_size) is True else False
        self.DI_AtPutDown2 = True if self.pos_in_range(self.pos, self.pos_putdown2, self.step_size) is True else False
        self.DI_AtEndLeft = True if self.pos_in_range(self.pos, self.pos_endleft, self.step_size) is True else False
        self.DI_AtEndRight = True if self.pos_in_range(self.pos, self.pos_endright, self.step_size) is True else False

    def reset(self, frame=0, render=True):
        self.pos = self.pos_init.copy()
        self.DO_TurnClockwise = False
        self.DO_TurnCounterClockwise = False
        self.DI_AtPickup1 = True if self.pos_in_range(self.pos, self.pos_pickup1, self.step_size) is True else False
        self.DI_AtPickup2 = True if self.pos_in_range(self.pos, self.pos_pickup2, self.step_size) is True else False
        self.DI_AtPutDown1 = True if self.pos_in_range(self.pos, self.pos_putdown1, self.step_size) is True else False
        self.DI_AtPutDown2 = True if self.pos_in_range(self.pos, self.pos_putdown2, self.step_size) is True else False
        self.DI_AtEndLeft = True if self.pos_in_range(self.pos, self.pos_endleft, self.step_size) is True else False
        self.DI_AtEndRight = True if self.pos_in_range(self.pos, self.pos_endright, self.step_size) is True else False

    def update(self, WPregister, frame=0, render=True):
        if self.DO_TurnClockwise is True and self.DO_TurnCounterClockwise is False:
            if xppumathlib.median(self.pos_endright.x, self.pos_endleft.x, self.pos.x+self.step_size.x):
                self.pos.x += self.step_size.x

            if xppumathlib.median(self.pos_endright.y, self.pos_endleft.y, self.pos.y+self.step_size.y):
                self.pos.y += self.step_size.y

        elif self.DO_TurnClockwise is False and self.DO_TurnCounterClockwise is True:
            if xppumathlib.median(self.pos_endright.x, self.pos_endleft.x, self.pos.x-self.step_size.x):
                self.pos.x -= self.step_size.x

            if xppumathlib.median(self.pos_endright.y, self.pos_endleft.y, self.pos.y-self.step_size.y):
                self.pos.y -= self.step_size.y

        if render is True:
            obj = bpy.data.objects[self.name]
            obj.location = self.pos.copy()
            obj.keyframe_insert(data_path="location", frame=frame)

        self.DI_AtPickup1 = True if self.pos_in_range(self.pos, self.pos_pickup1, self.step_size) is True else False
        self.DI_AtPickup2 = True if self.pos_in_range(self.pos, self.pos_pickup2, self.step_size) is True else False
        self.DI_AtPutDown1 = True if self.pos_in_range(self.pos, self.pos_putdown1, self.step_size) is True else False
        self.DI_AtPutDown2 = True if self.pos_in_range(self.pos, self.pos_putdown2, self.step_size) is True else False
        self.DI_AtEndLeft = True if self.pos_in_range(self.pos, self.pos_endleft, self.step_size) is True else False
        self.DI_AtEndRight = True if self.pos_in_range(self.pos, self.pos_endright, self.step_size) is True else False