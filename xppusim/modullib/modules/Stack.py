import bpy
from mathutils import Vector
import xppumathlib

from xppusim.modullib.Module import Module
from xppusim.modullib.components.MonostableCylinder import MonostableCylinder


class Stack(Module):
    def __init__(self, name, Modul_Register, frame=0, render=True, logging=False):
        super().__init__()
        Modul_Register.append(self)
        self.name = name
        self.pos_magazine = bpy.data.objects[name + ".separator.Retracted"].matrix_world.to_translation().copy()
        self.pos_provided = bpy.data.objects[name + ".separator.Extended"].matrix_world.to_translation().copy()

        self.blockage_weight = 1000
        self.sep_blocked = False

        self.separator = MonostableCylinder(name + ".separator")

        self.DI_WPProvided = False
        self.AI_WPWeight = False
        self.DI_WPMetallic = False
        self.DI_WPLight = False
        self.VI_Position = 0

    # Move registered workpices into magazine
    def initialize_magazine(self, wp_register, frame=0, render=True, logging=False):
        wp_count = 0
        for wp in wp_register:
            wp.pos = self.pos_magazine + wp_count * Vector([0, 0, wp.dim.z+0.1]) #Assuming same dimensions for all workpieces
            wp_count += 1
            if render is True:  # Add initial keyframe
                obj = bpy.data.objects[wp.name]
                obj.location = wp.pos.copy()
                obj.keyframe_insert(data_path="location", frame=frame)
                obj.keyframe_insert(data_path="rotation_euler", frame=frame)


    def reset(self, frame=0, render=True, logging=False):
        self.separator.reset(frame, render)
        self.sep_blocked = False

        self.DI_WPProvided = False
        self.AI_WPWeight = False
        self.DI_WPMetallic = False
        self.DI_WPLight = False
        self.VI_Position = 0

    def update(self, WPregister, frame=0, render=True, logging=False):
        moved_wps = []

        # Check if lowest workpiece in stack has been removed and move other workpieces down
        wp_at_magazine = False
        for wp in WPregister:
            if xppumathlib.wp_at_pos(self.pos_magazine, wp, dim=wp.dim):
                wp_at_magazine = True

        if wp_at_magazine == False and self.separator.DI_Retracted == True:
            for wp in WPregister:
                if abs(wp.pos.x-self.pos_magazine.x) <= self.e_precision and abs(wp.pos.y - self.pos_magazine.y) <= self.e_precision:
                    wp.pos.z -= (wp.dim.z + 0.1)
                    if not wp in moved_wps: moved_wps.append(wp)

        # Update separator
        self.separator.update(WPregister, frame, render, self.sep_blocked)

        # Move workpieces with separator
        wp_at_sep = None
        self.sep_blocked = False
        self.wp_moved = Vector([0, 0, 0])

        # Move workpiece if separator is extended
        for wp in WPregister:
            if xppumathlib.wp_in_btw(self.pos_magazine, self.pos_provided, wp): # Check if workpiece is in seperation area of stack
                # Check if separator moved past the first workpiece and move it
                if (self.pos_magazine.x < self.pos_provided.x and wp.pos.x < self.separator.pos.x) or \
                (self.pos_magazine.x > self.pos_provided.x and wp.pos.x > self.separator.pos.x) or \
                (self.pos_magazine.y < self.pos_provided.y and wp.pos.y < self.separator.pos.y) or \
                (self.pos_magazine.y > self.pos_provided.y and wp.pos.y > self.separator.pos.y):
                    wp_at_sep = wp
                    self.wp_moved = self.separator.pos - wp.pos
                    wp.pos = self.separator.pos.copy()
                    if not wp in moved_wps: moved_wps.append(wp)

        # Move other workpieces in separation area in case of any overlaps (if final position not reached)
        for wp in WPregister:
            if xppumathlib.wp_in_btw(self.pos_magazine, self.pos_provided, wp):
                if self.overlapp(wp, WPregister) and wp != wp_at_sep:
                    if wp.pos.to_tuple(4) != self.pos_provided.to_tuple(4): # Only move if final position not reached
                        wp.pos = wp.pos + self.wp_moved
                        if not wp in moved_wps: moved_wps.append(wp)

        # If there is any overlapping after all workpieces have been moved, stop updating separator
        for wp in WPregister:
            if xppumathlib.wp_in_btw(self.pos_magazine, self.pos_provided, wp):
                if self.overlapp(wp, WPregister):
                    self.sep_blocked = True
                    if render is True: # Add keyframe at blocked position
                        obj = bpy.data.objects[self.separator.name]
                        obj.keyframe_insert(data_path="location", frame=frame)

        # Resolve existing overlapp when separator is retracted again
        for wp in WPregister:
            if xppumathlib.wp_in_btw(self.pos_magazine, self.pos_provided, wp):
               if (self.separator.pos + self.separator.step_size).to_tuple(4) == wp.pos.to_tuple(4):
                   if self.overlapp(wp, WPregister):
                       wp.pos = self.separator.pos.copy()

        # Update sensor values
        self.DI_WPProvided = False
        self.AI_WPWeight = False
        self.DI_WPMetallic = False
        self.DI_WPLight = False
        for wp in WPregister:
            if wp.pos.to_tuple(4) == self.pos_provided.to_tuple(4):
                self.DI_WPProvided = True
                self.AI_WPWeight = wp.weight
                if self.sep_blocked == True: self.AI_WPWeight = self.blockage_weight
                if wp.color == "metallic" or wp.color == "white": self.DI_WPLight = True
                if wp.color == "metallic": self.DI_WPMetallic = True
                break

            # Check if workpiece is in seperation area of stack
            if xppumathlib.wp_in_btw(self.pos_magazine, self.pos_provided, wp):
                self.VI_Position = xppumathlib.vec_norm(wp.pos - self.pos_magazine)


        # Crate keyfranes for moves workpieces
        if render is True:
            for wp in moved_wps:
                obj = bpy.data.objects[wp.name]
                obj.location = wp.pos.copy()
                obj.keyframe_insert(data_path="location", frame=frame)