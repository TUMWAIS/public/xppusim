import bpy
import xppumathlib

from xppusim.modullib.Error import Error
from xppusim.modullib.components.BistableCylinder import BistableCylinder
from xppusim.modullib.Module import Module
from xppusim.modullib.components.MonostableCylinder import MonostableCylinder


class Stamp(Module):
    def __init__(self, name, Modul_Register, wp_setup, frame=0, render = True, logging=False):
        super().__init__()
        Modul_Register.append(self)
        self.name = name
        self.wp_setup = wp_setup
        self.pos_stamping = bpy.data.objects[name + ".slidingCylinder.Retracted"].matrix_world.to_translation().copy()
        self.pos_provided = bpy.data.objects[name + ".slidingCylinder.Extended"].matrix_world.to_translation().copy()

        self.slidingCylinder = BistableCylinder(name + ".slidingCylinder")
        self.stampingCylinder = MonostableCylinder(name + ".stampingCylinder")

        self.DI_WPAvailable = False

        if render is True:  # Add initial keyframe
            obj = bpy.data.objects[self.name]
            obj.keyframe_insert(data_path="location", frame=frame)

    def reset(self, wp_setup, frame=0, render=True, logging=False):
        self.slidingCylinder.reset(frame, render)
        self.stampingCylinder.reset(frame, render)
        self.wp_setup = wp_setup
        self.DI_WPAvailable = False

    def update(self, WPregister, frame=0, render=True, logging=False):

        # Update sensor values and check correct positioning of workpiece (before updating slidingCylinder)
        self.DI_WPAvailable = False
        for wp in WPregister:
            if xppumathlib.wp_in_btw(self.pos_provided, self.pos_stamping, wp):
                if wp.pos.to_tuple(4) == self.slidingCylinder.pos.to_tuple(4):
                    self.DI_WPAvailable = True
                else:
                    raise Error("Workpice not placed into slider of stamp")

        # Update sliding and stamping cylinder
        self.slidingCylinder.update(WPregister, frame, render)
        self.stampingCylinder.update(WPregister, frame, render)
        
        if (self.stampingCylinder.DI_Retracted is False) and ((self.slidingCylinder.DI_Extended is False and self.slidingCylinder.DI_Retracted is False) or (self.slidingCylinder.DI_Extended is True and self.slidingCylinder.DO_Retract is True and self.slidingCylinder.DO_Extend is False) or (self.slidingCylinder.DI_Retracted is True and self.slidingCylinder.DO_Extend is True and self.slidingCylinder.DO_Retract is False )):
            raise Error("Moved SlidingCylinder while Stampingcylinder was extended!")
        
        # Move workpiece if slidingCylinder has been moved
        for wp in WPregister:
            if xppumathlib.wp_in_btw(self.pos_provided, self.pos_stamping, wp):
                if wp.attached is True and wp.pos.to_tuple(4) != self.slidingCylinder.pos.to_tuple(4):
                    raise Error("Workpiece moved by Stamp while attached to the crane")
                wp.pos = self.slidingCylinder.pos.copy()

                if render is True:
                    obj = bpy.data.objects[wp.name]
                    obj.location = wp.pos.copy()
                    obj.keyframe_insert(data_path="location", frame=frame)
                    break


        # Update workpiece RFID data
        if self.stampingCylinder.DI_Extended is True and self.slidingCylinder.DI_Retracted:
            for wp in WPregister:
                if wp.pos.to_tuple(4) == self.slidingCylinder.pos.to_tuple(4):
                    wp.RFID_data["Stamped"] = True
                    self.wp_setup = wp.color