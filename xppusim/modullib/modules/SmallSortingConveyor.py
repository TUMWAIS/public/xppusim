import bpy
from mathutils import Vector
import xppumathlib

from xppusim.modullib.components.Ramp import Ramp
from xppusim.modullib.components.Switch import Switch
from xppusim.modullib.modules.Conveyor import Conveyor


class SmallSortingConveyor(Conveyor):
    def __init__(self, name, num_wps, Modul_Register, frame=0, render=True, logging=False):
        super().__init__(name, Modul_Register, frame, render)
        self.switch_step = Vector([2, 0, 0])

        self.pos_WPatSwitch = bpy.data.objects[name + ".switch.WPatSwitch"].matrix_world.to_translation().copy()
        self.pos_WPPosition1 = bpy.data.objects[name + ".presencesensor1.sensorpos"].matrix_world.to_translation().copy()
        self.pos_WPPosition2 = bpy.data.objects[name + ".presencesensor2.sensorpos"].matrix_world.to_translation().copy()
        self.pos_WPPosition3 = bpy.data.objects[name + ".presencesensor3.sensorpos"].matrix_world.to_translation().copy()
        self.pos_WPLight1 = bpy.data.objects[name + ".opticalsensor1.sensorpos"].matrix_world.to_translation().copy()
        self.pos_WPMetallic1 = bpy.data.objects[name + ".inductivesensor1.sensorpos"].matrix_world.to_translation().copy()
        self.pos_RFCHandover = bpy.data.objects["refillconveyor.posstart"].matrix_world.to_translation().copy()
        self.pos_PACHandover = bpy.data.objects["picalphaconveyor.posend"].matrix_world.to_translation().copy()

        self.handover_steps = 20 # Number of steps for handover to/from other conveyor

        self.ramp1 = Ramp(name + ".ramp1")
        self.switch = Switch(name + ".switch")
        self.DI_WPPosition1 = False
        self.DI_WPPosition2 = False
        self.DI_WPPosition3 = False
        self.DI_WPLight1 = False
        self.DI_WPMetallic1 = False
        self.VI_WPPosition = [-1] * num_wps

    def reset(self, frame, render=True, logging=False):
        self.switch.reset(frame, render)
        self.ramp1.reset()
        self.DI_WPPosition1 = False
        self.DI_WPPosition2 = False
        self.DI_WPPosition3 = False
        self.DI_WPLight1 = False
        self.DI_WPMetallic1 = False

    def update(self, WPregister, frame=0, render=True, logging=False):
        self.ramp1.update(WPregister, frame, render, logging)
        self.switch.update(WPregister, frame, render)

        # Move workpieces at RefillConveyor handover
        if self.switch.DO_Extend is True and self.DO_TurnClockwise is True:
            self.handover_WP(self.pos_WPatSwitch, self.pos_RFCHandover, self.handover_steps, WPregister, frame, render)

        # Move workpieces at PicAlphaConveyor handover
        if self.DO_TurnCounterClockwise is True:
            self.handover_WP(self.pos_start, self.pos_PACHandover, self.handover_steps, WPregister, frame, render)

        # Move remaining workpieces on conveyor
        for wp in WPregister:
            if self.switch.DI_Extended is True and self.DO_TurnClockwise is True and self.pos_in_range(self.pos_WPatSwitch, wp.pos, self.step_size) is True:
                wp.pos = self.pos_WPatSwitch.copy() # Blocked by switch
            elif self.switch.DI_Extended is True and self.DO_TurnCounterClockwise is True and xppumathlib.wp_at_pos(self.pos_WPatSwitch, wp, step=-self.step_size) is True :
                self.WP_blocked(wp, frame, render) # Blocked by switch. Workpiece overlapps quite a bit, but that's ok. Solution would be to add another coordinate behind switch
            else:
                self.move_wp(wp, WPregister, frame, render)

        # Update sensor values
        # Check which workpiece is on the conveyor. Due to performance reasons not integrated into sensor functions
        wp_list = []
        for wp in WPregister:
            if xppumathlib.wp_in_btw(self.pos_start, self.pos_end, wp) is True:
                wp_list.append(wp)

        self.DI_WPPosition1 = self.upd_pressensor(self.pos_WPPosition1, wp_list)
        self.DI_WPPosition2 = self.upd_pressensor(self.pos_WPPosition2, wp_list)
        self.DI_WPPosition3 = self.upd_pressensor(self.pos_WPPosition3, wp_list)
        self.DI_WPMetallic1 = self.upd_inductive(self.pos_WPMetallic1, wp_list)
        self.DI_WPLight1 = self.upd_optical(self.pos_WPLight1, wp_list)
        self.VI_WPPosition = self.upd_virtualposition(self.pos_start, wp_list, WPregister)