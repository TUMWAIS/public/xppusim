import bpy
from mathutils import Vector
import xppumathlib

from xppusim.modullib.Error import Error
from xppusim.modullib.components.MonostableCylinder import MonostableCylinder
from xppusim.modullib.components.Ramp import Ramp
from xppusim.modullib.components.Switch import Switch
from xppusim.modullib.modules.Conveyor import Conveyor


class LargeSortingConveyor(Conveyor):
    def __init__(self, name, num_wps, Modul_Register, frame=0, render=True, logging=False):
        super().__init__(name, Modul_Register, frame, render)
        self.switch_step = Vector([-2, 0, 0])

        self.pos_WPatSep1 = bpy.data.objects[name + ".separator1.WPatSep"].matrix_world.to_translation().copy()
        self.pos_WPatSep2 = bpy.data.objects[name + ".separator2.WPatSep"].matrix_world.to_translation().copy()
        self.pos_WPPosition0 = bpy.data.objects[name + ".presencesensor0.sensorpos"].matrix_world.to_translation().copy()
        self.pos_WPPosition1 = bpy.data.objects[name + ".presencesensor1.sensorpos"].matrix_world.to_translation().copy()
        self.pos_WPMetallic1 = bpy.data.objects[name + ".inductivesensor1.sensorpos"].matrix_world.to_translation().copy()
        self.pos_WPLight1 = bpy.data.objects[name + ".opticalsensor1.sensorpos"].matrix_world.to_translation().copy()
        self.pos_WPPosition2 = bpy.data.objects[name + ".presencesensor2.sensorpos"].matrix_world.to_translation().copy()
        self.pos_WPMetallic2 = bpy.data.objects[name + ".inductivesensor2.sensorpos"].matrix_world.to_translation().copy()
        self.pos_WPLight2 = bpy.data.objects[name + ".opticalsensor2.sensorpos"].matrix_world.to_translation().copy()
        self.pos_WPatSwitch = bpy.data.objects[name + ".switch.WPatSwitch"].matrix_world.to_translation().copy()
        self.pos_PACHandover = bpy.data.objects["picalphaconveyor.posstart"].matrix_world.to_translation().copy()

        self.handover_steps = 20 # Number of steps for handover to/from other conveyor

        self.sep1_blocked = False
        self.sep2_blocked = False
        self.separator1 = MonostableCylinder(name + ".separator1.pusher")
        self.separator2 = MonostableCylinder(name + ".separator2.pusher")
        self.ramp1 = Ramp(name + ".ramp1")
        self.ramp2 = Ramp(name + ".ramp2")
        self.ramp3 = Ramp(name + ".ramp3")
        self.switch = Switch(name + ".switch")

        self.DI_WPPosition0 = False
        self.DI_WPPosition1 = False
        self.DI_WPMetallic1 = False
        self.DI_WPLight1 = False
        self.DI_WPPosition2 = False
        self.DI_WPMetallic2 = False
        self.DI_WPLight2 = False
        self.VI_WPPosition = [-1] * num_wps

    def reset(self, frame, render=True, logging=False):
        super().reset(frame, render)
        self.ramp1.reset()
        self.ramp2.reset()
        self.ramp3.reset()

        self.separator1.reset(frame, render)
        self.separator2.reset(frame, render)
        self.switch.reset(frame, render)
        self.DI_WPPosition0 = False
        self.DI_WPPosition1 = False
        self.DI_WPMetallic1 = False
        self.DI_WPLight1 = False
        self.DI_WPPosition2 = False
        self.DI_WPMetallic2 = False
        self.DI_WPLight2 = False

    def update(self, WPregister, frame=0, render=True, logging=False):
        self.ramp1.update(WPregister, frame, render, logging)
        self.ramp2.update(WPregister, frame, render, logging)
        self.ramp3.update(WPregister, frame, render, logging)
        self.separator1.update(WPregister, frame, render, self.sep1_blocked)
        self.separator2.update(WPregister, frame, render, self.sep1_blocked)
        self.switch.update(WPregister, frame, render)

        # Throw error if workpiece falls down conveyor
        for wp in WPregister:
            if wp.pos.to_tuple(4) == self.pos_start.to_tuple(4):
                raise Error("Workpiece falls from conveyor")

        # Move workpiece at separator
        self.separate_WP(self.pos_WPatSep1, self.separator1, WPregister, frame, render)
        self.separate_WP(self.pos_WPatSep2, self.separator2, WPregister, frame, render)

        # Move workpieces at switch
        if self.switch.DO_Extend is True and self.DO_TurnClockwise is True:
            self.handover_WP(self.pos_WPatSwitch, self.pos_PACHandover, self.handover_steps, WPregister, frame, render)

        # Move remaining workpieces on conveyor
        for wp in WPregister:
            if self.separator1.DI_Retracted == False and self.DO_TurnClockwise is True and xppumathlib.wp_at_pos(self.pos_WPatSep1, wp, step=self.step_size) is True:
                self.WP_blocked(wp, frame, render) # Blocked by separator 1
            elif self.separator1.DI_Retracted == False and self.DO_TurnCounterClockwise is True and xppumathlib.wp_at_pos(self.pos_WPatSep1, wp, step=-self.step_size) is True:
                self.WP_blocked(wp, frame, render) # Blocked by separator 1
            elif self.separator2.DI_Retracted == False and self.DO_TurnClockwise is True and xppumathlib.wp_at_pos(self.pos_WPatSep2, wp, step=self.step_size) is True:
                self.WP_blocked(wp, frame, render) # Blocked by separator 2
            elif self.separator2.DI_Retracted == False and self.DO_TurnCounterClockwise is True and xppumathlib.wp_at_pos(self.pos_WPatSep2, wp, step=-self.step_size) is True :
                self.WP_blocked(wp, frame, render) # Blocked by separator 2
            elif self.switch.DI_Extended is True and self.DO_TurnClockwise is True and self.pos_in_range(self.pos_WPatSwitch, wp.pos, self.step_size) is True :
                wp.pos = self.pos_WPatSwitch.copy()
            elif self.switch.DI_Extended is True and self.DO_TurnCounterClockwise is True and xppumathlib.wp_at_pos(self.pos_WPatSwitch, wp, step=-self.step_size) is True:
                self.WP_blocked(wp, frame, render) # Blocked by switch. Workpiece overlapps quite a bit, but that's ok. Solution would be to add another coordinate behind switch
            elif wp.attached is True and (self.DO_TurnClockwise is True or self.DO_TurnCounterClockwise is True) and xppumathlib.wp_in_btw(self.pos_start, self.pos_end, wp):
                raise Error("Workpice moved by LargeSortingConveyor while attached to the crane")  # Throw error if workpiece is moved while still attached to the crane
            else:
                self.move_wp(wp, WPregister, frame, render)

        # Update sensor values
        # Check which workpiece is on the conveyor. Due to performance reasons not integrated into sensor functions
        wp_list = []
        for wp in WPregister:
            if xppumathlib.wp_in_btw(self.pos_start, self.pos_end, wp) is True:
                wp_list.append(wp)
        self.DI_WPPosition0 = self.upd_pressensor(self.pos_WPPosition0, wp_list)
        self.DI_WPPosition1 = self.upd_pressensor(self.pos_WPPosition1, wp_list)
        self.DI_WPPosition2 = self.upd_pressensor(self.pos_WPPosition2, wp_list)
        self.DI_WPMetallic1 = self.upd_inductive(self.pos_WPMetallic1, wp_list)
        self.DI_WPMetallic2 = self.upd_inductive(self.pos_WPMetallic2, wp_list)
        self.DI_WPLight1 = self.upd_optical(self.pos_WPLight1, wp_list)
        self.DI_WPLight2 = self.upd_optical(self.pos_WPLight2, wp_list)

        wp_list = []
        for wp in WPregister:
            if (xppumathlib.wp_in_btw(self.pos_start, self.pos_end, wp) is True or
                xppumathlib.wp_at_pos(self.pos_WPatSep1, wp, frac=Vector([1.5, 1.5, 0])) or
                xppumathlib.wp_at_pos(self.pos_WPatSep2, wp, frac=Vector([1.5, 1.5, 0]))):
                wp_list.append(wp)
        self.VI_WPPosition = self.upd_virtualposition(self.pos_start, wp_list, WPregister)