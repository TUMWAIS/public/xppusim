import bpy
from mathutils import Vector
import xppumathlib

from xppusim.modullib.Error import Error
from xppusim.modullib.components.Carrier import Carrier
from xppusim.modullib.Module import Module
from xppusim.modullib.components.MonostableCylinder import MonostableCylinder


class PicAlpha(Module):
    def __init__(self, name, Modul_Register, frame=0, render=True, logging=False):
        super().__init__()
        Modul_Register.append(self)
        self.name = name

        self.carrier = Carrier(name + ".carrier", frame, render)
        self.liftingCylinder = MonostableCylinder(name + ".carrier.liftingcylinder")
        self.wp_attached = None
        self.DO_Intake = False
        self.DI_TakenIn = False

    def reset(self, frame=0, render=True, logging=False):
        self.carrier.reset(frame, render)
        self.liftingCylinder.reset(frame, render)
        self.wp_attached = None
        self.DO_Intake = False
        self.DI_TakenIn = False

    def update(self, WPregister, frame=0, render=True, logging=False):
        # Check if workpiece below gripper
        wp_at_gripper = False
        pos_gripper = Vector([self.carrier.pos.x, self.carrier.pos.y, self.liftingCylinder.pos.z]) # x/y-movement from carrier, z-movement from liftingCylinder
        for wp in WPregister:
            if self.DO_Intake == True and xppumathlib.wp_at_pos(pos_gripper, wp) is True:
                wp_at_gripper = True
                self.wp_attached = wp
            elif xppumathlib.wp_at_pos(pos_gripper, wp) and self.DO_Intake == False:
                if self.liftingCylinder.DI_Extended is True:
                    self.wp_attached = None
                else:
                    raise Error("Workpiece detached in mid-air")

        # Raise error if gripping activated, but workpiece not below crane
        if wp_at_gripper == False and self.wp_attached != None:
            raise Error("Workpiece moved while attached to Crane")

        self.carrier.update(WPregister, frame, render)
        self.liftingCylinder.update(WPregister, frame, render)

        # Move workpiece with gripper
        if self.wp_attached is not None:
            pos_gripper_new = Vector([self.carrier.pos.x, self.carrier.pos.y, self.liftingCylinder.pos.z])
            self.wp_attached.pos += pos_gripper_new - pos_gripper

            if render is True:
                obj = bpy.data.objects[self.wp_attached.name]
                obj.location = self.wp_attached.pos.copy()
                obj.keyframe_insert(data_path="location", frame=frame)

            # If attached workpiece overlapps with another workpiece stop updating the lifitingCylinder
            if self.overlapp(self.wp_attached, WPregister):
                raise Error("Collision with other workpiece on PicAlphaConveyor detected")

        #  Update sensor signals
        self.DI_TakenIn = True if self.wp_attached is not None else False