import bpy
from mathutils import Vector
from math import sqrt
import xppumathlib

from xppusim.modullib.Module import Module


class Conveyor(Module):
    """Class containing basic conveyor functionalities"""
    def __init__(self, name, Modul_Register, frame=0, render=True, logging=False):
        super().__init__()
        Modul_Register.append(self)
        self.name = name

        self.pos_end = bpy.data.objects[name+".posend"].matrix_world.to_translation().copy()
        self.pos_start = bpy.data.objects[name+".posstart"].matrix_world.to_translation().copy()
        self.step_size = (self.pos_end - self.pos_start) / xppumathlib.vec_norm(self.pos_end - self.pos_start) * 1 #0.47 # Distance per step

        self.DO_TurnClockwise = False
        self.DO_TurnCounterClockwise = False

    def reset(self, frame, render=True, logging=False):
        self.DO_TurnClockwise = False
        self.DO_TurnCounterClockwise = False

    def update(self, WPregister, frame=0, render=True, logging=False): # Will be overwritten by child classes
        Conveyor.move_wp(self, wp, WPregister, frame, render)


    # Move workpiece on conveyor belt
    def move_wp(self, wp, WPregister, frame, render):
        if xppumathlib.wp_in_btw(self.pos_start, self.pos_end, wp):
            if self.DO_TurnClockwise is True and self.DO_TurnCounterClockwise is False:
                if self.overlapp(wp, WPregister, self.step_size):
                    pass  # Blocked by other workpiece in clockwise direction
                else:
                    wp.pos += self.step_size
                    if not xppumathlib.wp_in_btw(self.pos_start, self.pos_end, wp): wp.pos = self.pos_end.copy()

            elif self.DO_TurnCounterClockwise is True and self.DO_TurnClockwise is False:
                if self.overlapp(wp, WPregister, -self.step_size):
                    pass  # Blocked by other workpiece in counterclockwise direction
                else:
                    wp.pos -= self.step_size
                    if not xppumathlib.wp_in_btw(self.pos_start, self.pos_end, wp): wp.pos = self.pos_start.copy()

        if render is True: # Add frame even if workpiece is not moved
            obj = bpy.data.objects[wp.name]
            obj.location = wp.pos.copy()
            obj.keyframe_insert(data_path="location", frame=frame)

    # Move workpiece perpendicular to conveying direction at switch
    def handover_WP(self, pos1, pos2, num_steps, WPregister, frame, render):
        for wp in WPregister:
            if xppumathlib.wp_in_btw(pos1, pos2, wp):
                wp.pos += (pos2 - pos1) / num_steps
                if xppumathlib.wp_in_btw(pos1, pos2, wp) is False: wp.pos = pos2.copy()

                if render is True:
                    obj = bpy.data.objects[wp.name]
                    obj.location = wp.pos.copy()
                    obj.keyframe_insert(data_path="location", frame=frame)

    # Create frame for blocked workpiece
    def WP_blocked(self, wp, frame, render):
        if render is True:
            obj = bpy.data.objects[wp.name]
            obj.location = wp.pos.copy()
            obj.keyframe_insert(data_path="location", frame=frame)

    # Move workpiece with separator when it is extended
    def separate_WP(self, pos, separator, WPregister, frame, render):
        self.sep1_blocked = False
        for wp in WPregister:
            # Check if workpiece in extraction range, separator not retracted, and separator moved since last time step
            if xppumathlib.wp_at_pos(pos, wp, frac=Vector([1.5,1.5,0])): # Only consider have the workpiece dimension for the overlapping
                if separator.DI_Retracted is False and \
                        ((separator.pos_extended.x > separator.pos_retracted.x and wp.pos.x < separator.pos.x) or
                         (separator.pos_extended.x < separator.pos_retracted.x and wp.pos.x > separator.pos.x) or
                         (separator.pos_extended.y > separator.pos_retracted.y and wp.pos.y < separator.pos.y) or
                         (separator.pos_extended.y < separator.pos_retracted.y and wp.pos.y > separator.pos.y)):
                    wp.pos.x = separator.pos.x
                    wp.pos.y = separator.pos.y

                    if render is True:
                        obj = bpy.data.objects[wp.name]
                        obj.location = wp.pos.copy()
                        obj.keyframe_insert(data_path="location", frame=frame)

                # Stop updating separator 1, if separated workpiece overlapps with any other workpiece
                if self.overlapp(wp, WPregister):
                    self.sep1_blocked = True
                    if render is True:
                        obj = bpy.data.objects[separator.name]
                        obj.keyframe_insert(data_path="location", frame=frame)

    # Get value of PresenceSensor
    def upd_pressensor(self, pos, wp_list):
        value = False
        for wp in wp_list:
            if xppumathlib.wp_at_pos(pos, wp): value = True
        return value

    # Get value of UltraSonicSensor
    def upd_ultrasonic(self, pos_start, pos_end, wp_list):
        value = xppumathlib.vec_norm(pos_end-pos_start)
        for wp in wp_list:
            if xppumathlib.vec_norm(wp.pos - pos_start) < value:
                value = xppumathlib.vec_norm(wp.pos - pos_start)
        return value

    # Get value of InductiveSensor
    def upd_inductive(self, pos, wp_list):
        value = False
        for wp in wp_list:
            if xppumathlib.wp_at_pos(pos, wp) and wp.color == "metallic":
                value = True
        return value

    # Get value of OpticalSensor
    def upd_optical(self, pos, wp_list):
        value = False
        for wp in wp_list:
            if xppumathlib.wp_at_pos(pos, wp) and (wp.color == "metallic" or wp.color == "white"):
                value = True
        return value

    # Get value of virtual position encoder
    def upd_virtualposition(self, pos_start, wp_list, WPregister):
        value = []
        for wp in wp_list:
            #value.append(xppumathlib.vec_norm(wp.pos - pos_start))
            if self.pos_start.x - self.pos_end.x != 0:
                value.append(sqrt((wp.pos.x - pos_start.x) ** 2))
            elif self.pos_start.y - self.pos_end.y != 0:
                value.append(sqrt((wp.pos.y - pos_start.y) ** 2))
        for i in range(len(WPregister)-len(value)):
            value.append(-1)
        value.sort(reverse=True)
        return value