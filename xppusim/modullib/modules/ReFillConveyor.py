import bpy
import xppumathlib

from xppusim.modullib.components.Switch import Switch
from xppusim.modullib.modules.Conveyor import Conveyor


class ReFillConveyor(Conveyor):
    def __init__(self, name, num_wps, Modul_Register, frame=0, render=True, logging=False):
        super().__init__(name, Modul_Register, frame, render)

        self.pos_WPbeforeSwitch = bpy.data.objects[name + ".switch.WPbeforeSwitch"].matrix_world.to_translation().copy()
        self.pos_WPbehindSwitch = bpy.data.objects[name + ".switch.WPbehindSwitch"].matrix_world.to_translation().copy()
        self.pos_WPPosition1 = bpy.data.objects[name + ".presencesensor1.sensorpos"].matrix_world.to_translation().copy()
        self.pos_WPPosition2 = bpy.data.objects[name + ".presencesensor2.sensorpos"].matrix_world.to_translation().copy()
        self.pos_LSCHandover = bpy.data.objects["largesortingconveyor.poshandover"].matrix_world.to_translation().copy()
        self.pos_SSCHandover = bpy.data.objects["smallsortingconveyor.switch.WPatSwitch"].matrix_world.to_translation().copy()

        self.handover_steps =  20 # Number of steps for handover to/from other conveyor

        self.switch = Switch(name + ".switch")
        self.DI_WPPosition1 = False
        self.DI_WPPosition2 = False
        self.VI_WPPosition = [-1] * num_wps

    def reset(self, frame, render=True, logging=False):
        super().reset(frame, render)

        self.switch.reset(frame, render)
        self.DI_WPPosition1 = False
        self.DI_WPPosition2 = False

    def update(self, WPregister, frame=0, render=True, logging=False):
        self.switch.update(WPregister, frame, render) # Update switch

        # Move workpieces at LargeSortingConveyor handover
        if self.DO_TurnClockwise is True:
            self.handover_WP(self.pos_end, self.pos_LSCHandover, self.handover_steps, WPregister, frame, render)

        # Move workpieces at SmallSortingConveyor handover
        if self.DO_TurnCounterClockwise is True:
            self.handover_WP(self.pos_start, self.pos_SSCHandover, self.handover_steps, WPregister, frame, render)

        # Move remaining workpieces on conveyor
        for wp in WPregister:
            if self.pos_compare(self.pos_WPbeforeSwitch, wp.pos) and self.switch.DI_Extended is True:
                wp.pos = self.pos_WPbehindSwitch.copy()

            elif self.pos_compare(self.pos_WPbehindSwitch, wp.pos) and self.switch.DI_Retracted is True:
                wp.pos = self.pos_WPbeforeSwitch.copy()

            elif self.DO_TurnClockwise is True and self.pos_in_range(self.pos_WPbeforeSwitch, wp.pos, 3*self.step_size) is True:
                if self.switch.DI_Retracted is True:
                    wp.pos = self.pos_WPbeforeSwitch.copy()
                else:
                    self.WP_blocked(wp, frame, render)

            elif self.DO_TurnCounterClockwise is True and self.pos_in_range(self.pos_WPbehindSwitch, wp.pos, 3*self.step_size) is True:
                if self.switch.DI_Extended is True:
                    wp.pos = self.pos_WPbehindSwitch.copy()
                else:
                    self.WP_blocked(wp, frame, render)

            else:
                self.move_wp(wp, WPregister, frame, render)


        # Update sensor values
        # Check which workpiece is on the conveyor. Due to performance reasons not integrated into sensor functions
        wp_list = []
        for wp in WPregister:
            if xppumathlib.wp_in_btw(self.pos_start, self.pos_end, wp) is True:
                wp_list.append(wp)

        self.DI_WPPosition1 = self.upd_pressensor(self.pos_WPPosition1, wp_list)
        self.DI_WPPosition2 = self.upd_pressensor(self.pos_WPPosition2, wp_list)
        self.VI_WPPosition = self.upd_virtualposition(self.pos_start, wp_list, WPregister)