import math
import xppumathlib

import bpy
from mathutils import Vector

from xppusim.modullib.Error import Error
from xppusim.modullib.Module import Module
from xppusim.modullib.components.MonostableCylinder import MonostableCylinder
from xppusim.modullib.components.TurningTable import TurningTable


class Crane(Module):
    def __init__(self, name, Modul_Register, frame=0, render=True, logging=False, turn_only_extended=False, move_at_pickpoint=True):
        super().__init__()
        Modul_Register.append(self)
        self.name = name
        self.pos_down = bpy.data.objects[name + ".liftingCylinder.Extended"].matrix_world.to_translation().copy() # Only to calculate arm length
        self.pos_center = bpy.data.objects[name].matrix_world.to_translation().copy()
        self.arm_length = math.sqrt(math.pow((self.pos_center.x-self.pos_down.x),2) + math.pow((self.pos_center.y-self.pos_down.y),2))
        self.turn_only_extended = turn_only_extended
        self.move_at_pickpoint = move_at_pickpoint

        self.wp_attached = None
        self.liftCyl_blocked = False
        self.liftingCylinder = MonostableCylinder(name + ".liftingCylinder")
        self.table = TurningTable(name)

        self.DO_Intake = False
        self.DI_TakenIn = True if self.wp_attached != None else False

    def reset(self, frame=0, render=True, logging=False):
        self.liftingCylinder.reset(frame, render)
        self.table.reset(frame, render)

        self.wp_attached = None
        self.liftCyl_blocked = False

        self.DO_Intake = False
        self.DI_TakenIn = True if self.wp_attached != None else False

    def update(self, WPregister, frame=0, render=True, logging=False):
        # Check if gripping is activated
        # Calculate gripper position based on crane angle
        self.pos_gripper = Vector([self.arm_length * math.cos(math.radians(self.table.angle)),
                                   self.arm_length * math.sin(math.radians(self.table.angle)),
                                   self.liftingCylinder.pos.z]) + self.pos_center
        # Check if workpiece below gripper
        wp_in_range = False
        for wp in WPregister:
            wp.attached = False
            pos_gripper_adj = self.pos_gripper.copy()
            pos_gripper_adj.z -= wp.dim.z # Origin of workpiece coordinate system at bottom of workpiece

            if xppumathlib.wp_at_pos(self.pos_gripper, wp, frac=Vector([0.5, 0.5, 0])):     # About 10 degrees from WP
                wp_in_range = True
            if self.DO_Intake == True and xppumathlib.wp_at_pos(self.pos_gripper, wp):
                wp.attached = True
                if not self.overlapp(wp, WPregister): # Check if workpiece clamped by other workpieces
                    self.wp_attached = wp
                    self.wp_attached.pos = pos_gripper_adj
                    if render is True:
                        obj = bpy.data.objects[self.wp_attached.name]
                        obj.location = self.wp_attached.pos.copy()
                        obj.keyframe_insert(data_path="location", frame=frame)
                    break
            elif pos_gripper_adj.to_tuple(4) == wp.pos.to_tuple(4) and self.DO_Intake == False and self.wp_attached is not None:
                if (self.table.DI_AtStamp is True or self.table.DI_AtConveyor is True or self.table.DI_AtStack is True) \
                   and self.liftingCylinder.DI_Extended is True:
                    self.wp_attached = None
                else:
                    raise Error("Workpiece detached in mid-air")

        # Update liftingCylinder, if not blocked
        self.liftingCylinder.update(WPregister, frame, render, self.liftCyl_blocked)

        # Update table
        pre_update_angle = self.table.angle
        self.table.update(WPregister, frame)
        # Throw collision error, if table is moved with retracted liftingCylinder and attached workpiece
        if (self.table.DI_AtStamp is False and self.table.DI_AtConveyor is False and self.table.DI_AtStack is False) \
            and self.liftingCylinder.DI_Retracted is False and self.wp_attached is not None and self.DO_Intake is True and self.move_at_pickpoint is True:
            raise Error('Crane extended with attached workpiece while not at pick-up location')

        if self.table.angle - pre_update_angle != 0 and self.wp_attached != None:
            if ((self.liftingCylinder.AI_Position < 10 and self.turn_only_extended is False) or
                    (self.liftingCylinder.DI_Retracted is False and self.turn_only_extended is True)):
                raise Error('Crane turned while lifting Cylinder not extended')

        #if self.table.angle - pre_update_angle != 0 and wp_in_range and self.wp_attached is None:
        #     raise Error('Crane moved with Gripper contacting a workpiece')

        # Throw collision error, if crane is retracted with attached workpiece outside of picking locations
        #if (self.table.DI_AtStamp is False and self.table.DI_AtConveyor is False and self.table.DI_AtStack is False and self.wp_attached is not None):
        #    if self.turn_only_extended is True and self.liftingCylinder.DI_Extended is False:
        #    #if self.turn_only_extended is True and self.liftingCylinder.DO_Extend is False:
        #        raise Error('Crane retracted with workpiece outside of picking positions')
        #    elif self.turn_only_extended is False and self.liftingCylinder.AI_Position < 10:
        #        raise Error('Crane retracted with workpiece outside of picking positions')

        # Move workpiece if gripper was activated
        self.pos_gripper = Vector([self.arm_length*math.cos(math.radians(self.table.angle)),
                        self.arm_length*math.sin(math.radians(self.table.angle)),
                        self.liftingCylinder.pos.z]) + self.pos_center

        self.liftCyl_blocked = False
        if self.wp_attached is not None:
            pos_gripper_adj = self.pos_gripper.copy()
            pos_gripper_adj.z -= self.wp_attached.dim.z # Origin of workpiece coordinate system at bottom of workpiece
            self.wp_attached.pos = pos_gripper_adj.copy()

            if render is True:
                obj = bpy.data.objects[self.wp_attached.name]
                obj.location = self.wp_attached.pos.copy()
                obj.keyframe_insert(data_path="location", frame=frame)

            # If attached workpiece overlapps with another workpiece stop updating the lifitingCylinder
            if self.overlapp(self.wp_attached, WPregister):
                self.liftCyl_blocked = True
                if render is True: # Keyframe for blocked cylinder
                    obj = bpy.data.objects[self.liftingCylinder.name]
                    obj.keyframe_insert(data_path="location", frame=frame)

        # Update sensor signals
        self.DI_TakenIn = True if self.wp_attached != None else False