import bpy
import time
import os
import sys

argv = sys.argv # Get arguments
video_path = argv[-1][14:] # Path in last argument starting at position 14 (after "-- video_path ")

bpy.context.scene.render.image_settings.file_format = "AVI_JPEG"
bpy.context.scene.render.filepath = video_path
bpy.ops.render.opengl(animation=True, view_context=False)

while not os.path.isfile(video_path): # Wait until rendering is finished
    time.sleep(1)

bpy.ops.wm.quit_blender()
