XPPUSim - Discrete State Simulation of the XPPU
============

This project provides a discrete state simulation of the eXtended Pick and
Place Unit (XPPU). The simulation is build in python and utilizes the latest
blender model of the XPPU. Due to the use of blender, the simulation provides
the ability to render its results for visualization. The simulation is build
with the overarching objective to be highly modular.

The project is currently still in a development state. All code including this readme shall therefore be considered as Work in Progress.
Accept breaking changes.

## Requirements
This section describes the minimum requirements for running *XPPUsim*.
The optional requirements are mostly necessary for supporting the 
further development of the simulation module.

#### Minimum requirements
The minimum requirements for running and rendering simulations are the following:
- [Python 3.7](https://www.python.org) including the packages
  - numpy (version 1.16 or later)
  - bpy (version 2.80)
  - blender-mathutils (version 2.80)
-   [Blender 2.80](https://wiki.blender.org/wiki/Main_Page) 

#### Optional Requirements
For development the following additional tools are recommended:
-   [conda](https://www.anaconda.org) (for managing the environment)
-   [Sphinx](http://sphinx-doc.org) (for generating documentation)


## Setup

### Install Python modules
The package *XPPUsim* can be installed via pip within an existing python environment
or via conda in a newly created one. To run the installation either python 3.7
including pip or conda needs to be installed. While pip installs the module
in the main or currently activated python environment, the installation with
conda creates a new environment named *xppusim_env*. During the installation all
required python packages excluding *bpy* and *blender-mathutils* are installed.

To install the module execute the following steps:

1. Download and extract this GitLab repository to your local machine
2. Open a terminal and move into your extracted folder *xppusim-master*
3. Install the module with one of the following commands
    ```bash 
    pip install .                           # Installation with pip
    conda env create -f environment.yml     # Installation with conda
    ```


### Install blender and the blender python module (bpy)
Next, *bpy*, *blender-mathutils*, and *blender* needs to be manually installed.
Detailed instructions for the installation process are provided in
[BLENDER.md](/BLENDER.md). 

## Usage

This section provides a short overview on how to use the simulation. For technical
details and further settings we refer to the Implementation section.

### Setting up  the simulation environment
This section describes the basic setup of the simulation environment. The 
complete python code can be found in *main.py*

In order to run the simulation of the XPPU, an instance of the simulation model
needs to be created by calling the constructor of the class *XPPU*. 

```python
from xppusim import modullib                        # Import of module modullib

xppu = modullib.XPPU(model_path_rel, root_path)     # Instantiation of simulation model
```

For the case that actions occure that cannot be simulated by the discrete model,
i.e., the falling of a workpiece released by the crane in mid-air, the simulation
throws an exception. It is therefore recommended to catch thrown errors and to
print the respective error message.

```python
try:

    # Include actions to be simulated here
    
except modullib.Error as err:
    print(err) 
```
All state changes during a simulation are added as animation data to the blender
model. This animation data can be saved to an external file in the directory
*<output_dir>/blend* and then be rendered via the installed blender application. The 
rendered video is saved to *<output_dir>/video/result.avi*. The path *<output_dir>*
needs to be provided as an argument to the saving and rendering function. The
directory must exist and write permissions are required.

```python
import os
output_dir = os.path.dirname(__file__) + "/output"      # Folder named "output" within the main's directory

xppu.save(output_dir, "tmp.blend")                      # Save animation data to /output/blend/tmp.blend
xppu.render_file(output_dir, "tmp.blend")               # Render saved blender model to /output/video/result.avi
```

### Test scenarios
The project includes various test scenarios of the different functions of the
simulation model. A test scenario can be executed by calling its function with
the simulation model instance as argument. A list of the various scenarios
can be found in the header of the module *test_scenarios.py*. Running the various
test scenarios is the recommended way to get to know the capabilities of the
simulation.
```
from xppusim import test_scenarios

test_scenarios.standard_1(xppu)
```

An optional boolean value can be passed to the test scenario functions in
order to print selected sensor values that are relevant to the respective
scenario.
 ```
test_scenarios.standard_1(xppu, True)
```


### High-level interface
The project also includes various high-level functions in order to simplify the
creation of new scenarios. High-level functions include commands to move the 
crane to a certain position, to pick up a workpiece, or to move a workpiece to
a ramp on the conveyor. The high-level functions are mostly implemented using 
the post-condition approach. Thus, a command is executed until a certain 
condition represented by a sensor value is fullfilled or a timeout occures.
A list of the available high-level functions can be found in the header
of the module *high_level_func.py*.

High-level functions are executed with a basic function call with the 
simulation model instance as argument.
 ```python
from xppusim import high_level_func

high_level_func.crane_moveToConveyor(xppu)           # Moves the crane from any position to the conveyor
```
An optional boolean value can be passed to high-level functions in order to
print selected sensor values that are relevant to the respective task.
 ```python
high_level_func.crane_moveToConveyor(xppu, True)    # Moves the crane from any position to the conveyor
 ```

### Low-level interface
Scenarios can be also build by manually setting the variables of the different
components of the XPPU and then executing a single or multiple time steps. The
available variables are similar to the variables included in the SysML model of
the XPPU. Their names usually start with "DO_".

```python
xppu.stack.separator.DO_Extend = True               # Extend monostable cylinder of stack
xppu.make_step()                                    # Make a single time step
xppu.crane.liftingCylinder.DO_Extend = False        # Retract monostable cylinder of crane
xppu.make_step(5)                                   # Make five time steps
```

The sensor values of the XPPU can be read in a similar way. Their names
usually start with "DI_" for digital sensors, "AI_" for analog sensors, and
"VI_" for virtual sensors.

```python
print(xppu.stack.separator.DI_Extended)             # Print value of end position sensor of cylinder
print(xppu.crane.table.AI_Position)                 # Print value of potentiometer of crane
```

Beside the manual setting and reading of each actuator and sensor variable,
variables can also be accessed with the *get_states()* and *set_states()*
function. While *get_states()* provides a dictionary containing all sensor
and actuator variables and their values, *set_states()* changes variables
based on the values provided in a dictionary.

```python
state_dict = xppu.get_states()                     # Gets dictionary with all states
print(state_dict[stack.separator.DI_Extended])     # Print value of end position sensor of cylinder
print(state_dict[crane.table.AI_Position])         # Print value of potentiometer of crane

state_dict[stack.separator.DO_Extend] = True       # Extend monostable cylinder of stack
xppu.set_states(state_dict)                        # Update all variables
```

## Implementation
This section will focus on the implementation details of the simulation. It will
probably be the most helpfull section, however, most parts are not written yet...

### Overall project structure
The project containts the following modules:
* main
* xppusim/test_scenarios.py
* xppusim/high_level_func.py
* xppusim/modullib.py
* xppusim/scripts/run_blender.py

#### main.py
This module is responsible for executing the simulation. It contains exemplary
code on how to setup and run simulations.

#### test_scenarios.py
This module includes various testing scenarios. For available scenarios, we refer
to the comments within its header.

#### high_level_func.py
This module includes the high level functions for running the simulation and 
setting up new scenarios. For a list of the available functions, we refer
to the comments within the modules's header.

#### modullib.py
This is the main module containing all classes implementing the discrete
state simulation

#### run_blender.py
This is an external script which is passed to the blender application for
external rendering.

### Implementation details
This section will describe how the the simulation is implemented via the 
various classes within *modullib*. It will be a great section, but it does not
exist yet...


## References
-   [conda user guide](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html)
    

<br>
<sup>
Copyright © 2019 TU Munich - Institute of Automation and Information Systems.
http://www.ais.mw.tum.de/en/institute/
All rights reserved. 
</sup>
<br>
<sub>
Contact: jonas.zinn@tum.de
</sup>
