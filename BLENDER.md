Installing Blender
=================

Blender offers the officially unsupported option to be compiled as a python module
called *bpy*. Bpy allows to access and manipulate blender models directly via python
without the need of a working blender installation. This manuel covers the
installation of *bpy* for various operating systems.

Due to a bug within the *bpy 2.80* implementation, this module cannot be used for
rendering. XPPUsim therefore relies on the full *blender 2.80* application to
render simulations. The installation of *blender 2.80* is also covered by this manual.




## Windows
This section includes instructions on how to install *bpy* as well as
*blender 2.80* on Windows. It is highly recommended to install the precompiled
*bpy* module included in this project. However, if you need to build it
yourself, you find respective instructions in the last part of this section. 
Note that blender stopped the support of Windows x86 since version 2.80.

### Installing the precompiled bpy module on Windows x64

This is the simplest and therefore recommended option to run this project. In 
order to install the precompiled bpy module, please execute the following steps:

1. Copy the content of the folder *bpy280/Python* from the repository to the root
 directory of your python environment.
  
   If a conda environment is used, the standard root directory is:
   
   ```bash
   C:\Users\<UserName>\.conda\envs\xppusim_env      # Change xppusim_env to your specific environment
   ```
  
   If no environment is used, the standard root directory is:
    
   ```bash
   C:\Users\<UserName>\AppData\Local\Programs\Python\Python3x 
   ```



### Installing the blender 2.80 application

The installation of the blender 2.80 application on Windows is straightforward. For its
installation we refer to the official [blender website](https://www.blender.org/).

In order to access the application with python the location of the blender.exe
needs to be added to the environment variable *PATH*. A guide to change this
environment variable can be found [here](https://www.computerhope.com/issues/ch000549.htm).
The standard windows installation path of blender that needs to be added is usually:
```bash
C:\Program Files\Blender Foundation\Blender\
```

### Compiling bpy for Windows x64

In order to compile and install *bpy* on Windows please follow the following steps.
The steps are based on the official 
[blender build guide](https://wiki.blender.org/wiki/Building_Blender/Windows). 
All steps are tested with blender 2.80, the required Visual Studio version as well as the
required libraries may change in future versions.


1. Install the following programs and tools:
   * [Visual Studio Community 2017](https://visualstudio.microsoft.com/de/downloads/)
   * [Subversion for Windows (SlikSVN)](https://sliksvn.com/download/)
   * [Git for Windows](https://gitforwindows.org/)
   * [CMake](https://cmake.org/)

   For the installation of CMake make sure to select the tick mark to add
   CMake to the environment variable path.
   
   For the installation of Visual Studio select Desktop Development with C++. 
   You can deselect all optional components excluding VC++ 2017, Windows 10 SDK,
   and Visual C++-Tools für CMake.

2. Create a working directory (e.g., *C:\blender-git*) and get the latest blender source code
   ```bash
   cd C:\blender-git
   git clone git://git.blender.org/blender.git
   cd blender
   git submodule update --init --recursive
   git submodule foreach git checkout master
   git submodule foreach git pull --rebase origin master
   ```

3. Download the required libraries for building bpy (~ 6 GB)
   ```bash
   cd C:\blender-git
   svn checkout https://svn.blender.org/svnroot/bf-blender/trunk/lib/win64_vc14  lib/win64_vc14
   ```

4. Compile the *bpy* module
   ```bash
   cd C:\blender-git\blender
   make bpy
   ```

5. Go to the following directory within the new folder containing your build in
   the blender-git directory
   ```bash
   blender-git/<YourBuild>/bin/release/2.8x
   ```

6. Copy the folder *2.8x* to the root directory of your python environment

   If a conda environment is used, the standard root directory is:
   
   ```bash
   C:\Users\<UserName>\.conda\envs\xppusim_env      # Change xppusim_env to your specific environment
   ```   
   
   If no environment is used, the standard root directory is:
    
   ```bash
   C:\Users\<UserName>\AppData\Local\Programs\Python\Python3x 
   ```
  
7. Copy the file *bpy.pyd* to the following directory within the root of your
python environment
   ```bash
   <PythonRoot>/Lib/site-packages/
   ```
   
8. Copy all files **.dll* EXCLUDING *python3x.dll* to the following directory
   ```bash
   <PythonRoot>/Lib/site-packages/
   ```


## Linux/Ubuntu
Congratulations for using Linux. While you need to build *bpy* by yourself,
the process is straightforward, if you follow the instructions in this section.
The instructions have been tested with Blender 2.83. 

### Installing the precompiled bpy module on Ubuntu
This is the simplest and therefore recommended option to run this project. In 
order to install the precompiled bpy module, please execute the following steps:

1. Extract the precompiled libraries from this repository
   ```bash
   tar -xzvf bpy290_precompiled_ubuntu.tar.gz
   ```
2. Copy the extracted content to the site-packages folder of your python environment

   If a conda environment is used, the standard site-packages directory is:
   
   ```bash
   /home/.conda/envs/xppusim_env/lib/python3.7/site-packages      # Change xppusim_env to your specific environment
   ```


### Compiling bpy for Ubuntu (non-portable)
In order to compile and install bpy on Ubuntu please follow the following steps.
The steps are based on the official
[blender build guide](https://wiki.blender.org/wiki/Building_Blender/Linux/Ubuntu)
and are tested with Ubuntu 18.04 LTS.

1. Install the packages *git* and *build-essential*
   ```bash
   apt-get update
   sudo apt-get install git
   sudo apt-get install build-essential 
   ```
2. Get the latest blender source code
  
   ```bash
   mkdir ~/blender-git
   cd ~/blender-git
   git clone https://git.blender.org/blender.git
   cd blender
   git checkout blender-v2.83-release
   git submodule update --init --recursive
   git submodule foreach git checkout blender-v2.83-release
   git submodule foreach git pull --rebase origin blender-v2.83-release
   ```

3. Install the required dependencies for building bpy
   
   ```bash
   cd ~/blender-git
   ./blender/build_files/build_environment/install_deps.sh
   ```

4. Point the environment variable *PYTHON_ROOT_DIR* towards the python
   environment in which you installed *XPPUsim*
   ```bash
   sudo export PYTHON_ROOT_DIR=/home/<UserName>/.conda/envs/xppusim_env      # Change xppusim_env to your specific environment
   ```

4. Compile and install *bpy* to your environment 
   ```bash
   cd ~/blender-git/blender
   sudo make bpy
   ```
   Make sure that the process finishes with "Blender successfully build" and
   without any message stating that the folder *site-packages* could not be found.
   Be aware that there will not be a blender file in the newly created folder
   *~/blender-git/build_files/blender/bin* even if this is stated by a message.
   
   If the installation has been successful skip steps 5-8, otherwise continue.
   
5. Enter the configuration dialog of cmake 
      ```bash
   cd ~/blender-git
   sudo ccmake build_linux_bpy
   ```
6. Press *t* in order to enable the advanced settings and add the path to the
site-packages folder of the python environment in which you installed *XPPUsim*
(*~/xppusim_env/lib/python3.x/site-packages*) to the key *PYTHON_SITE_PACKAGES*.
Press *c* to configure and *g* to generate the configuration.

7. Compile and install *bpy* to your environment
   ```bash
   cd blender
   sudo make bpy
   ```
  

### Compiling bpy for Ubuntu (portable)
If the compiled bpy module shall be used on different systems, a portable
version is necessary. The compiling of a portable version is described in the
following steps:

1. Install the packages *git* and *build-essential*
   ```bash
   apt-get update
   sudo apt-get install git
   sudo apt-get install build-essential 
   ```
2. Get the latest blender source code
  
   ```bash
   mkdir ~/blender-git
   cd ~/blender-git
   git clone https://git.blender.org/blender.git
   cd blender
   git checkout blender-v2.83-release
   git submodule update --init --recursive
   git submodule foreach git checkout blender-v2.83-release
   git submodule foreach git pull --rebase origin blender-v2.83-release
   ```

3. Download the precompiled dependencies
   ```bash
   cd ~/blender-git
   mkdir lib
   cd lib
   svn checkout https://svn.blender.org/svnroot/bf-blender/trunk/lib/linux_centos7_x86_64
   ```
   
4. Configure cmake. The strange use of spaces is necessary
   ```bash
   cmake -C~/blender-git/blender/build_files/cmake/config/bpy_module.cmake \
         -Wl,--verbose \
         -DWITH_OPENMP:BOOL='0' \
         -DWITH_OPENMP_STATIC:BOOL='1' \
         -DPNG_LIBRARY_RELEASE:FILEPATH=~/blender-git/lib/linux_centos7_x86_64/png/lib/libpng16.a \
         -DWITH_MEM_JEMALLOC:BOOL=OFF \
         -S ~/blender-git/blender \
         -B ~/blender-git/build_linux_bpy
   ```
   
5. Specify the path to the python installation
   ```bash
   cd ~/blender-git
   ccmake build_linux_bpy
   ```
   Press *t* in order to enable the advanced settings and add the path to the
   site-packages folder of the python environment in which you installed *XPPUsim*
   (*~/xppusim_env/lib/python3.x/site-packages*) to the key *PYTHON_SITE_PACKAGES*.
   Press *c* to configure, *e* to exit the configuration notes screen, and *g*
   to generate the configuration.
   
6. Build and install bpy.
   ```bash
    make install
   ```
   
### Installing the blender 2.80 application
The *blender 2.80* application can be directly installed via the Ubuntu
Software-Center. Alternatively you can download a tar.bz2 archive with latest
version from the official [blender website](https://www.blender.org/) and extract it to your prefered location. If
you want to install blender via apt-get, make sure that the version 2.80 is
available (currently not the case).

In order to access the application with python, its path needs to be added to
the environment variable *PATH*. If *blender* is installed via the Ubuntu 
Software-Center, this is handled automatically. Otherwise, the following lines
need to be manually added to the end of *~/.bashrc*.
```bash
export PATH=<Path to blender>${PATH:+:${PATH}}
```
The file *~/.bashrc*. can be edited within the console via:
```bash
nano ~/.bashrc
```


## macOS
If you are using macOS, you cannot be helped. While there is currently no
known solution to install bpy within conda, the following steps describe
the compiling and installation within the main python environment. 

1. Compile bpy
   ```bash
   export PATH="/Applications/CMake.app/Contents/bin":"$PATH"
   echo PATH="/Applications/CMake.app/Contents/bin":"\$PATH" >> ~/.profile
   mkdir .blenderpy
   cd .blenderpy
   mkdir master
   cd master
   git clone http://git.blender.org/blender.git
   cd blender
   make update
   cd ..
   mkdir build_bpy_darwin_custom
   cd build_bpy_darwin_custom
   cmake ../blender -DWITH_PLAYER=OFF -DWITH_PYTHON_INSTALL=OFF -DWITH_PYTHON_MODULE=ON -DWITH_OPENMP=OFF -DWITH_AUDASPACE=OFF
   make install
   ```
   
2. Copy the compiled module into your python directory. 
   ```bash
   mkdir /Library/Frameworks/Python.framework/Versions/3.7/lib/python3.7/Resources
   cp bin/bpy.so /Library/Frameworks/Python.framework/Versions/3.7/lib/python3.7/site-packages
   cp -R bin/2.82 /Library/Frameworks/Python.framework/Versions/3.7/lib/python3.7/Resources
   ```

You might need to compile and build *blender-mathutils* separately. For this,
please follow the blender-mathutils
[build instructions](https://gitlab.com/ideasman42/blender-mathutils)


<br>
<sup>
Copyright © 2019 TU Munich - Institute of Automation and Information Systems.
http://www.ais.mw.tum.de/en/institute/
All rights reserved. 
</sup>
<br>
<sub>
Contact: jonas.zinn@tum.de
</sup>