import os
from xppusim.modullib.XPPU import XPPU

output_dir = os.path.dirname(os.path.abspath(__file__)) + "/output"

# Create model
xppu = XPPU(render=True)


xppu.stack.separator.DO_Extend = True
xppu.make_step(10)
xppu.stack.separator.DO_Extend = False
xppu.crane.liftingCylinder.DO_Extend = False
xppu.make_step(10)
print(xppu.get_states())                 # To get full dataset, call xppu.make_step(), xppu.get_states() 10x in a row instead of xppu.make_step(10)
xppu.crane.DO_Intake = True
xppu.crane.liftingCylinder.DO_Extend = True
xppu.make_step(10)
xppu.crane.table.DO_TurnCounterClockwise = True
xppu.crane.liftingCylinder.DO_Extend = False
xppu.make_step(7)



xppu.save(output_dir, "tmp.blend")
xppu.render_file(output_dir, "tmp.blend")