# Setup script for the xppusim application.

from setuptools import setup, Extension, find_packages


# Get the local package version
def _version():
    path = "xppusim/__version__.py"
    namespace = {}
    with open(path) as stream:
        exec(stream.read(), namespace)
    return namespace["__version__"]

# C Extensions
xppumathlib = Extension("xppumathlib",
                  sources=["xppusim/libraries/xppumathlib.c"])

# Setup parameter
setup(
    name = "xppusim",
    version = _version(),
    description = "A discrete state simulation of the XPPU",
    author = "Jonas Zinn",
    author_email = "jonas.zinn@tum.de",
    packages = find_packages(),
    py_modules=["scripts/run_blender"],
    install_requires = ["numpy"],
    package_data={"xppusim": ["model/*.blend", "scripts/*.py"]},
    ext_modules=[xppumathlib]
)
