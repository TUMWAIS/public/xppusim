import os
from xppusim.modullib.XPPU import XPPU
from xppusim.modullib.Error import Error
from xppusim import test_scenarios

output_dir = os.path.dirname(os.path.abspath(__file__)) + "/output"

# Create model
xppu = XPPU(render=True, logging=True)

# Run Scenario
try:
    test_scenarios.standard_5(xppu)

except Error as err:
    print(err)
xppu.save(output_dir, "tmp.blend")
xppu.render_file(output_dir, "tmp.blend")